# Contributing

We love merge requests from everyone. By participating in this project, you
agree to abide by the following documents:

* [Code of Conduct](./CODE_OF_CONDUCT.md)
* [License](./LICENSE.txt)
* [Developer Certificate of Origin](./DCO.txt)
  as applied to the license above.

## Fork Me!

Fork, then clone the repo:

    git clone git@codeberg.org:your-username/interpeer.io.git

## Setup

Use [pipenv](https://pipenv.pypa.io/en/latest/) to create a virtual environment
and change to it or not, as you see fit.

Then install the requirements:

```bash
$ pip install pipenv
$ pipenv install
```

## Building

Run pelican, possibly via the `Makefile`:

```bash
$ pipenv run make html
```

The newly built site is now in the configured output directory.

## Adding Content

For the most part, follow the [Pelican Documentation](https://docs.getpelican.com/).

As a TL;DR, Pelican can generate pages and articles (and indices thereof),
which in this project are written in Markdown (with a few extensions). Static
HTML pages will be generated from this Markdown input. See the `content`
directory for the current content.

## Modifying the Theme

The theme currently used is referenced in the pelican configuration. We use
a versioned scheme, where the theme version is the start date of a major
update (similar to how you'd use a feature branch), and a `current` symlink
for the published version (similar to how you'd merge to master).
