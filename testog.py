#!/usr/bin/env python3

if __name__ == '__main__':
  import sys
  url = sys.argv[1]
  if not url.startswith('http') and not url.startswith('file'):
    import os, os.path
    url = 'file://' + os.path.abspath(url)
  print('URL:', url)

  from opengraph_parse import parse_page
  og = parse_page(url)

  longest = 0
  for k in og.keys():
    if len(k) > longest:
      longest = len(k)

  for k, v in og.items():
    print(k.ljust(longest), '=', v)
