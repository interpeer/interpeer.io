# interpeer.org

Repo for the [https://interpeer.org](https://interpeer.org) website.

## Usage

### Requirements

This project uses the [Hugo](https://gohugo.io/) framework and
[Tailwind](https://tailwindcss.com/) css for as CSS library.

### For local development

**Hugo**

See the [Hugo development commands](https://gohugo.io/getting-started/usage/#develop-and-test-your-site)

**Tailwind**

As (for now) we are not using any node npm packages for the frontend that's
why we are using the [Standalone CLI](https://tailwindcss.com/blog/standalone-cli)
of Tailwind. If it turns out we need more packages we might switch to npm, but
for now we are trying to avoid it.

To use Tailwind Standalone CLI please follow
[these instructions](https://tailwindcss.com/blog/standalone-cli#get-started).
It depends on your OS what executable you need to download.

**All together**

There is a bash script to run all local development scripts.

You might need to give the file permission first with `chmod +x ./dev.hs`.

Then run `bash dev.hs`.
This fires up the local Hugo server and Tailwind.
