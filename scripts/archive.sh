#!/bin/bash
pipenv run archiver \
  --sitemaps https://interpeer.org/sitemap.xml \
  --archive-sitemap-also \
  --jobs 4 \
  --rate-limit-wait 25 \
  --log INFO
