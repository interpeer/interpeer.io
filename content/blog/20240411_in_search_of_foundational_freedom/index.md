---
title: "In Search of Foundational FLOSS Freedom(s)"
date: 2024-04-11T09:00:00+00:00
tags: ['free software', 'open source', 'foss', 'floss', 'four freedoms', 'xz', 'xz-utils', 'CVE-2024-3094', 'rights', 'asimov', 'robots', 'laws of robotics', 'marx', 'ai', 'llm', 'artificial intelligence', 'large language model', 'perens', 'bruce perens', 'post-open license', 'digital commons']
author: Jens Finkhaeuser
categories: ['miscellaneous']
image: robots.jpg
---

A few days ago, a {{< external text="vulnerability in xz-utils named CVE-2024-3094"
  url="https://nvd.nist.gov/vuln/detail/CVE-2024-3094" archive=false >}}
was discovered, and since then the open source community as well as security
pundits fall over themselves and each other to provide the best analysis of
this incident.

Don't worry, this post isn't another one of those.

Because while all the speculation about what motivates such a long-term attack
is fun, the underlying issue is way, way simpler.

In a tweet[^1], {{< external text="Heather Adkins of Google" url="https://twitter.com/argvee" archive="force" >}}
posted an "unpopular opinion: if your hobby is now responsible for running the modern world,
it's no longer a hobby".

[^1]: Are they now called Xits?

{{< fig src="hobby.jpeg"
  width="100%"
  alt="unpopular opinion"
  caption="A very unpopular opinion."
>}}

I felt inclined to be kind that day, and chose to interpret these words to mean
that she feels a person maintaining such a project should be paid a living wage
for it. Surely that's what she meant, right?

I'm not writing this to be snarky. Well, OK, I am. But that's not the only
reason.

The CVE on the one hand, and this tweet on the other, represent a fundamental
issue in the relationship between the FLOSS community on the one hand,
and corporations on the other: FLOSS is built upon reciprocity, but corporations
treat it as a resource they can extract.

Of course, as in all of these things, there are corporations that do better and
those that do worse. But from the first invention of Open Source -- as a
derivative of and distinction to the previous Free Software -- the aim has been
to make FLOSS more friendly for corporations. In licensing terms, the more
permissive licenses of OS differ drastically from the copyleft style licenses
in that they no longer require reciprocity.

When I bring this up in any group of FLOSS contributors, the arguments
invariably follow the same paths. There's no surprise here, given that we've
been having and re-hashing these arguments for decades now. Rather than repeating
them here, let's just say that I actually understand and agree with all of them.

That is to say, each of these arguments have their *time*.

When Stallman invented Free Software, it was in response to objectionable
business practices. When the Open Source term was coined, it was to help bring the
benefits of Free Software to a wider audience, which in this case meant businesses
struggling with the existing closed source offerings.

At their respective points in time, each of these developments as well as a myriad
of smaller events leading up and branching off them *make perfect sense*.

But the world is ever changing, and has changed since then. Which means that the
old arguments, those that *led* to changes made decades ago, no longer apply.

Let me rephrase this more carefully: they *need* no longer apply.

What we can say with more certainty is that the reality they have helped construct
is far more brittle than intended. A failure such as the xz vulnerability is, above
all else, a call to re-examine the values we've constructed this reality on.

---

And this leads me to Sir Isaac Asimov.

And that's why I picked this picture of robots sharing power in reciprocity.

{{< fig src="robots.jpg"
  link="https://www.flickr.com/photos/68776313@N00/5164271"
  rel="external"
  target="\_blank"
  width="100%"
  alt="robots"
  caption="[\"robots\"](https://www.flickr.com/photos/68776313@N00/5164271) by [jmorgan](https://www.flickr.com/photos/68776313@N00) is licensed under [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)"
>}}

You might know Asimov wrote a bunch of stories about robots. With the rise of
LLMs and the related popularity of the "AI" term, he's gained more visibility again.
What you may not know is that he formulated {{< external text="three laws"
  url="https://en.wikipedia.org/wiki/Three_Laws_of_Robotics" archive=false >}} under which robots
could be helpful to humanity, rather than harmful:

* The First Law: A robot may not injure a human being or, through inaction,
  allow a human being to come to harm.
* The Second Law: A robot must obey the orders given it by human beings except
  where such orders would conflict with the First Law.
* The Third Law: A robot must protect its own existence as long as such
  protection does not conflict with the First or Second Law.

Never mind how much reasoning it takes to consider these laws, and that humans
struggle with the complexity of that -- he was writing fiction to explore these
ideas, after all, not actually programming robots.

I feel there is a spiritual kinship between these three laws and the four freedoms of
FLOSS:

* The freedom to run the program as you wish, for any purpose (freedom 0).
* The freedom to study how the program works, and change it so it does your
  computing as you wish (freedom 1). Access to the source code is a precondition
  for this.
* The freedom to redistribute copies so you can help others (freedom 2).
* The freedom to distribute copies of your modified versions to others (freedom 3).
  By doing this you can give the whole community a chance to benefit from your
  changes. Access to the source code is a precondition for this.

Both focus on ideals, and try to describe the ethical framework within which
decision making should move -- even though the topics of the respective decision
making processes are rather different.

For the purposes of my argument here it is important to understand that both
permissive licenses as well as copyleft licenses fulfill those four freedoms. That
isn't to say that all licenses do -- rather, that the question of whether those
freedoms are fulfilled governs whether or not a license becomes {{< external
  text="OSI approved" url="https://opensource.org/license" archive=false >}}.

What's striking to me is the history of Asimov's Laws. Because in the course of
exploring them in his writing, he also modified the laws. Some modifications
were small, but one modification in particular is pertinent here, namely his
introduction of a later Zeroth Law:

* The zeroth Law: A robot may not injure humanity or, through inaction, allow
  humanity to come to harm.

Asimov realized, in the course of his writing, that sometimes it isn't at all
about whether individual humans are harmed. Because acting according to the
three laws, a robot would, essentially, save Hitler from an assassination
attempt.

Mixing SF franchises is a risky business, but I'll cross the time streams now and
quote Spock here: "The needs of the many outweigh the needs of the few, or the one."

{{< youtube "KqpcmQhnl48" >}}

That line may encapsulate the meaning of this new zeroth law better than the law
itself.

---

But what does this have to do with us?

Well, if FLOSS is built on the four freedoms, and FLOSS has created an environment
that is *brittle*, i.e. prone to cracks and failures -- then just like Asimov
reviewed his three laws and added another to supersede them, perhaps it's time
for FLOSS to similarly augment the four freedoms.

It is, in fact, these words of Spock's that ring in my mind the most. They are
not particularly well reflected in the four freedoms, in my opinion.

The freedoms as formulated build upon each other, and freedoms 0 and 1 -- the
more fundamental ones -- speak only about the one. YOU should be free to run
and study software.

Freedom 2 introduces the many, in focusing on YOUR ability to help. Only
freedom 3 speaks of community.

Compare this to Asimov's Laws, where the original three laws also speak only
about individual robots. In a sense, they were more badly formulated in this
respect than the four freedoms are. But in amending them, he prioritized community
over the individual by making the new law not an addendum, but the foundational
principle.

I think that reciprocity needs to be similarly foundational in the four freedoms.
Perhaps we have to add a -1th freedom, or perhaps we just need to amend the ones
we have.

But it's clear that freedom to extract value from OS that permissive licenses
grant is not balanced by anything that guarantees reciprocity in a way that
keeps FLOSS communities sustainable. That permits "hobby" maintainers to work
in a way that doesn't burn them out, so that the world can continue to depend
on their efforts.

We *have* to address this in a fundamental way. The alternative may well be the
(eventual) end of FLOSS as we know it.

---

I actually struggle to formulate a -1th freedom. But in part, this is because
this framing of the issue in terms of "freedoms" is somewhat problematic.

There's a reason the {{% extref udhr %}} does not deal in freedoms. More precisely,
it does not *exclusively* deal in freedoms, but also in rights.

Freedoms grant everyone a choice. That is what freedom means: just because
I have the freedom to run a program as I wish, I have no obligation to run
it, nor to run it according to my wishes.

Rights are stronger: they permit people to demand that they can exercise the
freedoms granted to them. Laws that respect the fundamental human rights
understand that in practice, there is a balance to be made between the freedoms
of one person and those of the other. Both have the right to their freedoms,
but such freedoms must be moderated where it may impact someone else. This
is encapsulated by the adage that you have the right to swing your fists how you like,
but this right ends at my nose.

What this means is that the *rights of others* turn into *obligations of yours*,
or that the freedom of others restricts your own. This is a necessary component
of a fair system, even if it is less idealistic than speaking merely about
freedoms.

I would therefore be inclined to formulate an addition to the four freedoms
as a right, in order to bound the four freedoms within a framework of reciprocity.

This begs the questions of who to grant a right to, and which right to grant?

The issue with the freedoms as formulated is that "the many" are poorly reflected.
Who are those "many"? Other participants in FLOSS projects?

I think that the motivation of freedoms 2 and 3 is to explicitly offer help to
those who do not actively participate in projects. This, at any rate, is the
impression I'm left with after a number of other discussions on this topic. I
would therefore be inclined to grant a right to the general public.

Which right should that be? It must be their right to access, and to keep
accessing, the benefits of a FLOSS software. That is, after all, what
reciprocity implies for "them" as mentioned in freedoms 2 and 3.

Any license permits access in principle, but if the software is no longer
hosted anywhere, access is effectively denied. Similarly, access in the form
of a file upload somewhere is of little use to the general population; it takes
a person versed in evaluating FLOSS projects to even start any process
that may end with meeting the needs of the general public.

On the other hand, we cannot obligate a hobby project maintainer to provide
professional support and guarantee archiving of software assets. So how do we
go about this?

It brings me back to the bad ecosystem actors, those corporations who extract
value without "giving back" from FLOSS. And it brings me to Karl Marx, who has
stated as a reciprocal ideal "from each according to his ability, to each
according to his needs".

If we try to apply this ethos to the FLOSS ecosystem, we can fairly easily
define what each participant needs, as well as what each participant can give.
As an incomplete sample, consider the following table:

| Role                | Needs                                        | Ability                       |
| ------              | ----------------                             | -------                       |
| *FLOSS contributor* | Financial and infrastructure support         | Code & related asset creation |
| *User*              | Functioning software that meets their needs  | Use cases, production testing |
| *Corporation*       | Labor outsourcing                            | Revenue and infrastructure    |


Of course, for illustration purposes, I've limited the table to a subset of the
needs and abilities each participant brings to the table. It'll be easy to fill
the table with more examples.

But this set of examples does illustrate how contributors and users alike supply
the corporation with labor that they wish to outsource. Conversely, corporations
are designed to generate revenue, which can pay both for labor and infrastructure
that developers need.

The basic model of reciprocity that FLOSS needs to be sustainable is right there.

What's missing is the obligation, derived from participant rights, to act
accordingly.

Recall that we're discussing an addition to the freedoms here, not (yet) how
this may be reflected in licensing. That's a discussion that is much more
difficult, especially if one is not a copyright lawyer. But this also gives us
a lot of freedom in formulating such an addition.

I would, in the end, lean into Marx's model of reciprocity and formulate an
overarching right like this:

> Every participant in the FLOSS ecosystem has the right to have their needs
> met according to the abilities of the other participants.

Note that if applied in a licensing scheme, this strengthens the contributor's
right, in that they can demand some kind of financial and infrastructure support
in exchange for their labor.

It also strengthens the end user's right to demand that their needs be met. A
well-funded project then has little excuse not to fix bugs, improve stability
and documentation, or perform other bothersome tasks.

Finally, it also strengthens the corporations' ability to extract value from
FLOSS software. I think this should address what corporations fear with
copyleft licenses, that they lose this ability.

But all three participants are also required to feed into the ecosystem that
which they can, in order to keep the wheels turning.

It is not OK to ask FLOSS contributors for labor without supporting them,
directly or indirectly.

It is not OK to ask users to submit well crafted bug reports and user stories
without also deriving from those work packages that address their needs.

It is not OK to ask FLOSS contributors or users to supply labor, if it is not
compensated either by financial or infrastructure contributions.

---

Above, I have tried to address the fundamental issues plaguing the FLOSS
ecosystem by re-examining the core tenets on which it was built, and adjust
them to the current age.

We no longer live in a world where FLOSS is *rare*, a novel idea. Instead,
we live in a world where it is indispensable. To take it away would be to
regress by decades.

So rather than carving out a niche for FLOSS, its core tenets should be about
sustainability. Support, and continue supporting, what is already there while
permitting new projects to thrive.

I doubt that this blog post is the contribution to this debate that resolves all
issues. But I hope it provides a direction forward. Too much of the debates
that I witness circle around ideals made for a set of circumstances that are now
lost in time.

We need to look forward instead.

## Update 2024-04-25

There is an adjacent conversation going on these days that I think is relevant
here, which is whether it is ethical to use FLOSS code in LLMs, and under which
conditions. {{< external text="This thread" url="https://chaos.social/@onepict/112330985911516070" archive="force" >}}
by {{< fedi "@onepict@chaos.social" >}} makes an excellent entry point into the
debate.

On the one hand, the four freedoms clearly allow this. On the other, folk such
as {{< fedi "@abucci@buc.ci" >}} state:

> By my read FOSS freedoms do not cover the case of large scale agglomerates of
> codebases for the purpose of generating more code that could lie in competition
> with the constituent codebase. That's new, and should be treated as such.
> Complacency is not an appropriate response.

- On the one hand, it's worth understanding that copyright law can be interpreted
  to permit this. If I read source code, and write similar source code from memory,
  this isn't a violation of copyright -- unless the input and output are so similar
  that it's difficult to distinguish from a straight copy & paste job.[^copyright]
- On the other hand, comparing this human endeavour to a LLM is questionable, if
  only because of what the above quote states: this could allow a competitor to
  effectively fork a code base without copying anything verbatim, and then claim
  rights on the result. That would certainly set a bad precedent.

[^copyright]: IANAL -- but I grew up reading the SCO vs. Novell and SCO vs. IBM
saga on {{< external text="Groklaw" url="http://www.groklaw.net" archive=false >}}, and that
argument was extensively raised and discussed in the course of this.

While I still think that the above formulation of a right in addition to the
four freedoms can be improved significantly, I do think that the spirit of it
does prevent the second kind of scenario -- or rather limit its damage by having
positive side effects.

If the licensing scheme on code requires that all participants in the FLOSS
process benefit, a company engaging an LLM to generate competing code clearly
demonstrate its ability -- and so its obligation to contribute back. 

A firmly reciprocal license should not prevent use in an LLM per se, but
specifically ensure that there is no path by which a company can
use an LLM to generate code in competition to its inputs, and without paying
the authors of its inputs appropriately.

Such a contribution should be measured in the person hours they saved in using the
LLM -- by applying something like {{< external text="COCOMO" url="https://en.wikipedia.org/wiki/COCOMO" archive=false >}}
to the inputs.

I suspect that would effectively prevent these cases. And if not, be a boon to
FLOSS projects.

## Update 2024-08-28

The dicussions following this post have pointed me to
the {{% extref post_open %}} published by Bruce Perens just a month prior to this post. I have now had time
to read it.

I think he's off to the right start, in that he differentiates between a license
and a contract. Technically, the moment you add a right to the four freedoms as
I have done above, you've moved away from license agreements to full-blown
contracts. What he's clear about in the "license" text is that, once signed, the
text becomes a contract -- and foresees a public repository for such signed
contracts.

The rest of the license text does outline obligations various parties have to
each other under such a contract, which brings things a little closer to what
I have in mind here.

What I do not see, however, is that every party is identified in the same way
that I have tried above. As a result, I'm not sure the mutual obligations are
actually quite the right ones.

Further in this vein, there are some thresholds and rights embedded into the
text that I'm not sure every project would wish to undersign. For example,
the contract explicitly allows machine learning uses -- many FLOSS developers
I've sponen to are fundamentally opposed to this.

In my mind, what's needed is more of a *framework* than a single license body;
something more akin to the {{% extref cc %}} where one can
relatively easily pick and choose terms. In particular, the revenue threshold
at which the free license becomes a paid license is not something I would place
into the contract body, but into an addendum document that can be updated
independently.

But of course it must be noted that Perens explicitly positions this as a *draft*,
open for discussion. And as a draft, it is certainly much closer to the thing
we need, as opposed to the things we have.

## Update 2024-09-25

A slightly open-ended update of this is that yesterday's event {{< external
text="Towards European Digital Independence - Building the Euro Stack"
url="https://www.youtube.com/watch?v=RR2X_rZ_zbo&ab_channel=GreensEFA" archive=false >}} by
the European Greens/EFA triggered a discussion in the {{< external
text="Digital Commons Policy"
url="https://matrix.to/#/!ArwufDRELNfrhKkopj:matrix.org?via=matrix.org&via=utwente.io&via=openrightsgroup.org" archive=false >}}
Matrix room.

No, let's be fair: *I* was triggered by watching it, and some comments and responses
triggered me further. So I tried to provide something of a summary of this post,
with a specific focus on the funding and legal landscape in *Europe*.

The interesting question was raised: what can the state do to contribute
positively to the entire sitation?

It's a fair question! I did not address this in the above!

But first, a bit of context.

Part of the push in Europe towards getting more recognition for Open Source (not
so much FLOSS) is about [digital sovereignty]({{< relref "knowledge_base/digital_sovereignty" >}}). Specifically, both the political
and regulatory landscapes make it increasingly difficult to outsource digital
services to overseas corporations which may fall under a different legislatory
regime.

As such, the main political interest within Europe in FLOSS comes from this
angle of seeking more independence! But governments and institutions are what
they are, so the other parts of this is that a) they would love to transition
to local companies as service providers, and b) have as little effort in this
transition as possible.

The long and short of the story is that while this is very much an effort that
benefits *parts* of the Open Source community, it is very much that part that is
backed by corporations that can provide the above services.

And one of the results of *that*, in turn, is that the support for FLOSS
projects you might find emerge for this is very much like the {{% extref stf %}}
in Germany,
namely primarily focused on supporting the ongoing maintenance of those FLOSS
packages that the public sector already relies on.

It's worth highlighting that these efforts are incredibly valuable, before
rasising a "BUT".

But... another recent news item is that the {{< external
text="EU is cutting funding for the Next Generation Internet"
url="https://fsfe.org/news/2024/news-20240719-01.en.html" archive="force" >}} initiative, or {{% extref "ngi" "NGI" %}}
for short. NGI in turn, in particular through the efforts of the {{% extref nlnet %}}
has been focusing on the
other side of the coin. NGI appears to be (have been?) the only substantial
fund in Europe to look at what the wider FLOSS community needs, or can do in
terms of bottom-up, greenfield innovation.

The thing that triggered me is that from the discussions at the event, via
e.g. the {{< external text="position paper on funding public digital infrastructure"
url="https://openfuture.eu/publication/investing-in-public-digital-infrastructure/" archive="force" >}}
from {{% extref open_future %}}, this side
of the coin is conspicuously absent.

And so I repeat what I wrote about in April, namely that it seems the current
system not only is brittle, but headed towards collapse -- eventually, at least.

In the context of the ensuing discussion, the question I posited earlier was
raised: what can the state do within a revisited framework of four freedoms
and a right?

Now first, the needs and abilities of any organization can be seen as largely
equivalent, whether private or public.

But what the state has in addition over corporations is the obligation and power
to protect its citizenship, and so the digital commons as a good of and for the
citizenship.

I suggested that in my humble opinion, maintainers' and contributors' actual
legal rights are sufficient. I am not sure this is 100% correct, and am happy
for feedback here.

But I see a larger issue in the fact that *access* by individual contributors to
resources they need is limited and gated. It starts with something like NGI
funding in addition to the STF-style funding. It continues to the perennial
question of how to enforce licensing terms against misbehaving corporations
when doing so requires extensive funding in its own right.

The upshot is that *I think* the state can contribute better than any other
participant in the ecosystem the kind of easy to access infrastructure --
technical as well as legal and financial -- that permits even individual
maintainers to defend the commons. Not exclusively, but in particular also
from corporate value capture.

What precisely that might look like in practice, I am not sure yet. But I hope
I have inserted myself enough into that discussion to at least trigger some
thoughts on this.

Again, I'll update this with feedback as I receive it, by all means, please
let me know what you think!
