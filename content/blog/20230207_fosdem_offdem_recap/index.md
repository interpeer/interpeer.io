---
title: "FOSDEM'23 and OFFDEM O3 Recap"
date: 2023-02-07T15:00:00+00:00
author: Jens Finkhaeuser
categories: ['conferences']
image: interpeer_fosdem.png
tags:
- Conference
- FOSDEM
- FOSDEM23
- FOSDEM2023
- OFFDEM
- OFFDEM O3
- FOSS
- vessel
- eris
- hypercore
---

Unfortunately, in 2023 there were no Interpeer related talks at {{< external
  text="FOSDEM" url="https://fosdem.org" archive=false >}} -- after a few years of virtual
conference, FOSDEM had a large number of talk submissions to deal with, and
ours did not make the cut.

However, FOSDEM is not just about talks -- it's often more about the
meetings in the hallways and gathering spots. And so Interpeer was still
present at the conference.

{{< fig src="interpeer_fosdem.png"
    alt="Interpeer at FOSDEM"
    width="100%"
>}}

It won't be useful to summarize all the different conversations at the
conference. I do want to highlight one I've had in the unoffical fringe of
FOSDEM, however, at {{< external text="OFFDEM"
  url="https://foss.events/2023/02-04-offdem.html" archive="force" >}}.

Here, {{< fedi "@pukkamustard@chaos.social" >}} was presenting {{% extref "eris" "ERIS" %}},
while I gave a recap of [my IETF115 presentation of vessel]({{< relref "20221108_ietf115_vessel_and_web_centralization" >}}).
We were comparing file formats for distributed storage (and, consequently,
distribution). In addition, a developer of {{< external text="hypercore"
  url="https://github.com/holepunchto/hypercore" archive=false >}} gave a view from that
project's perspective. A few computer science students weighed in with questions
and remarks.

The result was probably one of the best discussions around these topics that
I've had to date. It was a true pleasure diving into the pros and cons of the
different approaches for various use cases.

---

Meanwhile, {{% extref "nlnet" "NLNet" %}} had made some
nifty hexaognal stickers for all the projects they support, including of course
Interpeer. The stickers did rather well, judging by how few or many were left
afterwards, and some have been spotted in the wild.

I still have not processed FOSDEM or OFFDEM entirely, but I did want to share
these two small highlights.

It's been good!
