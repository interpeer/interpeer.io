---
title: "Human Rights on the Internet"
date: 2022-03-16T19:00:00+00:00
tags: ['Interpeer', 'IRTF', 'IETF', 'Human Rights', 'Internet']
author: Jens Finkhaeuser
categories: ['miscellaneous']
---

There is an {{< external
  text="ongoing discussion on human rights on the Internet"
  url="https://mailarchive.ietf.org/arch/msg/hrpc/O9OyvXaJQYYObKNFZ5OMwFeLJqg/"
  archive=false
>}} on the IRTF HRPC mailing list that I want to express an opinion on.

I would also like to stress that this is not an official position of the
Interpeer Project. Although we are yet small, there exists already a variety
of positions amongst contributors on all kinds of topics. No, this is a
purely personal opinion.

For context, the Internet Research Task Force (IRTF) is a sibling to the
Internet Engineering Task Force (IETF) that does not get a lot of publicity.
While IETF concerns itself with standards on interoperability of current
networks, IRTF looks further into the future. The HRPC list concerns itself
with Human Right Protocol Considerations, i.e. any topic that relates to how
protocol design work may impact or support human rights.

Since the Interpeer Project aims to help bring about a human centric Internet,
this list is of utmost interest to me. As a consequence, of course I brought
myself into the discussion.

The discussion topic is censorship, and ties into current political events. As
a result of the Russian Invasion of Ukraine, Ukraine has asked the international
community for help. One such request was to shut down (parts of) the Russian
Internet as part of their cyber defence.

The discussion was sparked by a paper (linked in the first post) authored by
some noteworthy Internet related organizations on how various technical means
for sanctioning Russian access to the Internet do or do not help or hinder,
well, "things" in general. But it also concludes that some technical means
pose an acceptable risk and could be implemented. They boil down to publishing
blocklists.

This, of course, is perceived as censorship and overreach by some parties, and
so the discussion ensues.

In my personal estimate, it is important to emphasize a few things here that
may get lost in the overall debate.

## Goals

Any party that has so far contributed to the discussion shares the same goals.

1. One goal is to help reduce harm to people, which mostly refers to physical
   harm.
1. Another goal is to preserve freedom of access to the Internet; this is in
   some ways identical to preserving the Internet as a network of networks.

It's worth highlighting that there is no disagreement on these goals. I think
it's also of interest that these goals align with the {{< external
  text="Universal Declaration of Human Rights"
  url="https://www.un.org/en/about-us/universal-declaration-of-human-rights"
  archive=false
>}}, in particular Articles 3-5 and 12 (and/or 19) respectively.

This entire discussion, then, is the process of balancing two human rights
against each other, where none of them has more value than the other.

## Positions

In the discussion, one can also see a few positions crystallizing in opposition
of blocklists. I may not be able to provide a complete or fair list below, but
some opposing positions include:

1. Human rights concerns are the purview of politics, not technology folk.
1. Anything technology folk do can be used or abused by politics.
1. Politics has a tendency to use technology for political advantage without
   understanding it.

This leads some folk to the following conclusion: if politics uses technology
without understanding, that constitutes abuse of technology folk's efforts, so
in order to preserve in particular the freedom of access to the Internet, it's
best for technology folk no to undertake efforts that can be abused in this
way.

In and of itself, that is a fair assessment. But it also leaves the decision
space entirely to the people who are *not* experts on Internet technology. I
cannot see how this will in practice help preserve anything about the Internet's
best aspects.

The paper under discussion takes an entirely different position: it starts from
the point of view that decisions about the goals of the Internet cannot be left
to politics alone, so tech folk must engage with politics. If politics demands
some kind of sanctions being enacted, then the best route is to provide politics
with the choice of sanctions that do the least damage possible, and actively
preserve that which needs to be preserved.

## Martial Arts

Either of the above position is a defensive position. In martial arts, depending
on the style involved, one will be taught two types of defensive moves when
contact is unavoidable, *blocking* and *deflection*. In practice, the two are
not as clearly separated as one might think, but the categories serve an
important purpose for illustration.

When *blocking*, the force of the attack is effectively absorbed. What makes
the block effective at averting damage is that the block occurs at a site of
the defender's choice, where the kinetic energy may more readily be absorbed.

On the other hand, *deflection* aims at redirecting the force of the attack
elsewhere, so that - ideally - no part of the defender's body needs to absorb
any kinetic energy.

Either form of defence is valid in and of itself.

There are, however, reasons why *deflection* is the superior choice in the
long term. One is that any absorption of an attack's energy eventually
exhausts the defender. It's best to avoid this, though this can also be
weathered with sufficient stamina.

The more important reason is that the kinetic energy of the attack does not
stop when it is deflected; it has to go somewhere. This either leads the
attacker to overextend themselves, or to exert effort in pulling back. In either
case, the attacker becomes more vulnerable themselves, if only for the briefest
moment.

I contend that when engaging with politics to create sanctions, this is essentially
a *deflective* defence against attacks on freedom of access to the Internet. It
makes such attacks easier to withstand by sapping less energy, and makes the
attacker more vulnerable.

Not engaging on the grounds that it violates some principles, by contrast is
a *block*: it requires civil society to absorb and re-absorb over and over
again the same kinds of attack until the defence is ground to dust.

## Process and Principles

The above can only hold true if engaging with politics occurs in just the right
way. Finding this form is a thoroughly difficult task to begin with, and to make
matters worse, any approach once discovered must necessarily adjust as the
political landscape changes.

I strongly suspect that proponents of the *blocking* kind of defence fear this
difficulty. I also think they're right to do so. But I do not agree that this
fear is a good reason not to try anyway.

What precise form this should take is beyond me as an individual. But I can
offer some insights into what qualities it should have.

1. The principles it must be anchored in should not be attackable by politics.
   I refer to the UDHR above for a reason; while not every nation on this
   planet has ratified it, it is nevertheless the least contentious law amongst
   the community of nations, and attacking it is treated as an attack on all.
   While this does not rule out such attacks, it ensures backing.
1. If we learn anything from safety engineering, quality assurance, etc. it is
   that humans are fallible and machines stupid. The latter point means that any
   process surrounding a blocklist cannot be safely automated, but needs human
   eyes upon them. The first part means that no single human should be in charge
   of providing those eyes.
1. From the above follows that a diversity of opinions - from multiple
   stakeholders involved in the process - provides the best safety net against
   abuse of such mechanisms.
1. People are prone to forgetting what they did, which means any process for
   creating blocklists must contain provisions for periodic review. Such review
   must not default to keeping a sanction in place, but the sanction must be
   defended anew at every renewal.

In summary, additions to a blocklist must essentially be discussed and voted on,
as such are the mechanisms of multi-stakeholder consensus building. But I would
add that any implementation of blocklists should also require much the same, or
we otherwise invite backdoors for abuse or laziness. In short, it should be made
illegal to implement the list without undergoing some kind of review process
again.

This does not, of course, prevent any implementor from creating a list with the
same items and short-circuiting the process in that fashion. However, this
cannot be done with the approval of the blocklist's originator, and therefore
raises the question why a multi-stakeholder process firmly entrenched in human
rights considerations was circumvented in the first place.

This deflects the attack back onto the attacker.

## Effectiveness

Criticism of blocklists in general are mostly discussed in the beginning, but
one specific criticism remains: that of effectiveness. As some commenters wrote,
a blocklist is likely going to be either overly broad and damaging, or ineffective.
By introducing a slow and considered approach to populating the list as hinted at
above, one may achieve a less damaging scope, but simultaneously render the
list less effective.

This is true, and somewhat by design.

The criticism rests upon a false dichotomy between the goals mentioned at the
outset. While it is true that blocking some particularly bad actors will reduce
harm to people, more harm will be averted by not blocking everything else.

That is, the list should not be an effective shield against everything. No life
can thrive in a lightless bunker. Instead, it should aim only to avert the worst
disasters, and let through light and nourishment of any kind.

High effectiveness in protection is a false goal. High effectiveness in
balancing human rights against each other is the true aim.

This may make such a blocklist a somewhat symbolic gesture. This, too, has been
raised as a criticism. At which point I have to ask, which symbol has *not* led
to change? The answer is, of course, none of the ones that you can remember.

## Conclusion

As I wrote at the outset, none of the above is an official Interpeer Project
opinion, it is mine alone. Of course as the initiator of the project, I will
undoubtedly wield undue influence over it for the time being. I hope that this
will change in time, because I am far from infallible.

I stress this because I do *not* believe that any part of the Interpeer software
stack to come should default to implementing some kind of blocklists, whether
arrived at by a well-defined process in deference to human rights or otherwise.

I *do* believe that in order to create a human centric Internet, such decisions
have to move closer to the end-user. It should not be anyone else that decides
who to block and why, but every user for themselves.

This point of view makes it all the more important that we can leverage the
wisdom of others. A blocklist published by others is the embodiment of such
wisdom; they tell us that "here be dragons". Such a blocklist transparently
and carefully curated under strict human rights considerations is almost
certainly going to be a better choice than any random blocklist off the Internet
(though it should by no means be the only choice).

Which means the creation and publication of it is something of a necessity in
the same way that the implementation must ideally be each individual's choice.

I cannot predict whether the discussion above will lead to any such blocklist,
or whether the criteria applied to creating it satisfy my needs, either the
ones outlined above or others yet to be discovered.

But I can say with absolute certainty that I would much rather have a disagreeing
body of folk all concerned with human rights grudgingly and publicly create one
than leave this entirely in the shadows, determined by politics or corporations.

In that sense, I applaud the effort put into the paper that sparked this
discussion. It's the first step in dragging these things kicking and screaming
into the light, where they belong.
