---
title: "CLT'23: Distributed Authorization with CAProck"
date: 2023-03-12T17:30:00
tags: ['Interpeer', 'Conference', 'Talk', 'Chemnitzer Linux-Tage', 'Linux-Tage', 'CLT', 'CLT2023', 'CLT23', 'Linux Days']
author: Jens Finkhaeuser
categories: ['conferences']
video: https://makertube.net/w/e2TKcB1PuQ3os2pCjjvSmE
summary: |
  This year, we were presenting at the Linux Days in Chemnitz. The talk was about how
  to achieve full distribution with authorization in networked systems -- of course
  using CAProck.
  The slides are in English, but the presentation itself was held in German.
aliases:
- /blog/2023/03/linux-tage-chemnitz-verteilte-autorisierung-mit-caprock
- /blog/2023/03/linux-tage-chemnitz-2023-verteilte-autorisierung-mit-caprock
- /blog/2023/03/linux-days-chemnitz-distributed-authorization-with-caprock
- /blog/2023/03/linux-days-chemnitz-2023-distributed-authorization-with-caprock
---

This year, we were presenting at the Linux Days in Chemnitz. The talk was about how
to achieve full distribution with authorization in networked systems -- of course
using [CAProck]({{< relref "projects/caprock" >}}).

The slides are in English, but the presentation itself was held in German.

{{< peertube id="698a6f6b-7799-4160-a767-5621a8b43c1e"
  title="CLT2023: Verteilte Autorisierung mit CAProck"
  >}}

Dieses Jahr waren wir mit einem Talk auf den {{< external
  text="Chemnitzer Linux-Tagen" url="https://chemnitzer.linux-tage.de/2023/en/programm/beitrag/139"
  archive=false
>}} praesent. Hierbei ging es darum, wie man Autorisierung in vernetzten System auch
vollstaendig verteilt, ohne den Single Point of Failure eines zentralen Servers
hinbekommt -- natuerlich mit [CAProck]({{< relref "projects/caprock" >}}).

Die Folien sind auf English, aber der Vortrag war auf Deutsch.

