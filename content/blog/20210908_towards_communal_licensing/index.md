---
title: Towards Communal Licensing
date: 2021-09-08 10:48:27.314000+00:00
tags: ['licensing', 'community', 'foss', 'permissive', 'copyleft', 'communal']
author: Jens Finkhaeuser
categories: ['miscellaneous']
image: community_center_room.jpeg
---

There's something rotten in the state of FOSS, and it's not the software. It's
the community -- or more precisely, the communal spirit.

This post deviates a little from the regular topics. I do not intend to write
a lot on this topic here, unless you, dear reader, provide me with feedback
that it fits your interest. Consider it an experiment while work continues on
implementing the protocol suite as usually discussed here, and there are fewer
technical updates to write.


{{< fig src="community_center_room.jpeg"
  link=""
  rel="external"
  target="\_blank"
  width="100%"
  alt="Community Center Room 2"
  caption="[\"Community Center Room 2\"](https://www.flickr.com/photos/30326207@N00/3643938531) by [Timm Suess](https://www.flickr.com/photos/30326207@N00) is licensed under [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)"
>}}

## F(L)OSS -- What is it?

Many of my readers will be able to skip this section, but I do not want to lose
those who can't. The thing that is commonly described as "Open Source" in the
media actually comes in two very distinct flavours.

On the one hand, there is "Open Source" itself -- so called because there was a
need to distinguish it from its predecessor on the other hand:
"Free/Libre Software".

* Free/Libre Software's goal is to give the user {{% extref "4freedoms"
    "certain freedoms" %}} on
  what to do with the software. These freedoms include the ability to modify
  the software and share copies of the modified work.
* Open Source's goal is much simpler: it's aim is to improve code quality by
  making it accessible to a larger community of developers or reviewers, often
  summarized as {{< external text="Linus' Law: given enough eyeballs, all bugs are shallow"
    url="https://en.wikipedia.org/wiki/Linus%27s_law" archive=false >}}.

In practice, the licenses used in each type of software project differ. Free
software uses so-called {{% extref "copyleft" "copyleft" %}} licenses, which use
copyright law to force derived works to adopt the same or compatible licenses.
This is sometimes described as a viral license.

Open source tends to use so-called "permissive" licenses, because they are not
all that different from giving something to the public domain. Other than
expressly giving no guarantees for bug-freeness or use of any kind, there tends
to be very little text to the license terms of such licenses.

## Deeper Reasons for the Split

The FOSS community tends to be split on which kind of license is preferred.
Often, people opt for open source/permissive licenses because it rids them of
having to consider intellectual property law and license terms in the first
place. And while I disagree with that point of view, it's their decision to
make.

But when you dig deeper into the background to the split, and recall -- as I
do -- the public discussion around it, it becomes very evident that one
particularly driving reason for creating open source license was business:
it's very difficult to charge for the use of software when users have the right
to download the code and build the software themselves.

To be sure, copyleft licenses are not at all anti-business. They just require
businesses to find different revenue streams from simple license agreements.
Those might be consultancy related to the modification of the software, or
hosting fees, etc.

This requires a shift in mental models, though, that not everyone is prepared
to make. In particular it requires shifting away from treating ownership and
control of (intellectual) property as the primary means for generating revenue
towards more of a service economy point of view.

We'll get back to that later. For now, just keep in mind that to anyone *not*
prepared to make that shift, copyleft licenses must seem toxic to business
needs. Consequently, these licenses tend to be shunned by a fair number of
businesses. This, of course, only serves to fuel the more philosophical debate
of whether code quality or user freedoms should be valued more.

## The Problem

Let's take a quick look at a particularly problematic example of a business
practice in this space, by looking at Docker (I won't link there out of respect).
Docker is a software that lets you package and then run other software in a way
that keeps it in relative isolation from other software you might be running.

The reasons for doing so range from managing complex infrastructure better to
having more security through this isolation. But none of that is the point here.

A quick and very rough timeline of events goes like this:

1. The main docker software was started and built with a permissive license.
1. This license also permitted the company to build products surrounding this
   main docker software.
1. Since both the main software (and the products) were useful, they attracted
   a lot of users. In particular, they attracted a lot of community
   contributions to the main software.
1. The main product offerings were offered free to use to other FOSS software,
   in particular those that permit for easy packaging and distribution.

So far, it's a win-win situation, right?

And because it was easy for FOSS to be packaged and distributed in this way, it
evolved into a quasi standard -- which is the technical point of view. The
business point of view is that it evolved into a quasi monopoly.

A monopoly which Docker the company quickly exploited, by restricting access to
those product offerings most useful to FOSS projects.

Of course *legally speaking*, this is perfectly fine. In the abstract, it also
makes sense from a business perspective to reap license fees from the most used
parts of your offering.

But it also screws completely with the larger FOSS community, much of which
operates on a volunteer basis. Understandably, a lot of developers are angry at
the company for on the one hand benefiting from the community's contributions,
and on the other hand, not providing anything back.

## Analysis

For copyleft proponents, this is good fuel. It's a great story of something
that copyleft would have prevented. Which is true, but perhaps not for the
reason they intend: a copyleft licensed software would have been harder to
integrate into proprietary products in the same way. It's far more likely that
copyleft would have prevented the success of the software than that it would have
prevented the exploitation of the community.

Listening to voices from the community, it seems as if there is a growing
dissatisfaction with how neither copyleft nor permissive licenses can address
this well. There is no particular reason, after all, to be anti-business (either
in practice or in the minds of people), as long as the business plays nicely.

Let's get back to the distinction between treating property or services as
revenue engines. There is a relation here to politics, and I'll quote {{<
  external text="the Wikipedia article on \"The Dictator's Handbook\""
  url="https://en.wikipedia.org/wiki/The_Dictator%27s_Handbook" archive=false >}} here:

> The main difference between the scenarios of democratic and authoritarian
> politicians is that democratic politicians have to please a large number
> of power brokers and/or the public at large while authoritarian ones please
> relatively small circles. These differences are illustrated in the
> infrastructure developed in authoritarian and democratic societies.

One of the relationships that emerges is that if the authoritarian regime has
easy access to natural resources, then the incentive to please the public is
very low. It is far easier to distribute these natural resources amongst a
small group of supporters than to invest into the public at large. Conversely,
more democratic regimes tend to be associated with service economies.

Although the open source movement is far from having authoritarian roots -- one
of the most visible supporters in its beginning is also known to have fairly
strong US-style libertarian views -- there is a rough analogy at play here. As
intellectual property is a kind of resource that can be directly exploited,
having control over such a resource (with a certain value) means there is less
incentive to invest into the community.

Perhaps it is time to re-evaluate the old split between copyleft and permissive
licenses and philosophies, and zoom in more on how they focus on *people versus
property* respectively. This should help lead to an alternative path that
balances the two views better.

## A Path Forward

One of the realisations from this change of perspective is that hardly anyone
is opposed to economic activity on principle. Instead, most of the
dissatisfaction is focused on when property control becomes excessive to the
clear detriment of people. Ideally, there should be a feedback loop that
requires some form of compensation to the public if a property is exploited,
and such compensation should be commensurate with the amount of exploitation
effected -- the more you make, the more you need to give back.

While intellectual property laws certainly would permit for such provisions in
a software license, there are some issues, practical and theoretical, making
this difficult.

The first is that not all exploitation should be handled in this manner. The
four freedoms of free software should really continue to apply -- benefiting
from the fact that you can modify and redistribute software doesn't need
addressing as such.

The second is that any kind of discrimination in a license violates articles
5 and 6 of the {{% extref "osd" "Open Source Definition" %}}.
In principle, not permitting
discrimination is a good thing, but the wording of these articles is so
libertarian that one cannot even discriminate against groups or fields of
endeavour if such discrimination is for the public good, directly or indirectly.
I couldn't ban Nazis from using my software, nor charge only one-percenters.

This unfortunately matters greatly.

Both the Open Source Initiative which authors this definition, as well as the
Free Software Foundation which has the four freedoms at its core maintain lists
of licenses that they approve of. Furthermore, the complexity of software supply
chains makes it necessary (and often a legal requirement) to be able to
automatically check large bodies of code from many disparate projects for
violations of license requirements. This in turn leads to the maintenance of
license lists and the specification of license metadata and tooling such as
undertaken by {{< external text="SPDX" url="https://spdx.dev/" archive=false >}}.

In short, if neither OSI nor FSF approves your license, it's unlikely to be
very successful.

The third is that a license is far more likely to be accepted if it is easy
to understand. Assuming the terms of your communal balancing are acceptable
enough, either such that OSI and/or FSF can agree with them, or that "Communal
License" can become a third pillar, this will only happen if developers adopt
these conditions. And that means the target audience for such terms has to be
the development community.

A fourth and more subtle one is that it's actually good to foster commercial
adoption of such a license. The only thing that's bad is unfettered
profiteering. One might object to profit-making on principle, but as long as
the profit-making fuels the adoption of the software in some form or another,
it's beneficial for the community at large.

Do I have a suggestion?

Kind of. Not really.

Because the problem is complex, anything I can write here is at best a starting
point, a collection of thoughts without coherent structure.

1. To emphasize the *people* aspect, I would opt with a well known copyleft
   license to begin with, but add exceptions to it. This is in itself common
   enough practice. Such exceptions typically exclude certain use-cases of
   software from the viral nature of the copyleft license.
   In our case, the exception would be more of an exclusion, listing conditions
   under which the copyleft license cannot apply.
2. If the copyleft license does not apply, it may be that no license is granted
   at all, or a different license must apply. To set the tone and emphasize the
   ethical nature of the scheme, I would simply not grant any license at all to
   people, groups or fields of endeavour that violate the {{< external
     text="Universal Declaration of Human Rights"
     url="https://www.un.org/en/about-us/universal-declaration-of-human-rights"
     archive=false
   >}}.
4. Other exceptions from the copyleft license should be required in some fashion
   to give back to the community.
   One choice here could be to issue a commercial license that is only valid in
   conjunction with a donation receipt signed by the copyright holder. In this
   way, the copyright holder can with relatively little overhead ensure that
   some form of give back has occurred. It would be a lot easier to have a fixed
   value, or publicly visible scheme of profit percentages. This, of course,
   complicates things further.
5. An alternative to the donation scheme above would be simply to have to pay
   license fees to the copyright holder. But in order for this to be a communal
   thing, the copyright holder would have to be some kind of public interest
   entity. While public interest entities can receive donations (details depend
   on legislation, I'm not going into that), when a payment is made in order to
   receive a particular benefit, it typically no longer classifies for the tax
   benefits of a donation. This, then, can also be seen as a way to clarify the
   donation scheme and turn it into a simple license payment in the special case
   when the copyright holder is a public interest entity.

The problem with these suggestions is, they're already straying pretty far from
simple terms. I can't say I'm very happy with that aspect. But they do, at least
in spirit, capture the idea that excessive profiteering is avoided by also
benefiting the public.

What do you think?
