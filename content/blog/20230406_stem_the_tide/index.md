---
title: Stem the Tide
date: 2023-04-06 09:00:00.000000+00:00
tags: ['interpeer', 'motivation', 'digital sovereignty', 'privacy']
author: Jens Finkhaeuser
categories: ['miscellaneous']
image: "bucket_spade.jpg"
---

{{< fig src="bucket_spade.jpg"
    alt="Bucket & spade"
    width="100%"
    link="https://www.flickr.com/photos/65032901@N00/2079882809"
    rel="external"
    target="\_blank"
    caption="[\"Bucket & spade\"](https://www.flickr.com/photos/65032901@N00/2079882809) by [Dale Gillard](https://www.flickr.com/photos/65032901@N00) is licensed under [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)"
>}}


The other day, I was asked by a friend what it is I'm doing with this project.
He's very much into following technological trends, but not a deeply technical
person himself. That drove home yet again how hard it is to provide an
"elevator pitch" summary of our work.

When I speak about a "human centric" Internet, what I mean is a digital place
where human rights are protected, and human needs are met. Now most people will
agree that this is a worthy goal, but almost certainly will still ask "so what
do you mean by that?"

And that's the problem.

Because what this means depends strongly on your own experiences. If I say "a
place where you won't be persecuted or harassed" and you happen to be a
middle-aged white man, chances are good that your reaction is that this
already exists, pretty much everywhere you go.

If I tell you it's about communicating in privacy, without companies or
governments spying on you, the answer I hear most is "I've got nothing to
hide". Which may be true, but misses the point. You only have to hide that
which people might use against you -- and that can change without you ever
changing your behaviour.

If I suggest it's a way of ensuring [digital sovereignty]({{< relref "knowledge_base/digital_sovereignty" >}})
in a fundamental way,
that might at first interest some politician -- until they realize this is not
a solution we can roll out immediately, but longer term research.

I could go on.

Here's the thing: people are very good at making do with what they have. We
can slap together a solution to any of the issues above, and any of the ones I
did not mention, in a relatively short period of time. And it'll be good
enough. So why bother with anything else?

Because that only wins the game. It doesn't win the tournament.

Because the tournament isn't about providing a safe space for some group, or
allowing your family chat to remain with your family, or liberating one city
government from having to send their data overseas to use digital services
within their budget range.

Well, I lie. Technically, the tournament is about that. But it isn't about any
single one of those things, but about all of them together.

The simple truth is that we're not winning the tournament.

It's not particularly hard to understand why, but it is very easy to lose sight
of those reasons: with every "game" we compete in, we're starting over.

In every single instance, we're asking slightly different questions -- what to
protect and from whom changes. But the tools in our toolbox are always the same.

1. We need to identify people, but we also need to allow them to stay as anonymous as possible.
1. We need to give the right people access, and only the right people.
1. When it comes to protecting data, we can only encrypt/decrypt and sign/verify stuff.

See, winning the tournament isn't about winning each game we tackle. It's about
winning enough games to make a difference. So if we have to go through the same
process over and over to identify risks and actors in threat scenarios, but the
solutions always look suspiciously similar, that takes a lot of energy away from
tackling the next game.

We're trying to hold back a tidal wave, and we have a bucket. And because we
have a ton of friends, and each of them brings their own bucket, we think we can
do this -- when the real solution would be a dam.

So what is it that we're doing? We're trying to build that dam.

Building something as comprehensive as that is complex. As I pointed out above,
we only have a limited set of tools to build this. So the majority of our work
is about figuring out how to recombine the tools we know in a way that addresses
all of the threat scenarios we're aware of.

We're not the only ones doing this kind of work. But it turns out that when you
look at other proposed solutions, they often neglect one aspect of the whole in
order to reach their goal faster.

My go-to example here is video streaming, such as for a video call with your
kids when you're away on a trip. Does this exist already? Totally!

For now, though, your video will travel from your device A through some
company's server S to your kids' device B.

Is this secure? Well, hopefully the company followed best practices, in which
case they encrypted the traffic from A to S, and the traffic from S to B. This
is called transport encryption, and luckily most companies employ this nowadays.

But the server S? It can read your video. It can create a copy of it, and do
with it whatever the company wants.

So we need something better than that. We need end-to-end encryption, where the
video gets encrypted from A to B, and the server S only sees the encrypted data.
Does that exist? Yes, a few companies offer this.

But the company will know that A called B for some amount of minutes. Malicious
actors can still scan this metadata and create profiles on you that they can
exploit.

So ideally, you'd want the company that owns S not to know that A is you, and B
is your kids. Does that exist? Yes, sort of...

Yes, completely anonymous systems do exist. But do they do a good job at
streaming video? Most do not.

And if it's not your kids you want to call, but your parents? They probably
have a different device, and the service you're barely using to keep your
immediate family safe won't work there. That's the next game.

The tournament isn't about creating a single platform for secure, anonymous
video calls. The tournament is about making the entire Internet's approach to
video calls secure and anonymous by default. And to phone calls. And to
messaging. And to sharing cat pictures. And to work from home with remote
colleagues on the same document. And to...

To do this well, it turns out that you have to re-think how things are done,
and also challenge the current norms somewhat. Technology works at different
layers of abstraction, and each layer has to fit the other layer well in
order to achieve an overall goal. In many cases, the layers are very well
chosen -- but in places, they could work a little better together.

The good news is: we think we've identified a way to do this.

The bad news is: we have to finish building it. We have to test it, to prove
whether we're right. We have to document every aspect of it. We have to reason
about why it works. We have to present those proofs and reasons to the entire
technology community. And then they will have to agree that our solution works
for them, too.

Only when that is achieved can we win the tournament.

Is this hard?

Oh... you have no idea. Every day I wake up and ask myself if I've completely
lost it. Why did I start this again?

But... there are only two possible answers to this problem.

We either grab our little bucket and hope for the best. Or we roll up our
sleeves and start digging.


