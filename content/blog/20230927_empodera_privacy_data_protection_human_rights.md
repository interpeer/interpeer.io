---
title: "EmpoderaLIVE'23: Privacy and Data Protection as Fundamental HUman Rights"
date: 2023-09-27T21:00:00+00:00
tags: ['Interpeer', 'Conference', 'Talk', 'Empodera', 'EmpoderaLIVE', 'EmpoderaLIVE 2023']
author: Jens Finkhaeuser
categories: ['conferences']
video: https://makertube.net/w/opaeZRXE5G4GWnMXoq6ZGj
---

I was thrilled to be on stage with the {{% extref tor %}}
for the 2023 edition of {{< external text="EmpoderaLIVE"
  url="https://empodera.org/live/en" archive=false >}}. Giorgio and
I followed a strong segment from Joyce Dogniez of the {{% extref isoc %}}
which delved into the intersection of human rights issues and the Internet already.

{{< peertube id="b564da37-539b-4f3f-a6cc-0a43394fe76e"
  title="2023 EmpoderaLIVE: Interpeer and Tor Projects on Privacy and Data Protection as Fundamental Human Rights"
  >}}

After Giorgio explained how the Tor project saves lives right now, my part was
relatively simple -- I mostly said that I want to make Tor project obsolete.

Below is the full transscript.

---

**Introduction:**
Privacy and data protection as a fundamental human right.  That's what we're
about to hear about.

The Interpeer project tries to find a secure and efficient network protecting
human rights by design. And Jens Finkhaeuser is going to tell us about this.

Giorgio Maone is a key member of the Mozilla Security Group, the Tor
application development team. And to lead this interview, Antonio Fumero, a
member of our family.

It's all yours.

**Antonio Fumero:**
Thank you very much.

Thank you for being here.

Privacy and data protection as a human right.  I think we're talking about two
key points in building a true digital society for all.  We are pretty convinced
that the two projects you are talking on behalf of here are quite similar in
going to the point, in chasing the same goals.  I mean, beyond each mission
statement or whatever.

What I'm interested in is in having you making your point of each of the projects.
Which is the selling point, the unique selling point of your project?

We are talking about very well known, at least for the name of the onion routing project.
For sure very well known since its very beginning back in the 90s.  In the mid 90s.
As far as I remember.  95 or something like that.

**Giorgio Maone:**
A bit later - it was founded by the US government.

So you see, not always governments have this interest in breaking encryption.
So we work with conflict of interest in a good way.  Because the same government
tries to break encryption and funds encryption because of their interests.

How many of you know Tor or use Tor?

That's good.  Less of half, but it's a good number.

I will start with a classical joke.

A Spaniard, a German, an Italian walk in Tor and they all look the same.

Incredible.  Right?

So the aim of the Tor project is access to the network that is anonymous and private
for everyone.  And more access to resources that are censored by governments.
Or where Internet is blocked towards some destination for political reasons, for instance.

So the focus of Tor is privacy, anonymity and access.  Anti-censorship.

Why it's called Tor?

I don't know if you...  There's no image of Tor, but Tor means "The Onion Router".
So I've got a lot of stickers outside.
Okay, for you.  I was trying to reach for them.  But in the Tor logo there is an onion.

Why?

Because the approach of Tor to networking is layering the access through nodes of the
network which don't know anything of each other because the traffic is re-encrypted
at each of these layers.

There are three nodes.  One which you contact directly, a middle node and an exit node.

So the other party you are contacting on the clear Internet cannot know anything about
who you are on this side of the connection.

And this is one part.

Who of those who know and use Tor here use the Tor browser?

Just one.  Okay.  Okay, because this is half of the story.

You are anonymous on the network in the sense that the other parties cannot see your IP,
your Internet address.  But there are many ways to tell who you are if you access this
network using common clients like, I don't know, Chrome or Firefox.

So the Tor browser is another key piece.  It's like Firefox but with a lot of patches,
add-ons, things that make it very difficult for the other parties to fingerprint
you and tell when you revisit the same site that you are that person.

This is why I told the joke about Italian, Spaniard and German looking the same.

This is the goal of the Tor browser.

I'm a Tor browser developer.  I also developed this extension called NoScript
which is the third part of this picture which is security.

Because as Joyce said, Joyce said a lot of the things that we are here to say because
the fundamentals are there.  Security privacy and anonymity online is for all the
Internet and Tor is working with the stuff we got to provide the best shot at this thing.

So the third thing is security.

Because even if you got end-to-end encryption, the police, an authoritarian regime, of course,
or you know, can infiltrate your device, can put a Trojan in your device and intercept
your messages, your connection, whatever, before they are encrypted.

So encryption works as long as your device is safe.  And NoScript is meant to
reduce the attack surface for external people to inject their
code inside your device and intercept you before you go to the network.

Indeed, I work for Tor, I've been working for Tor for a year and a half now.
But NoScript has been inside the Tor browser as a built in for ten years now.
So I'm late in the game as a person, but I was inside Tor for a long time.

And this is the aim of the project.

But another thing I want to say before going on.
There's a fourth component in the Tor project which is very important.
Now it's maybe the most important of all, which is humans.

We are a human centric organization.

Having access to the Tor network, having the Tor browser protecting you, NoScript, whatever,
doesn't serve the purpose if people don't use this technology and don't understand this
technology.

So a large part of our organization is about training, is about outreaching, is about creating
community, is about creating awareness in the society of the importance of accretion
for now in other places where there is already life or death matters, as Joyce said.
But also here, because you never know tomorrow which law is passed which makes your life
illegal.

It's happening in the United States.

We're seeing with women rights.

People get arrested because it's planned in a trip and they are pregnant and the police
assumes that they are going to have an abortion in other states.
So if communication is not safe, even accessing the website can be illegal, can put you in
jail.

And this is another layer of Tor because we also provide a technology called Onion Services
which helps websites to have an anti-censorship technology which makes them not blockable
but also makes the website anonymous on its side.
So it cannot be censored by killing the people operating it, for instance.

And even the New York Times, for instance, or Facebook run their Onion site to allow
people in censored environments to access their resources.

We probably would not know many of the things that happened in the last September in Iran
if it was not for Tor allowing people to exfiltrate information about videos, for instance, on
the social networks about what was happening.

The Tor browser as an Android application was the most downloaded application during
these turmoils in Iran.
And we are very proud of it because it means that our work saves lives.

**Antonio Fumero:**
That's great.

We are talking...

Great pitch.

I'd be definitely donating to the project, for sure.

But we are talking about quite technical foundations here in both of the projects.
But at the end of the day, for instance, Interpeer has a different approach, a different angle
for the same problem.

There is privacy, there is data protection.

How is different Interpeer from these kind of projects building tools for anonymity?

**Jens Finkhäuser:**
That's a good question.

It is different and it's not.

So Joyce was talking about encryption, end-to-end-encryption, there's the Tor project.
We heard from other projects that try to provide privacy online.

On the flight here to Malaga I had this idea about how to explain this, that it uses seatbelts.

I'm just old enough that my mum's car didn't have seatbelts in the back of the car.
The driver was protected, the children, who cares?

So obviously we thought that's a bad idea.

So how did we solve this?

Well, the thing we didn't do is we didn't go to every driver and say,
you are now liable if you don't have a seatbelt in the back.

What we did is we went to manufacturers and said,
well, if you produce cars without seatbelts in the back, you're going to have fines.
I think this is a much better approach.

But then I asked myself the question, why do trains not have seatbelts?

They go as fast as cars, they transport people.

Well, I mean, there are going to be a lot of different reasons coming together,
but two obvious ones are they go on rails, so it's very hard to drive them into a ditch.
And the other one is that instead of having a thousand people driving a thousand cars,
each of which can fail mechanically, or the person can have a bad day and this causes an accident,
we just have a single vehicle on rails operated by a person that has a support network as well, I should say,
and that reduces the risk of accidents so much that seatbelts are no longer even a question.

So what I'm trying to do with Interpeer is I'm looking at all these different solutions there
and I'm trying to ask myself what is the best way to put them into the infrastructure of the Internet
so that these problems that we're now solving with seatbelts no longer exist.

That's the pitch.

**Antonio Fumero:**
That's a good metaphor, for sure.

I think that one of the things I've been thinking about is the point that
the concept of privacy has changed a lot since the 90s.

So we are talking about two different projects.

Tor is a project that was born back in the 90s with a concept of privacy that is totally or completely different
than the one we have nowadays.

What do you think your project would be a necessity in another scenario?

**Giorgio Maone:**
I think it's more necessary now than when Tor was created because our digital life is our life now.

When Tor was created it was more a military tool, a tool to help revolutionize,
helping USA to foster their foreign policy.

Then it became an open source project, completely transparent, so anybody can trust it no matter how it was born.
And at the same time, the Internet as a whole has become more aware of it having been growing organically
with no security in mind, with no privacy in mind.

And we, and by we I mean standard bodies, for instance, I'm part of the W3C, the Web Application Security Working Group.
We tried to patch the Internet as it was, the web as it was, to make it more private and more secure for everybody.

So there's this movement toward a safer Internet for everybody.

But the foundation, as Jens was suggesting, are pretty weak.

So Tor is even more necessary today because everybody is on the Internet, everybody has stakes in the Internet,
their banks, their relationships, all their life, their movements.
If I go to the bar, the Internet knows about it.

So having something that has privacy and anonymity and security as its first focus is much more important now than before.

And we are also seeing a movement in so-called Occidental governments to use the same tactics
used by authoritarian regimes under other justifications like child pornography, terrorism, whatever,
but the same tactics of censorship, of breaking encryption.

So it's even more useful for us that we consider ourselves more lucky than an Iranian, an Afghani, Pakistani, Chinese.

And a point I want to stress, everybody should use at least once a day Tor to help other people to blend better.
Because otherwise, if only the people who risk their lives use Tor, they are singled out.

If everybody uses Tor for something, Tor is normalized.
Everybody uses Tor. Otherwise, you use Tor, you are a criminal.

No, it's not this. I value my privacy. I value privacy of other people who are fighting for their freedom.
I value democracy here, there, now and tomorrow.

**Antonio Fumero:**
Yes. Good point.

On the other hand, we have a new project, recently funded, that is Interpeer.

Interpeer is a recently funded project within this next generation Internet framework from Europe.

What do you think you need, or what do you think that the toughest challenges you are coping with in your projects?

Do you think there are lessons learned from community-based efforts like Tor you can use for get traction in Interpeer projects, for instance?

**Jens Finkhäuser:**
For traction, maybe as well.

So first of all, thank you, yes.

[NGI, the Next Generation Internet Initiative](https://www.ngi.eu/), is partially funding my work.
So is the [Internet Society Foundation](https://www.isocfoundation.org/), so it's very good that we are on stage right after.
And without them, it wouldn't work.

What we're doing is relatively long-term, obviously.
If you want to change how the Internet works, it's not going to happen tomorrow.

So one of the biggest questions is how do we sustain this. Funding, right?

It's a word that we don't like to talk about so much, but it is really the main thing here.
Because I can, off the bat, I can find a handful of people who would work on this for years, but I don't have the funds to pay them.

NGI is great because it's a very low overhead program to get funding.
And Internet Society was, Foundation, sorry, they both exist, right?
It was not much more difficult.

So these are great starting points.

But one of the things that happens in software is that the more users you get, the more issues you find with how the software is being used.
And this is then maintenance work.
And maintenance work is not really the focus of research grants.

Things like sitting here, coming here to talk, right?
I can allocate a small budget for this, but technically, it's not what I'm being paid for.

So there's a whole, I think as a society, Paul yesterday was saying that open source, what we're both doing is a public good.
It's a commons.

I think as a society, we have to shift our thinking quite drastically away from this is what people do in their spare time, which we also do, to be fair, because we like this.
To how do we sustain this as something that keeps getting created and maintained and so forth?

I don't really have answers.
I just know that this is one of the tough challenges.

**Antonio Fumero:**
Thank you.

We are running out of time, so is anybody in the audience that wants to make a question over there?

**Audience Member #1:**
I wanted to say that privacy, speaking about Tor, what you said before are positive uses,
but how can you guarantee everything?

Because we know what anonymity is about in social media.
It can be used by criminal groups.
The anonymity using Tor browser, for instance.

**Giorgio Maone:**
You mean how can I guarantee you that you are anonymous when you use?

**Jens Finkhäuser:**
No, the question was how can you guarantee that it's used for good users, not for criminal users.

**Giorgio Maone:**
Oh, okay.  Okay.  Okay.

It's much the same of the answer we got from Internet Society about encryption in general.
The good and the bad thing of guaranteeing end-to-end encryption and anonymity, especially for an open source project like Tor.

For instance, what happens inside WhatsApp, we don't know.
That's because the most technical savvy people still use Signal because they use the same protocol as Signal, but WhatsApp is a closed source application.
So you must trust them to actually do what they say.

But to the point, we cannot tell what people does actually with Tor.

As I said, we are human-centric.
We try to have a community, a broad community with ties in NGOs, in digital defenders.
We make trainings.
We get to know relay operators who are these people who run the actual nodes that relay the traffic on the Tor network.

But we cannot tell what actually happens inside these nodes as traffic because it's the guarantee we provide to the world.
Otherwise, we would be the interceptor ourselves.

So I got a little answer to the question about how we fight crime even if there is encryption.

How we did before the Internet.

You infiltrate, you send cops undercover in the community.
So you do the same work we do to train our people, create communities, get to know people.
You can do it for law enforcement.
You can go undercover in this community and try to unmask them.

I've been asked more than once by cops to put backdoors in NoScript because they even managed to own a server with a pedophile ring there.
And that was heartbroken because I've got three children.
I was heartbroken, but I could not tell them because if I put a backdoor in NoScript, which is a free open source software,
the day after I bought the project for all the people which relies on this project for their lives.
And they don't get anything because someone will fork my project and NoScript will keep working, but nobody will trust me.

OK, so the answer is do your job, cop, please.

**Audience Member #2:**
Yes, going ahead with the same issue, we have to say that Tor has not a very good reputation.

As our fathers and teachers, we see that maybe our children will have instant access to places where we don't want them to be.

**Giorgio Maone:**
Are you using some kind of parenting control or something like that?

**Audience Member #2:**
Yes, can we apply parenting control?

**Giorgio Maone:**
If you are using parenting control, you can prevent them from using Tor if you don't trust them.

I prefer to trust my children, my three children.

I prefer to speak with them, have an open language so they can report about bad things that can happen.
They trust me and I trust them and I entrust them with privacy.

If you are using parenting control, you can already prevent them from using Tor if you are concerned about this.

About the bad reputation of Tor, fortunately, it's something that is changing because the news try and have tried,
probably also instigated by governments, by the police, to remove the light because they talk about dark web, so no light.
To remove the light from Tor, like it was a place where only crimes happen.

But what is crime for a government is fighting for freedom in another government, right?

So, yes, we are trying to move away from this messaging about the dark web.

We also underwent great organizational changes.
All our leadership are women.
It's something that I enjoy a lot because it's a very pleasant place to work in.
We are very dedicated.
We believe in what we do, but we are also relaxed.
We have a good work-life balance.

I don't want to say we are friends, but usually this rhetoric is used by corporations to fool you in a bad way.
But yes, we all believe in what we do and we try to do it for the common good.

Oh, one comment.

Before I forget, everybody here can help people even without using the Tor browser daily.

Just open a tab in a website called [snowflake.torproject.org](https://snowflake.torproject.org).
It's a very simple web-based proxy.
You can also install it as an extension in your browser.

But just opening a tab and keeping this tab running in your browser creates a bridge which augments the capacity of the Tor network to serve these people who are trying to evade censorship in Iran, in China, whatever.
And so you are doing a good deed, a very good deed, with no expense.
It's something that costs nothing.

Of course, you can also donate, you can take our stickers, (in the) coffee break.
But you also can just run a snowflake proxy and do good for the world.

**Antonio Fumero:**
So, as a concluding remark, I think you all have two action points today.

The first one, download the Tor browser.

And the second one, [make a donation to Interpeer Project for not needing these tools anymore]({{< relref donations >}}).

So, thank you very much for sharing with us this few minutes.
