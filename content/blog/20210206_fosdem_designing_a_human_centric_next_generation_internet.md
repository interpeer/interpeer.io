---
title: "FOSDEM'21: Designing a Human Centric Next Generation Internet"
date: 2021-02-06T18:30:00+00:00
tags: ['Interpeer', 'Conference', 'Talk', 'FOSDEM', 'FOSDEM21', 'FOSDEM2021']
author: Jens Finkhaeuser
categories: ['conferences']
video: https://makertube.net/w/wKhd7kjasvLzTtVkNPhcrK
---

In February, I was able to give a talk at {{< external text="FOSDEM"
  url="https://fosdem.org" archive=false >}}, Europe's premier conference on free and open
source software. Entitled "Designing a Human Centric Next Generation
Internet", I tried to lay out the requirements such an Internet might have,
derived from the failures of the current web.

{{< peertube id="f8fd2184-4858-4566-9be6-68c6484961d1"
  title="FOSDEM'21: Designing a Human Centric Next Generation Internet"
  >}}

I suspect the talk was a little too unfocused, but the response was great
nonetheless! It certainly helped me understand better how I need to present
the Interpeer Project in future.
