---
title: "Musings on the Data Rights Protocol"
date: 2023-02-14T10:00:00+00:00
author: Jens Finkhaeuser
categories: ['miscellaneous']
image: cctv_pinata.jpg
tags:
- Data Rights Protocol
- California Consumer Privacy Act
- ccpa
- General Data Protection Regulation
- gdpr
- Datenschutz-Grundverordnung
- dsgvo
- data rights
- human rights
- privacy washing
- privacywashing
---

A few days ago, I found myself attending a pitch by the {{<
  external url="https://digital-lab.consumerreports.org/"
  text="Consumer Reports Digital Lab" archive=false >}} for
their {{% extref datarights %}}. At first glance, it's a great idea! Give
organizations a standardized interface for exercising your data rights, which
means you can use a simple app to request what data is collected about you,
have it deleted, etc. What's not to love?

Turns out, there are some immediate concerns, and some longer-term, more vague
issues that need addressing.

{{< fig src="cctv_pinata.jpg"
    link="https://www.flickr.com/photos/37584375@N02/15889151046"
    rel="external"
    target="\_blank"
    width="100%"
    alt="Web We Want Festival - Privacy Pinata"
    caption="[\"Web We Want Festival - Privacy Pinata\"](https://www.flickr.com/photos/37584375@N02/15889151046) by [Southbank Centre London](https://www.flickr.com/photos/37584375@N02) is licensed under [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)"
>}}

## Background

A few years ago, the State of California passed their {{% extref ccpa %}},
which is now being implemented into law. It's modelled somewhat similarly
to the European Union's {{% extref gdpr %}},
though there are also some differences between them.

Under these laws, consumers have been granted data over [personally identifying information (PII)]({{< relref "knowledge_base/personally_identifiable_information" >}}),
such as the right to know who keeps which information, and to have it deleted.

The Consumer Reports Digital Lab performed some research and found that
people do not often exercise their rights, because doing so proves difficult.
Bringing transparency into the process by offering a standard interface to
many organizations was the goal behind the development of the data rights
protocol.

The protocol therefore seems like an effective solution to the problem, as it
enables the use of simple apps for these purposes.

## CCPA vs GDPR

When asked about the GDPR, however, the lab representative spoke about a
"laser focus" on California.

At this point it's worth understanding how these two different pieces of
legislation apply. A detailed comparison can be found in {{< external
  text="GDPR v. CCPA - The Future of Privacy Forum"
  url="https://fpf.org/wp-content/uploads/2018/11/GDPR_CCPA_Comparison-Guide.pdf"
  archive="force"
>}}, and will not be reiterated here.

The short form, however, is that GDPR is supposed to protect people in the
EU, even when the entity gathering data is outside. In CCPA, the phrasing is
subtly different, in that it protects California *residents* -- not visitors --
and the entity has to "do business" in the state, which tends to involve
exceeding taxation thresholds.

In other words, the reason that GDPR is blamed for cookie banners worldwide
is because any person in the Union could theoretically visit the website. CCPA
might not apply for something as simple as that.

As an aside, note that cookie banners are in no way required by GDPR, and are
a [deceptive pattern]({{< relref "knowledge_base/deceptive_pattern" >}}) intended to make people
believe such a requirement exists. It does not. Advertisers hate the GDPR,
however, so have found cookie banners as a tool to make people hate it as well.

Finally and crucially, GDPR requires [informed consent]({{< relref "knowledge_base/informed_consent"
    >}}) for collecting data.  CCPA does not. That is, under CCPA companies
may collect data about you without your knowledge.

## Horse before the Cart

The first thing that comes to mind when working through this is that CCPA
puts the cart before the horse. In the interest of privacy, it's the gathering
and processing of PII that should be regulated -- permitting folk to review
PII and have it deleted just follows from that.

Under CCPA, a business could legitimately hide their data gathering practices,
and thereby avoid acting on requests for disclosure or deletion.

This is clearly not a problem with the data rights protocol! But it is also
somewhat disheartening situation that the focus of the protocol is limited to
situations where individuals already detected that their data is being
gathered.

It would broaden the scope of the protocol considerably and for the better if
it included from the get-go things about giving consent for processing in the
first place -- with appropriate warnings that data might be gathered without
consent in other situations.

## A Good Excuse?

A less obvious concern is that the protocol is developed in cooperation with
data processing companies. There is no reason to assume that the consumer
reports digital lab is corrupt in any form. There is no reason to assume that
the resulting protocol is not good enough.

But it does raise the question why these processing companies would get
involved in this effort.

It's clearly in their interest to *appear* to do the right thing for data
subjects, even though CCPA gives them plenty of opportunity to be sneaky. And
that, then, suggests that maybe their involvement is merely a form of
[#PrivacyWashing]({{< relref "knowledge_base/privacy_washing" >}}).

## Closing Thoughts

The data rights protocol seems like a good start. But the specific limitations
of the California legislation it targets make it easy to hold up implementation
of the protocol as a seal apparently indicating that one acts in data subject's
best interest -- distracting from the fact that data can be harvested without
consent.

It appears as if extending the protocol to other legislations is vital to help
avoid this. And it's absolutely necessary before one can speak about
"standardizing" access to data subject's rights.

The current state of things leaves me hopeful that this project develops into
something far more useful than it currently is. But until that happens, it may
be prudent to eye it very carefully. There is a potential for misuse as a
PrivacyWashing technique here that should not be ignored.
