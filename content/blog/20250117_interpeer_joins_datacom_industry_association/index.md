---
title: "Interpeer joins the Datacom Industry Association"
date: 2025-01-17T08:00:00+00:00
tags: ['interpeer', 'datacom', 'datacom industry association', 'dia', 'founding member', 'founder']
author: Jens Finkhaeuser
categories: ['announcements']
image: interpeer_dia.png
---

2024 was a relatively quiet year for the Interpeer Project, so it's only right
to go out with a bang: Interpeer gUG joined the {{< external
  text="Datacom Industry Association"
  url="https://datacom-ia.eu"
  archive=false
>}} as a *founding member* in December!

{{< fig
    src="interpeer_dia.png"
    link="https://datacom-ia.eu/members/"
>}}

DIA aims to be the voice for the European Datacom Industry & Research Community,
with its focus on secure, innovative, and collaborative digital communication solutions.
We're incredibly proud to be a part of this group!

---

At Interpeer, we consider it of vital importance Europe's drive towards [digital sovereignty]({{< relref
  "knowledge_base/digital_sovereignty" >}}) is well supported by the R&D
community, and a healthy [digital commons]({{< relref "knowledge_base/digital_commons" >}})
is an essential component of any strategy pursuing this goal. There is no *digital sovereignty* if digital communications
solutions are captured, either because standards or implementations
are not part of the *commons*.

It is particularly alarming that examples such as the
[threat to Europe's Next Generation Internet programme]({{< relref "20240715_the_eu_must_keep_funding_free_software" >}})
keep occurring. While recent years saw the emergence of many funding efforts
to support *Free/Libre and Open Source Software (FLOSS)* better, the funds scopes
have also *narrowed* dramatically, focusing predominantly on such FLOSS packages
that are already part of solutions provided to the *public sector*.

While the importance of supporting such software is undeniable, this narrow
focus immediately excludes existing software that may yet be adopted, software
that is publicly available but not provided by the public sector, as well as any
endeavours that look beyond the status quo. It is a *conservative* approach
rather than one that looks at securing the future.

We therefore consider DIA as a necessary vehicle for representing the longer term
view to European institutions charged with pursuing *digital sovereignty*
strategies.

Our motivation for joining in the foundation of the association is
so inextricably linked with our motivation for developing a
[human-centric Internet]({{< relref "knowledge_base/human_centric_internet" >}}):
an Internet that continues to serve the needs of every person, and improves
in doing so -- a true commons.
