---
title: Mini-Roadmap
date: 2021-05-11T14:00:00+00:00
tags: ['Interpeer']
author: Jens Finkhaeuser
categories: ['connection reset by peer']
---

Since I occasionally find people focused on the same kind of problem that
I'm trying to tackle with this project, and I have a terrible tendency to
share my thoughts informally, I figured it would be a good idea to post a
kind of roadmap for the project.

This isn't going to be fixed in stone forever. But its general outline should
remain more or less intact.

The overall goal is to "fix" the problems of the World Wide Web by addressing
the privacy and security needs of the users and by embracing that most users
will use a multitude of devices, some of which are shared. Finally, it's
important to understand that real-time usages and eventually consistent usages
of the web are both required, and that there are good uses for both the "document"
and the "application" web, as some people call it.

I've written elsewhere on the problems of the web, and a high level overview
on how to move forward:

* [Let's talk about REST](https://reset.substack.com/p/lets-talk-about-rest)
* [An Architecture for a Better Internet](https://reset.substack.com/p/an-architecture-for-a-better-internet)

The purpose of this post is to outline a rough roadmap of "chunks" that need to
be addressed.

1. Provide some kind of legacy free, reasonably simple socket abstraction. This
   is what [packeteer](https://codeberg.org/interpeer/packeteer) is about. And
   while that project still can use some love, it's "done enough" to move
   forward.
1. Provide peer-to-peer connectivity that allows for multiple networking links
   and multiplexes individual "channels", each of which can have its own
   reliability capabilities and carries its own encryption context. This is what
   [channeler](https://codeberg.org/interpeer/channeler) is about, and I'd call
   it around 30% complete as of today.
1. Provide a kind of identity and authorization framework. This is going to be
   somewhat abstract, not necessarily something directly expressed in a
   networking protocol. The idea is to allow for some limited delegation of
   responsibilities.
1. Provide a filesystem-like interface for synchronizing documents (and
   applications) in a scalable, eventually synchronous fashion. This should
   fulfil the document web use cases, but by building on the previous two
   milestones should be secure and private by default.
1. Use the filesystem-like interface for providing also real-time communication
   capabilities. This isn't as difficult as it sounds, but worth separating
   into its own milestone.
1. Extend the filesystem-like interface to better work with content types.
   Content types should be the driving force behind how a real-time API here
   works, and how documents are to be manipulated. The aim is to *not* leave
   everything to server implementors as in REST, but follow an approach that
   looks closer to how e.g. [schema.org](https://schema.org) represents data.
1. Provide a real-time API schema based on e.g. [CapTP](https://spritelyproject.org/news/what-is-captp.html)
   to demonstrate that the application web needs can be served more sanely
   than via REST's data silos - for if and when the document web approach
   simply will not work.
1. Build sample applications. This can really be happening at any stage, and I
   have a few ideas for earlier stages. But the whole thing has to lead somewhere,
   so it's worth putting a final milestone here.

This overview has always been in my head, and it's somewhat reflected in the
grant roadmaps I've submitted. But I don't think I've ever communicated it
publicly, and that's been a mistake.
