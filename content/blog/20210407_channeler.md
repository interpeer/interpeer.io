---
title: "First Channeler Milestone"
date: 2021-04-07T18:30:00+00:00
tags: ['Channeler', 'Milestone', 'NGI0', 'NLNet']
author: Jens Finkhaeuser
categories: ['connection reset by peer']
---

A few days ago, I managed to complete another milestone for the NGI0 grant
agreement. It's released as [channeler]({{< relref "projects/channeler" >}})
on the Interpeer code page.

In a sense, this is more of an interim update. The grant agreement covers
many more milestones in this repository, and as it stands, the code is highly
work in progress.

But it demonstrates the basic channel establishment functionality -- and while
that doesn't have any bells and whistles yet, it's a good starting point for
further iteration and refinement.

The protocol design largely follows the ideas laid out on the
{{< cat "connection reset by peer" >}} blog.

The next step will be to add per-channel reliability features, which is
also a good reason to streamline the packet buffer handling, etc.
