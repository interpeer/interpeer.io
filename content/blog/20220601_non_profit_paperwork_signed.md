---
title: "Non-Profit Paperwork Signed"
date: 2022-06-01T14:30:00+00:00
tags: ['Interpeer', 'non-profit', 'ngo']
categories: ['announcements']
author: Jens Finkhaeuser
---

Today, at 14:00, I went to the notary to sign the paperwork for incorporating
a non-profit organization for the Interpeer Project.

If you've gone through this process before, you know that after the notary
witnessing the signing of the company articles, you then have to create an
account in the entity's name, put the initial money there, prove to the notary
that you've done so, and only *then* will they register the entity properly.
It'll take some time to be operational.

However, it means that the foundation date is now the 1st of June, 2022. That
much won't change.

The form of legal entity I went with is a public interest, miniature limited
liability company - which may seem like an odd choice. The short explanation
is that it's the only form a non-profit can take here in Germany that I can
run entirely on my own. Of course I can still add to the board, etc. until
that is no longer a necessary consideration.

The foundation is mostly useful for a few reasons:

1. It's an entity with a reduced tax burden.
1. It opens up potential access to different grants.
1. It allows me to hire people as employees.
1. It can issue donation receipts, which opens another funding channel.
1. It can be used to keep copyright of software with an entity that is legally
   bound to serve the public interest.

Let's see how much of that is going to happen in the next couple of months.
In the meantime, however, it makes me incredibly happy that I can now pursue
other ways to keep this project running.
