---
title: "Interview With Jens Finkhaeuser - Founder and CEO at the Interpeer Project"
date: 2024-10-20T09:00:00+00:00
tags: ['Interpeer', 'interpeer project', 'safety detectives', 'history', 'motivation']
author: Jens Finkhaeuser
categories: ['miscellaneous']
---

There's a new [interview up on Safety Detectives](https://www.safetydetectives.com/blog/jens-finkhaeuser-interpeer-project/)
where I get to discuss the origins, motivation and direction of the Interpeer
Project as a whole. It's been a great conversation, and I think one of the
better summaries of the project from the beginnings to where we are now.

I also get to make some predictions for the Internet that make me rather sad,
but help illustrate why the project is taking the architectural approach that
it does:

> Now I’m writing this in a night where Kirk sweeps over France and heads into
> central Europe. Hurricane Milton is heading for Florida, mere weeks after Helene
> has wreaked havoc there. And more storms are forming.
> 
> I’m particularly sensitive of France and Miami here, because some of my peers
> are monitoring data centres there, wondering whether services they host in
> those locations will remain unaffected.
> 
> Again, the Global South tends to be the worst affected by those climate change
> events. But we’re already seeing their impact on the parts of the Internet in
> the Global North. We can be all but certain that intermittency of Internet
> services is going to increase around the globe, even if only in one area or
> another at a time.
>
> So my predictions for the Internet is that the Global North/South divide will
> grow, and disruptions in parts of the Internet will increase. And that is not
> even counting deliberate disruption as a form of warfare. 

[Head over to the interview](https://www.safetydetectives.com/blog/jens-finkhaeuser-interpeer-project/)
for context and the full story!
