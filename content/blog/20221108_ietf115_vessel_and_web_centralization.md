---
title: "IETF115: Vessel Container Format and Web Centralization"
date: 2022-11-08T18:30:00+00:00
tags: ['Interpeer', 'Conference', 'Talk', 'IETF', 'IETF115']
author: Jens Finkhaeuser
categories: ['conferences']
video: https://makertube.net/w/aD19Nx9zmZ1cxzf4eMRmo1
---

{{< external text="IETF115" url="https://datatracker.ietf.org/meeting/115/proceedings"
archive=false
>}} in London was our first change to present some of Interpeer's work to the
IETF community -- or, more precisely, to it's sister organization, the {{< external
  text="Internet Research Task Force" url="https://irtf.org/" archive=false >}}.

On the 7th, I was invited to the *Decentralized Internet Infrastructure
Research Group (DINRG)* to recap FOSDEM's talk on web centralization.

{{< peertube id="88756656-bddd-4c79-9809-48c8b3f41c42"
  title="IETF115: Web Centralization @ DINRG"
  >}}

This was followed on the 8th in the *Information-Centric Networking Research
Group (ICNRG)*, where I spoke about the [vessel]({{< relref "projects/vessel" >}})
container format.

{{< peertube id="4e0cdc67-c3bc-4924-965a-e0562094bb24"
  title="IETF115: Vessel Container Format @ ICNRG"
  >}}
