---
title: Conferences & Papers
date: 2021-01-02 12:34:19.080000+00:00
tags: ['AnyWi', 'HiPEAC', 'FOSDEM', 'FOSSASIA']
author: Jens Finkhaeuser
categories: ['conferences']
image: conference.jpeg
---

As is usual these days, I don't write here half as much as I would like to. In
recent weeks, this is because I have been very busy preparing for a number of
conferences and writing papers. Since they all relate in one way or another to
the protocol work I have been writing about, I figure people might find them
interesting.

I have mentioned before that I am working part time at {{< external
  text="AnyWi Technologies B.V."
  url="https://www.anywi.com" archive=false >}}. The two jobs are fairly complementary; at
Interpeer, protocol design is for peer-to-peer networks, while in the latter,
it is for autonomous vehicles, in particular for drones. Really, the Venn
diagram between the two, if you would draw it, would probably have about 90%
overlap or so. So the appearances and publications relate to both.

* 2021-01-18 -- HiPEAC ‘21: {{< external text="DroneSE Workshop"
  url="https://www.hipeac.net/2021/budapest/#/program/sessions/7841/" archive="force" >}}, {{%
    doi "10.1145/3444950.3444954" "Reliable Command, Control and Communication Links for Unmanned Aircraft Systems" %}}
* 2021-02-06 -- FOSDEM ‘21: {{< external
  text="Designing a Human Centric Next Generation Internet"
  url="https://fosdem.org/2021/schedule/event/humancentricinternet/" archive=false >}}
* 2021-03-13 to -21 -- FOSSASIA ‘21: {{< external
  text="Designing a Human Centric Next Generation Internet"
  url="https://eventyay.com/e/fa96ae2c/session/6672" archive="force" >}} (somewhat shorter, but
  live)

{{< fig src="conference.jpeg"
  link="https://www.flickr.com/photos/26455260@N06/2926691615"
  rel="external"
  target="\_blank"
  width="100%"
  alt="Conference"
  caption="[\"Conference\"](https://www.flickr.com/photos/26455260@N06/2926691615) by [dkalo](https://www.flickr.com/photos/26455260@N06) is licensed under [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)"
>}}

## Reliable Command, Control and Communication Links for Unmanned Aircraft Systems

My AnyWi colleague Morten Larsen and I co-wrote a paper related to the
{{< external text="COMP4DRONES" url="https://comp4drones.eu" archive="force" >}} project, in
which we outline the beginnings of EU regulations for safely communicating with
commercial drones.

Drones in the EU are subdivided into three broad classes; the first is the toy
category, the last is the military category. In the middle there is space for
commercial "pizza delivery" type drones.

In order to fly them cost-effectively, it's best to make use of commercially
available communication links - such as LTE/5G or WiFi. The downside is that
those may not always provide the reliability you'd need to fly in higher risk
areas, e.g. over people's heads.

AnyWi is working on technology effectively bundling these links into a virtual
link with higher availability, failover and even bandwidth.

There is a {{< external text="preprint published at researchgate"
  url="https://www.researchgate.net/publication/347522757_Reliable_Command_Control_and_Communication_Links_for_Unmanned_Aircraft_Systems"
  archive=false >}}, and the paper will be published in the {{< external text="ACM"
  url="https://www.acm.org/" archive=false >}} proceedings of the conference.

## Designing a Human Centric Next Generation Internet

This talk, which I have confirmed for FOSDEM and FOSSASIA, but not yet for
LibrePlanet, I want to talk about the vision and progress on the Interpeer
Project.

On the one hand, it's an attempt to shake people up a bit and think about how
the web stack isn't really suitable for the future. I wrote in this blog
before about safety in communications, and there will be a bit more on that
topic in different words.

The other part of the talk is going to cover some of the protocol design I have
been writing about here, as well as in the other paper that may or may not
(probably will) be published in MICPRO.

## Multi-Link Tunnelling Protocol

Where the first paper written with Morten is covering the general problem of
reliability in commercial drones, this second paper is about the multi-link
tunnelling protocol we're working on; this work is done in the context of
{{< external text="ADACORSA" url="https://adacorsa.eu" archive="force" >}}.

It's a bit more abstract than the protocol design things I've written about
here, in that it focuses on messages and state machines, and stays far away from
more concrete information such as byte-level packet encoding. That's deliberate,
because on the one hand, we're still working on it - and on the other,
wire-encoding is the simpler part.

Plus, some version of this protocol will make it into the Interpeer stack
somehow. After all, handover between ground stations for a drone is the
same kind of scenario - at the networking layer - as keeping up a video
conference stream on your mobile phone as you leave your home WiFi and fall
back to the LTE cell.

**Note 2022/12/22:** This paper was not published, in the end.
