---
title: Google vs. the Open Web
slug: google-vs-the-open-web
aliases:
- apple-vs-the-open-web
- privacy-pass
- private-access-tokens
- web-environment-integrity
date: 2023-07-21 07:38:27.314000+00:00
tags: ['google', 'kill', 'open web', 'openweb', 'accessibility', 'inclusion', 'appliance', 'generative system', 'poverty', 'web environment integrity', 'wei', 'privacypass', 'privacy pass', 'private access token', 'apple']
author: Jens Finkhaeuser
categories: ['miscellaneous']
image: open_web.jpg
faq:
  - question: What is the "Open Web"?
    answer: >-
      The term "Open web" has a fairly sweeping definition — it encompasses
      technical concepts like open-source code and open standards. But it also
      refers to democratic concepts, like free expression and inclusion.

      The underlying principle to all of these ideas is that the web should be
      by and for everyone, much like a commons in the digital realm.

      This implies that access to resources on the web should not be moderated
      by governments or corporations with their own definitions of what people
      should access.
  - question: What is "Web Environment Integrity"?
    answer: >-
      Web Environment Integrity or WEI for short is an abandoned proposal by
      Google for controlling access to web sites.

      It is part of a class of proposals in which "trusted agents" are consulted
      whether access is permitted, and those "trusted agents" are provided by
      operating system or browser manufacturers. The reasoning goes that those
      agents have access to user information, and can therefore provide
      authorization for access.

      While in principle this approach is supposed to protect privacy, because
      it does not expose user information directly to websites, in practice it
      centralizes control over the experience of the web within a single entity.

      The proposals also rarely if ever include mechanisms for controlling and
      auditing of the processes and data by which these entities decide on
      authorizing access, which leaves them highly vulnerable to systematic
      abuse.

      This kind of proposal is a serious threat to the Open Web.
  - question: Is Apple involved in "Web Environment Integrity"?
    answer: >-
      Apple is not involved in Web Environment Integrity (or WEI for short).
      However, Apple is championing a similar proposal called Privacy Pass.

      Like WEI, Privacy Pass is part of a class of proposals in which "trusted agents" are consulted
      whether access is permitted, and those "trusted agents" are provided by
      operating system or browser manufacturers. The reasoning goes that those
      agents have access to user information, and can therefore provide
      authorization for access.

      While in principle this approach is supposed to protect privacy, because
      it does not expose user information directly to websites, in practice it
      centralizes control over the experience of the web within a single entity.

      The proposals also rarely if ever include mechanisms for controlling and
      auditing of the processes and data by which these entities decide on
      authorizing access, which leaves them highly vulnerable to systematic
      abuse.

      This kind of proposal is a serious threat to the Open Web.
  - question: What is "Privacy Pass"?
    answer: >-
      Privacy Pass is a technology similar to Web Environment Integrity and
      is championed by Apple and developed at the IETF.

      Privacy Pass is part of a class of proposals in which "trusted agents" are consulted
      whether access is permitted, and those "trusted agents" are provided by
      operating system or browser manufacturers. The reasoning goes that those
      agents have access to user information, and can therefore provide
      authorization for access.

      While in principle this approach is supposed to protect privacy, because
      it does not expose user information directly to websites, in practice it
      centralizes control over the experience of the web within a single entity.

      The proposals also rarely if ever include mechanisms for controlling and
      auditing of the processes and data by which these entities decide on
      authorizing access, which leaves them highly vulnerable to systematic
      abuse.

      This kind of proposal is a serious threat to the Open Web.
  - question: What are "Private Access Tokens"?
    answer: >-
      Private Access Tokens are part of a technology called Privacy Pass which
      is championed by Apple and developed at the IETF.

      Privacy Pass is part of a class of proposals in which "trusted agents" are consulted
      whether access is permitted, and those "trusted agents" are provided by
      operating system or browser manufacturers. The reasoning goes that those
      agents have access to user information, and can therefore provide
      authorization for access.

      While in principle this approach is supposed to protect privacy, because
      it does not expose user information directly to websites, in practice it
      centralizes control over the experience of the web within a single entity.

      The proposals also rarely if ever include mechanisms for controlling and
      auditing of the processes and data by which these entities decide on
      authorizing access, which leaves them highly vulnerable to systematic
      abuse.

      This kind of proposal is a serious threat to the Open Web.

---

A few days ago, I made {{< external text="a social media post"
  url="https://chaos.social/@interpeer/110740096258668222" archive="force" >}} about Google vs.
the Open Web. It received some responses, so I'll reproduce it below with some
additional comments.

{{< fig src="open_web.jpg"
  link="https://www.flickr.com/photos/35034356597@N01/2787595632"
  rel="external"
  target="\_blank"
  width="100%"
  alt="Open Web - Gnomedex 2008"
  caption="[\"Open Web - Gnomedex 2008\"](https://www.flickr.com/photos/35034356597@N01/2787595632) by [Randy Stewart](https://www.flickr.com/photos/35034356597@N01) is licensed under [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)"
>}}

---

## Google is trying to kill the Open Web.

Using the proposed "Web Environment Integrity" means websites can select on
which devices (browsers) they wish to be displayed, and can refuse service
to other devices. It binds client side software to a website, creating a
silo'd app.

{{< external
  url="https://github.com/RupertBenWiser/Web-Environment-Integrity/blob/main/explainer.md"
  text="Web Environment Integrity on GitHub" archive="force" >}}


This penalizes platforms on which the preferred client side software is not
available.

This is an issue for accessibility and inclusion, in particular when the reason
the software is not available is tied to the needs of marginalized groups, such
as when poverty makes it impossible to own sufficiently modern devices, etc.

"Web Environment Integrity" is a deeply antisocial proposal, and goes counter
to the design principles of the web.

In all honesty, this move by Google is hardly surprising. They've been trying to
capture the web as their own platform for a long time. This is just the latest
battle.

But it also marks a particularly perverse point, in how the proposal admits it
exists primarily to extract value from people. People mining at its worst.

Remember when their motto was "Don't be Evil?"

---

## Analysis of the Proposal

Some details on the proposal may help here.

The proposal suggests that websites should be able to request an attestation
from the browser about its "integrity". Such attestations are to be provided
by external agents, which -- presumably -- examine the browser and its plugins,
and issue an approval only if those checks pass.

The attestation is sent back to the website, which can now decide to deny
service if the agent did not give approval.

Ostensibly, this is to ensure *for the user* that the environment has not been
tampered with in any way. The described use cases, however, make fairly clear
that it is *for the business* that this feature exists.

In particular, the proposal suggests that "Google Play" could provide such
attestations, and also provides an example case which intends to ensure that
ads are served only to legitimate users, not to automated processes.

These two points are not raised together. But put them together, and you find
the following underlying problem:

1. Advertisers want to reduce costs.
1. Website owner wishes to display ads.
1. Google's ad network charges per *impression*.
1. Bots create impressions.

The proposal effectively provides a solution for Google's advertising problem,
and tries to couch it in more user friendly terms. The above scenario is the
closest to a problem they describe outright.

The solution, expressed in the proposal, is to exclude bots via attestations,
such that ads generate impressions only with logged-in Google Play users.

However...

In general, bots are pretty easy to exclude.[^1] They usually advertise themselves
by a user agent string. Yes, that can be faked -- but it seems highly unlikely
that bots using faked user agents create such a large number of impressions that
Google has to use this route against them. If I look at my own webserver logs,
it's very clear which are bot requests *just from the log information*.

[^1]: People have complained that it's not so simple. [I responded in an update](#update-on-bots)

If bots are not the actual problem, then what is?

The agent providing the attestation is free to use whichever means to approve
or disapprove of a browser. That includes examining whether the browser runs
ad blockers.

Given how advertising networks track users, and user tracking is a practice
that comes under increasing criticism, ad blockers are also gaining in
popularity. Security experts regularly recommend the use of ad blockers as
a cyber security measure -- as ads can be used to side-load malware into an
otherwise legitimate website.

What Google is really after is ad blockers.

### Problems

The downside of this approach is that it opens up a door for arbitrary abuse.
Websites can refuse service unless you install their proprietary data collection
agent. Websites can refuse service if you use the wrong browser -- we'd enter
the {{< external text="browser wars" url="https://en.wikipedia.org/wiki/Browser_wars" archive=false >}}
of the late 90s, with renewed effort.

The day this proposal gets accepted is the day the Open Web is set back by
decades.

In {{< external text="The Future of the Internet -- And How to Stop It"
  url="https://yalebooks.yale.edu/book/9780300151244/the-future-of-the-internet-and-how-to-stop-it/" archive="force" >}},
Jonathan Zittrain lays out, starting with the telephone network, that there
exist "appliances" and "generative systems".

An "appliance" works like any other household appliance, like a toaster. It
has one primary function, and all other functions it may offer are at best
mild variations of the primary one. It toasts.

Zittrain lists the PC and the Internet as examples of generative systems.
Generative systems are not necessarily as complete in functionality as an
appliance -- they provide some basic functionality, but with no specific
primary purpose. The purpose is left to the user. Another way of phrasing
this is to call these things tools, or crafting materials.

Maybe it's worth pointing out the text of the above image at this point:

> The Open Web is a collection of user-developed, non-proprietary frameworks
> for building services, emphasizing interoperability and the balance of data
> access and ownership between providers and end-users.

Generative systems are significantly more impactful than appliances precisely
because they leverage the user's imagination to address their own needs. They
crowdsource meaning at global scale. This is what makes the Internet so
powerful.

Attestations from an agent doing arbitrary things effectively turns the web
into an appliance -- or, to be more specific, it turns the browser into an
extension of an appliance website.

Of course website owners are free to build appliances. They already are doing
so. But this reduces the usefulness of "the web" step by step, until the
generative open web is lost. We're already seeing the negative effects of this,
and technology like the proposed would only accelerate the trend.

Google does not need to mind. The inexorable logic of capitalism means that
businesses that managed to build upon a generative system to rise, now have
to turn that same system into an appliance for their own needs, or risk being
open to competition.

## Reactions

The reactions to the post were diverse, and it's worth addressing a few.

1. **This does not imply accessibility or inclusion issues!** -- Yes and no.
  No, in principle this technology does not *cause* accessibility issues. But
  the {{< external text="pareto principle"
    url="https://en.wikipedia.org/wiki/Pareto_principle" archive=false >}} implies that effort
  should be spent on 20% of the browser market because that captures 80% of the
  users -- and cost effectiveness then mandates that the remaining 20% of users
  should be ignored, because they'll cost too much to support.
  \
  That is exactly the worry here. Marginalized groups which need specialized
  browser -- for example with good screen reader capability, or capable of
  running on cheaper/older devices -- will effectively be excluded by rational
  business logic.


1. **Worry about access, not about technology!** -- The argument is that good
  regulatory frameworks will legally mandate access, so that should be the
  focus.
  \
  This is true, but not enough. The two problems with this line of thinking are
  that first, good regulatory frameworks are rare. And part of the reason for
  that is the second problem, namely that technology moves faster than the law.
  \
  Which means that worrying about access *instead* of technology will still
  exclude marginalized groups in practice. What is required instead is to
  worry about technology in the short term, and regulation in the long term.

1. **It is legitimate for businesses to wish to protect their interests.** --
  That is a debatable point. Businesses "protecting their interests" to the
  detriment of people is not legitimate. But within the bounds of that, sure,
  why not.
  \
  Here's the problem, though: the Internet and open web are *generative
  systems*, which means the reason they have a positive impact is because
  people can decide how to use them. The moment this decision making power
  is curtailed, the system shifts towards an *appliance*.
  \
  If businesses protect their interests by reducing a former generative
  system to an appliance, by definition this is to the detriment of people,
  and no longer legitimate.

## Updates

### 2023-07-21

After raising [a code of conduct violation](https://github.com/w3c/PWETF/issues/306)
for the proposal with the W3C's group responsible for said code, I was
rightly told that they are not responsible (TL;DR, see the link). I then
sent an email to the ombudspeople at W3C which I'll reproduce here:

```
From jens@OBFUSCATED Fri Jul 21 17:23:38 2023
Date: Fri, 21 Jul 2023 17:23:38 +0200
From: "Jens Finkhaeuser" <jens@OBFUSCATED>
To: ombuds@w3.org
Subject: Web Environment Integrity proposal

Dear Ombudspeople of the W3C,

I wish to raise concerns about the behaviour of the people working on
the Web Environment Integrity proposal, as well as the proposal
itself.

https://github.com/RupertBenWiser/Web-Environment-Integrity/

In particular, I would like to draw your attention to isuse #131 in
their working repository:

https://github.com/RupertBenWiser/Web-Environment-Integrity/issues/131

The group working on this claims to adhere to the W3C Code of Ethics
and Professional Conduct. However, as documented in this issue, they
violate said code.

As a bit of background, WEI is a deeply unethical proposal that
suggests to use cryptographic means to permit websites to deny
services to users based on arbitrary user metadata. Such metadata
is to be processed by agents running on the user's machine, which
provide attestations about the browser environment.

One such proposed service is Google Play, which has access to personal
identifiable information (PII). This turns the proposal into a
technological mechanism for discrimination.

The community has raised and is raising issues about the ethics of
such a proposal, which led me to find the W3C code of ethics.
Unfortunately, as was pointed out to me, the code of ethics does not
concern the content of proposals - merely the conduct of participants.

Unfortunately, some maintainers of the repository have taken to
closing issues raised by the community -- the fourth bullet point in
the "participant" explanation of the code ("Anyone from the Public
partaking in the W3C work environment (e.g. commenting on our specs,
(...)"). This violates several points in section 3.1 of the same
document, whereby use of reasonable means to process diverse views are
required.

It seems to be the case that this proposal has not yet made it to a
W3C group. However, its maintainers already violate the W3C code of
ethics in practice in the run-up to such an activity. In the meantime,
even though the code is not directly applicable to the proposal
contents, it nonetheless violates said code in spirit.

It seems appropriate that W3C does not permit this proposal to go ahead
in any formal fashion.

Kind regards,
  Jens Finkhaeuser
```

### 2023-07-22

Google has now closed the ability to contribute to the
repository, including by raising or commenting on issues.

### 2023-07-26 -- #1

Apple has already shipped a similar API for about a year.

As described on the Apple developer blog, [Private Access Tokens](https://developer.apple.com/news/?id=huqjyh7k)
implement pretty much the same mechanism as Google's WEI.

There are a few notable differences in *tone*, however. The first is a direct
quote from the above blog post:

> Note: When you send token challenges, don’t block the main page load.
  Make sure that any clients that don’t support tokens still can access
  your website!

This note is not doing anything in itself, but it *does* stand in stark
contrast to the motivations documented in WEI. In particular, the proposal
suggests that private access tokens should be used instead of e.g. CAPTCHAs
or other, more disruptive authentication mechanisms.

The second important difference is in the actual details of the proposal.
It states that the token issuer is an external web service rather than some
opaque process running on the user's machine. Suggested are some CDN
providers' services. The clear message of intent here is that this is
supposed to be a mechanism by which CDNs authenticate a request to the
source.

The protocol by which this is to be done is defined by the [IETF PrivacyPass Working Group](https://datatracker.ietf.org/wg/privacypass/about/).
Reading through [the protocol draft](https://www.ietf.org/archive/id/draft-ietf-privacypass-protocol-11.html),
it furthermore becomes clear that the data the client is supposed to send
to the issuer is... nothing but the challenge sent by the server, in an
obfuscated (hashed) manner.

This leads to two conclusions.

1. No personal data is being leaked.
1. There is no checking of the "environment", aka the browser and its plugins,
   that can prevent some browsers from receiving an attestation.

**Not so fast!**

As [has been pointed out to me](https://chaos.social/@Mayabotics@tech.lgbt/110780812126529634),
this analysis is incomplete. That is because the specifications provided by
the PrivacyPass WG are incomplete.

What is missing from the specification set is how client and attester
interact. The *issuer*, as described above, is oblivious to PII. However,
it can influence which attester to use.

The attester, on the other hand, is an unknown. Various parts of the specs
refer to *possible* ways this may occur, leaving any specifics unwritten.
While this includes the possibility of clients not sending sensitive
attributes to the attester, no mention of the consequences of that is made
(though one can assume that attestation then fails).

This openness effectively means that the same model as WEI with the same
problems can be implemented -- a fact the architecture document acknowledges
in [section 5.1 "Discriminatory Treatment"](https://ietf-wg-privacypass.github.io/base-drafts/draft-ietf-privacypass-architecture.html#section-5.1-1).

I have to thank {{< fedi "@Mayabotics@tech.lgbt" >}} for nudging me to give
those parts a closer look! I was too focused on the issuer protocol.

### 2023-07-26 -- #2

[Mozilla has taken a stance against WEI](https://github.com/mozilla/standards-positions/issues/852#issuecomment-1648820747)
writing:

> Mozilla opposes this proposal because it contradicts our principles and
  vision for the Web.

That's something, at least.

### 2023-07-26 -- #3

As a honourable mention, the maintainer of the Google
repository has published a [personal blog post about their experience](https://blog.yoav.ws/posts/web_platform_change_you_do_not_like/),
which contains some fair and some unfair bits.

However, one of the points bears commenting on:

> Don't assume a hidden agenda
>
> When thinking about a new proposal, it's often safe to assume that Occam's
  razor is applicable and the reason it is being proposed is that the team
  proposing it is trying to tackle the use cases the proposal handles. While
  the full set of organizational motivations behind supporting certain use
  cases may not always public (e.g. a new device type not yet announced,
  legal obligations, etc), the use cases themselves should give a clear enough
  picture of what is being solved.

There are a few comments to this:

1. Given that Apple's mechanism is undergoing IETF standardization, the only
   reason for an opposing mechanism is that the existing approach does not
   fulfil Google's needs.
1. Google *clearly* states its needs in its use cases. There is no hidden
   agenda that people complain about, but rather the agenda as it is stated
   clearly in plain text.

This comment actually *confirms* the community's worst fears.

### 2023-07-26 -- #4 {#update-on-bots}

[Some HackerNews folk have called me "embarrassingly
uninformed" about how to detect bots](https://news.ycombinator.com/item?id=36875164).

I should ignore that, but this admittedly stings a little, given that I
worked on threat management solutions in a former life. With that in mind,
at least my data set is *a lot* larger than the comments suggest.

But the gist of the criticism is true to the extent that I've written bots
myself that have circumvented stronger security measures than a user agent
check. Given sufficient motivation, it's in easy reach.

Which begs the question: how *would* one write a bot that circumvents this
kind of attestation mechanism?

Whether it's WEI or PrivacyPass, the weak spot is the attester. Either an
attack manages to convince the attester that a client is legitimate. Or
a legitimate client is used, but in a way the attester will not complain
about.

The latter could be as simple as using [Selenium WebDriver](https://www.selenium.dev/documentation/webdriver/)
to make requests with a legitimate browser. I suspect it'll be a little
more difficult than that in practice.

But that is beside the point -- the real point is that *bots can be as
sophisticated as a real browser, including being able to pass attestation*.

Which means WEI is, again, not really about bots *at all*, which was the
original point these fine folk seemingly missed.

### 2023-07-26 -- #5

Today is a day for lots of updates, as information on
WEI continues to accumulate.

[Chromium already has commits for WEI](https://github.com/chromium/chromium/commit/6f47a22906b2899412e79a2727355efa9cc8f5bd),
which probably means this stuff will be out a lot sooner than the specs
solidify.

### 2023-07-28

The HackerNews post by now contains [a very useful comment](https://news.ycombinator.com/item?id=36880224)
that complaining on GitHub to Google is pointless. This is part of why I
raised this to W3C.

I'll copy a bit from the comment below:

> My thoughts exactly. These GitHub protests, while emotionally satisfying, do not work. Google does not care and they are already drunk on monopolist power.
>
> Contact info for antitrust authorities:
>
> US:
> 
> - https://www.ftc.gov/enforcement/report-antitrust-violation
> - antitrust@ftc.gov
> 
> EU:
> 
> - https://competition-policy.ec.europa.eu/antitrust/contact_en
> - comp-greffe-antitrust@ec.europa.eu
> 
> UK:
> 
> - https://www.gov.uk/guidance/tell-the-cma-about-a-competition-or-market-problem
> - general.enquiries@cma.gov.uk
> 
> India:
> 
> - https://www.cci.gov.in/antitrust/
> 
> I could not find an easy contact method for filing a complaint for the CCI, but it looks like this is the process?
> 
> - https://www.cci.gov.in/filing/atd
> 
> Canada:
> 
> - https://www.competitionbureau.gc.ca/eic/site/cb-bc.nsf/frm-eng/GH%C3%89T-7TDNA5

I could not agree more. But anti-trust is only one angle, and PrivacyPass (Apple's
Private Access Tokens) suffers from similar issues.

Here in the EU, you can also:

- Raise your concerns with the [European Data Protection Board](https://edpb.europa.eu/about-edpb/about-edpb/members_en)
  on how discriminatory access may simply violate the lawfulness of processing
  personal data as described in [Article 6 of GDPR](https://gdpr-info.eu/art-6-gdpr/).

- [Contact the European Data Protection Supervisor](https://edps.europa.eu/about-edps/contact_en)
  with similar concerns.

- [Contact the European Agency for Fundamental Rights](http://fra.europa.eu/en/contact)
  on how technology that discriminates users based on opaque practices is likely
  a violation of fundamental human rights in the EU.

As well, of course, as contacting similar institutions in your home country.

With regards to PrivacyPass specifically, joining [the IETF PrivacyPass working group](https://datatracker.ietf.org/wg/privacypass/about/)
is as easy as joining a mailing list, and raising your concerns there. You can
then vote against adoption when a draft makes it far enough.

### 2023-08-06

[Alex Russell of Google/Chrome/Blink is trying to reframe WEI](https://web.archive.org/web/20230807124006/https://toot.cafe/@slightlyoff/110841255265717224),
as a bunch of folk "doing All The Good One Can Do for the the web", but getting
caught up what they can do rather than what they should do.

The rationale posited is that "Google doesn't have the sort of hierarchy that
would force you to ask anyone 'should we?'".

There are two very immediately disturbing things about this:

1. The assertion that Google does not ask this question as a matter of process,
   and
1. the notion that individual Googler's won't ask this question irrespective of
   what Google's processes are.

Either alone is damning, both together is a declaration of moral bankruptcy.

Understandably, reactions to this thread are not particularly supportive of
this interpretation. An official reaction from Google seems to be missing
still at this point in time.

---

The Interpeer Project's mission is to build next-generation Internet technology
that re-focuses on people over businesses. You can support us by
[donating today]({{< relref donations >}}).
