---
title: "FOSSASIA'21: Designing a Human Centric Next Generation Internet"
date: 2021-03-18T21:00:00+00:00
tags: ['Interpeer', 'Conference', 'Talk', 'FOSSASIA', 'FOSSASIA21', 'FOSSASIA2021']
author: Jens Finkhaeuser
categories: ['conferences']
video: https://www.youtube.com/8lPMe31KjvM?start=30032
---

Just as my previous FOSDEM talk, my presentation at {{< external
  text="FOSSASIA" url="https://fossasia.org/" archive=false >}}
was titled "Designing a Human Centric Next Generation Internet".

Trying to learn from the previous presentation -- and having less time, and
adding the pressure of a live event -- I tried to focus more on a future
Internet architecture.

{{< youtube "8lPMe31KjvM?start=30032" >}}

The benefit of that approach is that it can be done as an analysis of the
web's more-or-less-achieved REST architecture and it's successes and failures.
Reading {{% extref "rest" "Roy Fielding's REST Dissertation" %}}
is certainly highly recommended!

The video starts about 8 hours and 20 minutes into the day, and the embed is
set to begin just there.
