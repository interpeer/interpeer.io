---
title: "New Year, New Website"
date: 2023-01-26T08:30:00+00:00
tags: ['Interpeer', 'non-profit', 'ngo']
categories: ['announcements', 'conferences']
author: Jens Finkhaeuser
image: 'happy2023.png'
---

## Happy New Year 2023!

Is a new website a good reason for a blog post? Maybe on a slow news day. But
we have not been updating you here for a while, so let's use the change of year
for a recap and update.

{{< fig src="happy2023.png" >}}

## 2022 Recap

2022 has been a very busy year for us. Our R&D focus has shifted from lower
level transport protocols to information-centric networking concerns. This
rounds out the overall vision of a human-centric ICN stack.

### Interpeer NGO Founded

- [As we wrote previously]({{< relref "20220601_non_profit_paperwork_signed" >}}),
  we got the paperwork for the non-profit filed. Amongst other things, that means
  you can now support us with [donations]({{< relref donations >}}) (which we'd
  greatly appreciate!).
- We developed [vessel]({{< relref "projects/vessel" >}}), a streaming and multi-authoring
  capable container format for ICN.
- In a similar vein, [CAProck]({{< relref "projects/caprock" >}}) contributes to ICN by
  offering a means for distributed authorization. We've also made a start on
  submitting {{< i-d text="this work as Internet-Drafts"
    name="draft-jfinkhaeuser-caps-for-distributed-auth"
    >}}.
- A lot of development effort went into [wyrd]({{< relref "projects/wyrd" >}}), an
  implementation of a conflict-free, replicated data type.
- We attended {{< external text="IETF115" url="https://www.ietf.org/blog/ietf115-catchup/"
   archive=false
  >}} where we presented *vessel*, as well as the general problem with the web
  that we're aiming to resolve.
- As always, development on other [projects]({{< relref projects >}}) did not
  stop. We're now up to 8 software projects, with more to follow.

## New Year 2023

So what's in store for 2023? In short, more of the same.

- As you see, we've got a new website! This allows us to present project
  information a lot better than we did previously. And maybe it's enough to
  get more blog posts out again. But this is just a start, and will evolve
  over time. If you have {{< external text="feedback on the site"
    url="https://codeberg.org/interpeer/interpeer.org/issues/" archive=false >}}, we'd
  love to hear from you! The website is made with help from {{< external
    text="autonomic" url="https://autonomic.zone/" archive=false >}}.
- We're planning to attend {{< external text="FOSDEM 2023"
    url="https://fosdem.org/2023/" archive=false >}}. The schedule for talks is not yet
  confirmed, so we can't post links yet. But we hope to see you there
  either way!
- Related to FOSDEM, {{< external text="OFFDEM"
    url="https://foss.events/2023/02-04-offdem.html" archive="force" >}} is a parallel
  event where we'll be discussing *vessel* more. If you have questions
  about that, that's the place to be!
- Development work is going to focus on connecting the ICN to the transport
  layers. We also hope to provide better APIs for application developers in
  the near future.

## Acknowledgements

Looking back on 2022, it's clear that none of what we did would have been
possible without support!

We'd like to thank {{< external text="Adrian Cochrane of OpenWork Ltd."
  url="https://www.openwork.nz/" archive=false >}} for his work on *wyrd*. His contributions
were invaluable!

As far as funding goes, we rely completely on research grants at the moment.
As always, {{% extref "nlnet" %}}
has been an excellent supporter, and is one of the best places for FOSS
grants in Europe! But the recent ICN-oriented work is mostly supported by
the {{% extref "isoc" "Internet Society Foundation" %}}. We're very grateful
for the cooperation, and the contacts they forged with other researchers!

You guys rock!
