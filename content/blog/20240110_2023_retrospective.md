---
title: "2023 Retrospective"
date: 2024-01-10T09:00:00+00:00
tags: ['Interpeer', 'retrospective', '2023', 'progress', 'techtree', 'technology tree']
author: Jens Finkhaeuser
categories: ['miscellaneous']
---

With 2023 over, and some time between now and the last update, it's perhaps a
good moment to reflect on what happened in 2023 and where the project is right
now.

# Achievements

Last year saw the closing of our grant from {{% extref isoc %}},
who have been excellent partners for the past two years! I don't think I can
overstate how important this grant has been for our work.

In particular, a longer-running grant meant the focus could be more on actual
research -- trying things out and writing up the results -- and less on
implementation. Though of course, as always, we strive to do both.

In this case, it turned out that the research was necessary. While of course
you start any grant work with a proposal in which you outline what you intend
to do and achieve, findings along the way can change your course.

The grant funded much of the work on [CAProck]({{< relref "projects/caprock" >}})
and [vessel]({{< relref "projects/vessel" >}}), both of which are rather
foundatinal technologies.

With *CAProck* it turns out that there is a fair bit of community interest.
At any rate, it is the project for which we receive the most feedback, all of it
highly constructive so far. That has been promising, and it means we'll continue
to base future work on this library.

Vessel got rather more mixed reactions. In the [ICN]({{< relref "knowledge_base/information_centric_networking" >}})
community, the focus tends to be a lot more on *immutable* resources, with
mutability layered on top via mutable manifests of sorts. While that approach is
sound from a layering and dogfooding perspective, in our opinion, it does not
really reflect the way people interact with computer systems: immutability of
data tends to be the special case, so using it as a base for mutability seems
to complicate matters more than it helps.

However, this means on reflection that Interpeer's approach is sufficiently
untypical for ICN that it's perhaps best to clarify in which way it is similar
and different. As it turns out, that was also the stated goal of the ISOC
foundation grant, to arrive at a novel architecture.

The result is the [Interpeer Architecture Draft](https://specs.interpeer.org/draft-jfinkhaeuser-interpeer/),
which not only examines ICN as an alternative, but also Delay Tolerant Networking
(DTN) and the World Wide Web, and provides distinct approaches and features from either,
while being able to somewhat emulate all their modes, except with better
protection of fundamental human rights.

As an engineer, it feels a little unsatisfying that the most remarkable result
of a grant spanning multiple years is a mere document, even if this is par for
the course for research.

But more than a document, it also maps out the future work to be done in this
project.

# Outlook

The current focus of our work is on integration, which is a little premature
in some ways, given so much of the stack is still missing. However, we need
to have *some* target to work towards -- so a "vertical slice" approach that
produces a minimal target based on currently completed technology is not a bad
approach. This work is the current focus of a grant from {{% extref ngi NGI %}},
and a first result is the [GObject SDK]({{< relref "projects/sdks/gobject" >}}).

The SDK allows something very simple, so simple it doesn't seem particularly
powerful at first glance: it allows setting and getting named and typed
properties.

The interesting part is that these properties are stored in the [Wyrd]({{<
  relref "projects/wyrd" >}}) conflict-free, replicated data type, and then
serialized to a [vessel]({{< relref "projects/vessel" >}}) resource. This in
turn then becomes the API for overall architecture, which deals in resources.
In other words, the SDK provides a taste of what application developers can
expect, except in its final form these will be local-first, opportunistically
synchronized distributed resources.

The next step is to replicate much the same API on {{< external text="Android"
  url="https://source.android.com/" archive=false >}}, which mainly demonstrates -- and later
enables -- cross-platform synchronization.

To illustrate what kind of work is necessary beyond this, we borrowed a page
from our spiritual sibling project {{% extref librecast Librecast %}},
and built a technology tree. This shows
a mixture of projects and feature milestones, and their dependencies.

At the stage of writing this, we're not quite halfway to the goal. But it bears
mentioning that the ongoing work that is in parallel to development, here
highlighted in our salmon brand color, is fairly time-consuming. In particular,
this refers to the [specifications](https://specs.interpeer.org). The benefit
of always producing specifications (as much as possible), however, is that we're
not just writing code -- we're creating something that can even outlast this
project here.

{{< fig src="techtree.svg"
    page="projects"
    alt="Interpeer Technology Tree"
    rel="external"
    target="\_blank"
    width="100%"
    nolink="true"
    caption="Technology Tree"
>}}

# Funding

With the outlook showing a long road ahead still, and the largest grant ending,
it's worth talking about future funding.

Disappointingly, a number of grant applications last year fell through. In some
cases, the grant giver was clearly looking for other things, and the application
was more than a little hopeful. But in other cases, it seemed to fit, so not
getting selected stings a little.

Luckily, the funding landscape has changed since we started, and new funds open
up every few months. Often they're specialised and exclude green field R&D such
as our work.

NGI's funding is relatively reliable here, but has a major drawback: it is aimed
at the individual contributor scale. Other funds have other drawbacks: while they
offer significantly larger sums, they also require a lot more management overhead.

With the non-profit in place, our plan for 2024 is to approach larger funds and
shouldering this extra effort, but the run-up to those is long. This creates a
projected funding gap between spring and autumn of 2024. We may be able to fill
this with funding from NGI again, but words such as "may" are a little
terrifying.

Our individual contributors are highly skilled individuals and will always be
able to feed themselves. But for the *project*, it would be nice to have some
guaranteed continuity, if only in the form of keeping this website alive.

Our total monthly costs here are relatively low: they include hosting, as well
as some accounting and tax software fees. In total, this falls somewhere between
EUR 100.- and EUR 150.- per month.

*We can keep this website and related resources up almost indefinitely if those
funds are covered*, which brings me to an appeal. It'd be grand if a few of you
took this up!

{{<callout>}}
[You can help with just a few euros a month]({{<relref "donations" >}}) to keep the lights on!
{{</callout>}}

---

Having started this project some four years ago now, I have to say I'm happy to
be where we are.

When I take my younger self's perspective, the perspective of someone who --
relatively speaking -- made up for lack of design with speedy typing, I feel bad
about having relatively little source code to show. But when I head back from a
conference, having had conversations with professionals from this or other fields,
I always feel thrilled about what we've done so far.

Clearing that architecture document hurdle was a major step in shaping future
work in particular. So for 2024, let's continue this, and make more of those
tech tree hexes green!
