---
title: "Impressions from the EU Open Source Policy Summit 2023"
date: 2023-02-04T10:00:00+00:00
author: Jens Finkhaeuser
categories: ['conferences']
image: summit-website.png
tags:
- Conference
- OpenForum Europe
- FOSS
- European Commission
- digital sovereignty
- human rights
- permissionless
- innovation
video: https://www.youtube.com/live/QTQEzKQFjXg
---

Yesterday, I found myself accidentally live-blogging some thoughts on the
{{< external url="https://openforumeurope.org/event/the-eu-open-source-policy-summit-2023/"
  text="EU Open Source Policy Summit 2023" archive="force" >}} event organized by 
{{< external url="https://openforumeurope.org/" text="OpenForum Europe" archive=false >}}. I
say "accidental", because I didn't plan on doing so, but the first post got
enough interest that I continued. It's only fair to summarize my impressions
today, after the fact.

{{< fig src="summit-website.png"
    alt="EU Open Source Policy Summit 2023"
    width="100%"
>}}

I'm leaving the conference with some mixed feelings. It's been very clear
that decades of FOSS advocacy have only recently gained traction, and
there is still a lot of work to be done to bridge the gap between politics
and FOSS.

At the same time, panelists correctly pointed out that Open Source Policy
Offices, such as the {{< fedi "@EC_OSPO@social.network.europa.eu"
  "European Commission OSPO" >}} are very new institutions, dating back to
only 2-3 years ago -- and many member states of the EU have created
similar offices to help with this work.

In the end, this leaves me largely hopeful for the future of FOSS in the
European Union, and indeed the world.

## Politics

The politics angle to FOSS is briefly summarized, and every politician
that presented was pushing more or less the same angle: {{< external
  url="https://en.wikipedia.org/wiki/It%27s_the_economy,_stupid"
  text="It's the economy, stupid!" archive=false >}}.

That deserves some more explanation, because it's not quite as simple
as that, either. It breaks down to a few simple points:

1. In the software industry in general, the EU has been lagging behind the
   USA.
1. Increasingly, this is seen as a risk not only to the economy, but as
   a political risk to [digital sovereignty]({{< relref "knowledge_base/digital_sovereignty" >}}).
1. In order to catch up, the European Union needs to embrace openness,
  as only open collaboration can help here. The phrase "standing on the shoulders of giants"
  was used.
1. Next to funding FOSS, the political focus for this year is turning
  education towards imparting a better understanding of FOSS and open
  collaboration.
1. Finally, it is also clear to politicians that "european values", which
  they more or less equate with human rights, are best served by the bottom-up
  approach that FOSS embodies.

{{< fedi "@webmink@meshed.cloud" "Simon Phipps" >}} from OSI made the excellent
point that politician's access to software stakeholders is still very much defined
by the industrial revolution: they have established connections to producers,
consumers, and workers. But FOSS contributors tend to have a mixture of concerns
that crosses these boundaries, which creates a fourth mode that falls between
the cracks at this time.

## Industry

OpenForum Europe is mostly an industry association, so a heavy industry presence
was to be expected. Most of what was being said by industry representatives is
largely uninteresting to repeat, but a few points stand out, even if they were
accidentally made.

First and foremost, there is the above mentioned gap between FOSS practitioners
and politics. And despite decades of FOSS advocacy, industry managed to step
neatly into this gap. As a consequence, a lot of what politicians think they know
about FOSS is actually only the industry's view.

In the past, that view was heavy on {{<external url="https://en.wikipedia.org/wiki/Fear,_uncertainty,_and_doubt"
  text="fear, uncertainty and doubt" archive=false >}}. But in recent years, FOSS businesses
have realized they can sell FOSS to politics as a solution to e.g. digital sovereignty
or economic concerns -- as long as it is the FOSS solutions they provide.

Consequently, industry representatives mostly spoke to the European Commission
at the event, influencing policy in their favour. This is to be expected, but
it highlights very strongly how much the FOSS community has to step up its game.

### OpenWashing

Several speakers and panelists called out industry [#OpenWashing]({{< relref
    "knowledge_base/open_washing" >}}), which I have
also previously called [#FOSSWashing]({{< relref "knowledge_base/foss_washing"
    >}}). Gaël Blondelle of the {{< external
  url="https://eclipse.org" text="Eclipse Foundation" archive=false >}} did so, as well
as Simon Phipps and {{< fedi
  "@carlopiana@mastodon.uno" "Carlo Piana" >}} of the {{< external
    url="https://opensource.org" text="Open Source Initivative" archive=false >}}. I'm very
glad that these voices also made it to the ears of the European Commission.

It's particularly interesting to see that some of the organizations guilty
of this practice were, of course, in the room.

## The Rise of OSPOs

As mentioned before, OSPOs have been cropping up in various EU member states,
precisely to help facilitate communications between politicians and the community.
Here, some excellent news for the future of FOSS is hidden: every panelist or
speaker adjacent to an OSPO or funding agency (more on that below) showed a
high level of understanding of the FOSS community at large.

This is a massive win for the community that is easily overlooked. I myself have
complained to the EC OSPO that their public code repository is weirdly gated,
which contradicts the spirit of permissionless collaboration.

But at the conference, it also became very obvious that these kinds of compromises
are due to the amount of work ahead of OSPOs, in actually opening up the political
approach to software and FOSS. At the time, they can't always do the things they
know they need to do -- and are working hard to change that.

If you have an OSPO in your country, it's probably a great idea to reach out!

## Civil Society

Frustratingly little has been made of civil society's role as a FOSS stakeholder.
Of course, plenty of members from foundations were invited as panelists and gave
their views; this is FOSS advocacy as we know it.

But I heard mention only twice in the entire day, that foundations may be the right
stakeholders to fund in order to push FOSS forward.

Yes, industry maintains a large chunk of FOSS. It's politically expedient and
correct to involve industry. But this point of view sees only the tip of the iceberg,
and ignores the vast amounts of FOSS developers that work on projects outside of
their paid job.

Nonetheless, the point was raised, and by my impression, it was well received.

## Funding

While there was a panel of funding FOSS, I found it sadly devoid of actual
funding discussions.

However, similar to OSPOs, it was very clear that funding organization such
as {{% extref "nlnet" %}},
{{< fedi "@PrototypeFund@mastodon.social" "Prototype Fund" >}} or the newer
{{% extref "stf" %}} also get FOSS funding needs quite well.

It's continuous work with the European Commission that has allowed these funds
to emerge, and have lightweight funding programmes -- as opposed to the
Horizon Europe calls from which some of the money flows.

### Security

One point made by {{< external url="https://en.wikipedia.org/wiki/Brian_Behlendorf"
  text="Brian Behlendorf" archive=false >}} of the {{% extref "ossf" %}} was very
salient, comparing the cost
of preventing the "next {{<external url="https://en.wikipedia.org/wiki/Log4Shell"
  text="Log4Shell" archive=false >}}" security issue vs. suffering its consequence.

The calculation for giving the top most critical FOSS projects security audits
comes to millions -- but the fallout of the flaw caused billions in damage.

## European Commission

I had a chance to speak to some members of the European Commission in charge
of funding. I think it's well worth pointing out that these folk are
very much trying to do the right thing for FOSS.

The conversations here highlight to me how the EC stands and tries to negotiate
between FOSS and politics. They still have some learning to do of their own,
but also have political concerns to weigh.

1. I managed to impress on them that a lot of funding to date is very much
   focused on objectives. FOSS projects also need funding for maintenance,
   which is currently very underserved.
1. Similarly, and maybe more in the Interpeer Project's direct interest, this
   focus on objectives is informed by industry's current needs -- not on
   improving stuff in general. I managed to highlight that longer term, "moonshot"
   type funding is also necessary.
1. Finally, as politicians were impressing the need to develop citizen's skills
   in FOSS, it was important to me to let the EC know that some of those skills
   are not actually software related. We're seeing more designers move into
   FOSS, but there are fairly few project or product management practitioners
   spending time in the community. Some of the learning that needs to happen
   lies in incentivizing these and other folk to join the FOSS community.

## Miscellaneous

A number of miscellaneous points stood out to me:

- Calling for open collaboration between FOSS companies is a good thing. The point
  was that each player attempting to compete with well-established proprietary
  vendors is not a winning game; instead, concentrating on one vertical and opening
  up APIs to collaborate with other entities is. While I agree with this point from
  a business perspective, from an innovation perspective, it implies that nobody
  should try to improve on the state of the art by offering an alternative solution.
- Given the industry focus, it was to be expected but very disappointing to only hear
  the use of "open source", never "free" or "libre" software.
  - As a related point, the "digital sovereignty" term was identified as the term that
    opened up FOSS to politics -- both "free software" and "open source" are abstract
    terms divorced from political concerns, while sovereignty is the stuff of politics.
- The term "permissionless innovation" was used a fair bit, and it's good that people
  have an understanding that this is what powers FOSS. At the same time, access to
  funding is currently still reserved to those "permitted" to innovate by the grant
  making organization. True permissionless innovation requires a significant shift
  in policy here.
- Interoperability is seen as a key factor to digital sovereignty, as it permits
  swapping out services in a larger architecture. It reminds me of a friend and colleague
  who would chant "interoperate or die!" some twenty years ago. To me, it's a clear
  reminder how slow political shifts are.
- In terms of cybersecurity, the point was well made that there is not much of a difference
  between FOSS and propriety software here.

## Closing Thoughts

In conclusion to this summary, I find myself actually very hopeful for the state
of FOSS in Europe. Yes, industry influence on politics is still overly large.
No, politics do not yet understand the FOSS community in general.

But enough has shifted in these last five years or so with new funds opening up
from the European Commission and OSPOs springing up in governments all over the
European Union, that things do feel like the tentative beginnings of a very
significant shift.

We cannot lose momentum here, however.

**Update 2023-02-08:** The recording of the summit can now be found below. Do you
think the above captured the event well enough? Let us know!

{{< youtube QTQEzKQFjXg >}}
