---
title: "FOSDEM'22: On the Far Side of REST"
date: 2022-02-05T18:30:00+00:00
tags: ['Interpeer', 'Conference', 'Talk', 'FOSDEM', 'FOSDEM22', 'FOSDEM2022']
author: Jens Finkhaeuser
categories: ['conferences']
video: https://makertube.net/w/iRx5GGqSLfEEsJoFopVcEF
---

February is {{< external text="FOSDEM" url="https://fosdem.org" archive=false >}} month. This
year, I was able to give a talk on how REST contributes to centralizing the web,
and what could be done about that.

{{< peertube id="9095f4d2-69d6-4c9f-b720-884c6c0efc87"
  title="FOSDEM'22: On the Far Side of REST"
  >}}
