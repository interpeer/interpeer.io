---
title: Bitcoin is a Capitalist Nightmare
date: 2018-07-03 16:25:27.689000+00:00
tags: ['bitcoin', 'crypto', 'cryptocurrency', 'blockchain', 'capitalism', 'p2p']
author: Jens Finkhaeuser
categories: ['miscellaneous']
image: "bitcoin_vice.jpeg"
---

{{< fig src="bitcoin_vice.jpeg"
    alt="A symbolic bitcoin in a vice."
    width="100%"
>}}

## A nightmare of capitalism, that is, not a nightmare for capitalism.

Let me explain.

Throughout its history, Bitcoin has been lauded as a currency that can set us
free. It seems as if every other week, someone discovers bitcoin's
decentralized nature, and concludes that this makes it the most democratic
currency possible. {{< external
  text="Here's one very recent such story."
  url="https://hackernoon.com/why-everyone-missed-the-most-mind-blowing-feature-of-cryptocurrency-860c3f25f1fb"
  title="Why Everyone Missed the Most Mind-Blowing Feature of Cryptocurrency"
  archive="force" >}}

It's not that these views are *completely* wrong. On the surface, they're
quite correct. But they miss the deeper workings of the cryptocurrency, and
thereby become inadvertently -- and somewhat insidiously -- misleading.

## Bitcoin Recap

Bitcoin are mined, and bitcoin is a distributed ledger, called blockchain.
Those two concepts need to be understood for the remainder of this text.
Luckily, the level of understanding needs only be superficial.

> The sticklers for detail might note that the following description is
> somewhat inaccurate; it is, however, accurate enough for the conclusions
> drawn in this text. I value the concept explanation higher than the precision
> here. Mea culpa.

Bitcoin mining refers to a computer solving a hard mathematical problem. Such
problems take a long time to figure out, even for fast computers. But verifying
the solution is fast, once it's known. That's a fundamental property of all
cryptocurrencies, and it lets all participants ensure that the solutions other
participants provided are correct.

Once such a hard problem is solved, the solution is attached to the distributed
ledger, using a cryptographic hash method. The most important properties of
these hashes for our purposes is that even small variations in input produce
vastly different outputs, and that it's very unlikely for two inputs to
produce the same output. These properties make hashes useful for detecting
tampering.

Attaching a solution to the distributed ledger means taking a hash over the
entire ledger so far, and then also over the newly found solution. Then, the
new hash is appended to the ledger. By including both the previous data and
the new data in the hash, participants can:

* Verify the solution to the hard mathematical problem, proving that some
  computer spent significant time in creating the solution.
* Verify that this solution is "the next" in the ledger, by comparing the
  cryptographic hash to the ledger's contents; if the hash matches, the
  contents are not tampered with.

And that's how participants know that the ledger and all the transactions
within it are valid and whom they "belong" to.

Or do they?

There is a crucial aspect to decentralized systems, which is also included in
bitcoin, and that is consensus building.

The basic issue is this: what happens if two participants solve the same hard
mathematical problem simultaneously? Who is credited with the solution? That
sort of thing can easily happen, because there is no centralized authority
distributing problem solving work to participants; instead, each participant
can autonomously decide to pick a problem -- potentially the same problem --
to solve.

There are a number of possible ways to solve this kind of consensus issue,
but bitcoin opts for the simple and democratic version: every participant
"votes" by signing the ledger, and a simple majority decides.

And that's a problem.

## Who controls Bitcoin?

The simple answer is {{< external
  text="China"
  url="https://www.ccn.com/china-now-controls-bitcoin-thats-just-beginning/"
  title="China now Controls Bitcoin (and that's just the Beginning)"
  archive="force" >}}.
And that's old news, but not often reported.

The more complex answer is that the voting process in bitcoin isn't as
democratic as it seems at first glance.

A few paragraphs ago, I wrote how every participant votes on the contents of
the ledger. But what does that mean?

First, the term "participant" is a little misleading. It may sound like
"human". And for the majority of people interested in bitcoin, that might
be true. But in reality, each "participant" is a computer -- a machine that
signs off on the ledger on behalf of a human.

If you're like most people, you have a personal or family computer, and if
you use that for bitcoin, then you are fully represented on the bitcoin
ledger by that computer. But for some people that's not true. They own loads
of computers, dedicated to bitcoin.

What that means is that consensus building in bitcoin is not one person, one
vote. It is one person, as many votes (computers) as they can buy.

But it gets worse.

The signing process is somewhat similar to mining, meaning it takes some time
for a computer to complete. So the faster the computer, the faster the vote
can be cast.

Bitcoin does not wait for all votes to be cast in order to declare consensus.
Instead, it simply waits until 50% of the votes have been counted.

And that turns voting into a *race*. If you have a faster computer, your vote
can be counted when someone with a slower machine is left out.

So bitcoin favours those with control over the largest number of fastest
machines.

And that's not individual human beings. It turns out that's a handful of
companies in China.

## Capitalism

People can be a little bit confused about what capitalism is. And that's
because we don't find capitalism in the wild without some checks and balances
imposed on it by some kind of political system.

At it's core, capitalism is essentially "you've got to spend some to earn
some". And that becomes easier, the more you have to spend. Capital attracts
more capital.

*Politics might not help here: depending on your political system, you might
have tax breaks for the wealthy -- which is giving an extra advantage to those
who already have the advantage to begin with.*

This "spend some to earn some" is directly reflected in bitcoin's design.
After all, it takes money to buy and run the amount of computers necessary to
mine the most, and to hold the controlling number of votes in the consensus
algorithm.

## Politics

In 2012, Bruce Bueno de Mesquita and Alastair Smith published {{< external
  text="The Dictator's Handbook"
  url="https://www.amazon.com/Dictators-Handbook-Behavior-Almost-Politics/dp/1610391845"
  title="The Dictator's Handbook at Amazon"
  archive=false
  >}} -- a book with a rather
sensationalist title, but a much more serious background.

The book is the popular science version of a huge amount of research work the
two undertook for years with colleagues around the world. In it, they examine
political systems all over the world, and establish that political leaders in
democracies and tyrannies aren't all that different.

They meticulously lay out the rules by which leaders are driven to decision
making based on how much money they have to distribute to their supporters --
and related to that, how many supporters they need to appease.

The major difference between democracies and tyrannies appears to be whether
the people need to be taxed to raise this money, or whether natural resources
pay for it all.

There's a great explanatory video. Watch it, I'll wait.

{{< youtube rStL7niR7gs >}}

Chilling, isn't it?

## The Tyranny of Bitcoin

So how does bitcoin fit into this?

It's simple. Mining bitcoin doesn't need all that many people, nor that
many natural resources (other than energy). It utilizes a third resource,
"compute time", which we have seen earlier costs money to create.

That's real money, in most cases. It could also be bitcoin that's used to
buy more computers. Why not? Although as we have seen, having real money to
start with gives you the advantage.

By cutting most natural resources out, bitcoin cleverly disentangles itself
from old wealth like land ownership. That's the democratic looking part. By
cutting out the need for people's work, though, it also removes all political
incentives for educating people and keeping them healthy.

So if the proponent's vision becomes reality, and bitcoin replaces real money?

Then it becomes a monetary system that is based on a resource controlled
entirely by the wealthiest individuals or companies, with no need whatsoever
to rely further on natural resources, nor on the average individual for raising
more.

It's the perfect foundation for a globe-spanning tyranny, fuelled by
capitalistic principles.

I don't know what to call this other than a nightmare.

> *You might also be interested in
> [The New Cathedral and Bazaar]({{< relref "20200219_the_new_cathedral_and_bazaar" >}})*
