---
title: "The European Union must keep funding free software"
date: 2024-07-15T00:30:00+00:00
tags: ['Interpeer', 'NGI', 'Next Generation Internet', 'Horizon', 'Horizon Europe', 'Europe', 'European Union', 'European Commission', 'crowdstrike' ]
author: Jens Finkhaeuser
categories: ['miscellaneous']
---

Open letter initially published in French by the {{< external
  text="petites singularités"
  url="https://ps.zoethical.org/pub/lettre-publique-aux-ncp-au-sujet-de-ngi/"
  archive="force" >}}
association, translation by {{< external text="OW2"
  url="https://www.ow2.org/view/Events/The_European_Union_must_keep_funding_free_software_open_letter"
  archive="force"
>}} -- with French original version below.

To sign it: please publish it on your web site (French, English or both), then
add yourself in {{< external text="this table"
  url="https://pad.public.cat/lettre-NCP-NGI" archive=false >}}.

## Open Letter to the European Commission

Since 2020, {{< external text="Next Generation Internet (NGI)"
  url="https://www.ngi.eu/" archive=false >}} programmes, part of European
Commission's Horizon programme, fund free software in Europe using a cascade
funding mechanism (see for example {{< external text="NLNet's calls"
  url="https://nlnet.nl/commonsfund" archive=false >}}). This year, according to the
Horizon Europe working draft detailing funding programmes for 2025, we notice
that Next Generation Internet is not mentioned any more as part of Cluster 4.

NGI programmes have shown their strength and importance to support the European
software infrastructure, as a generic funding instrument to fund digital
commons and ensure their long-term sustainability. We find this transformation
incomprehensible, moreover when NGI has proven efficient and economical to
support free software as a whole, from the smallest to the most established
initiatives. This ecosystem diversity backs the strength of European
technological innovation, and maintaining the NGI initiative to provide
structural support to software projects at the heart of worldwide innovation is
key to enforce the sovereignty of a European infrastructure.  Contrary to
common perception, technical innovations often originate from European rather
than North American programming communities, and are mostly initiated by
small-scaled organizations.

Previous Cluster 4 allocated 27 millions euros to:

- "Human centric Internet aligned with values and principles commonly shared
  in Europe";
- "A flourishing Internet, based on common building blocks
  created within NGI, that enables better control of our digital life";
- "A structured eco-system of talented contributors driving the creation of new
  Internet commons and the evolution of existing Internet commons".

In the name of these challenges, more than 500 projects received NGI funding in
the first 5 years, backed by 18 organisations managing these European funding
consortia.

NGI contributes to a vast ecosystem, as most of its budget is allocated to fund
third parties by the means of open calls, to structure commons that cover the
whole Internet scope - from hardware to application, operating systems, digital
identities or data traffic supervision. This third-party funding is not renewed
in the current program, leaving many projects short on resources for research
and innovation in Europe.

Moreover, NGI allows exchanges and collaborations across all the Euro zone
countries as well as "widening countries"[^1], currently both a success and an
ongoing progress, likewise the Erasmus programme before us. NGI also
contributes to opening and supporting longer relationships than strict project
funding does. It encourages to implement projects funded as pilots, backing
collaboration, identification and reuse of common elements across projects,
interoperability in identification systems and beyond, and setting up
development models that mix diverse scales and types of European funding
schemes.

While the USA, China or Russia deploy huge public and private resources to
develop software and infrastructure that massively capture private consumer
data, the EU can't afford this renunciation.  Free and open source software, as
supported by NGI since 2020, is by design the opposite of potential vectors for
foreign interference. It lets us keep our data local and favors a
community-wide economy and know-how, while allowing an international
collaboration.  This is all the more essential in the current geopolitical
context: the challenge of technological sovereignty is central, and free
software allows to address it while acting for peace and sovereignty in the
digital world as a whole.

## Lettre Ouverte à la Commission Européenne

Depuis 2020, les programmes {{< external text="Next Generation Internet (NGI)"
  url="https://www.ngi.eu/" archive=false >}}, sous-branche du
programme Horizon Europe de la Commission Européenne financent en cascade (via
{{< external text="les appels de NLnet"
  url="https://nlnet.nl/commonsfund" archive=false >}}) le logiciel libre en Europe. Cette année,
à la lecture du
brouillon du Programme de Travail de Horizon Europe détaillant les programmes
de financement de la commission européenne pour 2025, nous nous apercevons que
les programmes Next Generation Internet ne sont plus mentionnés dans le Cluster
4.

Les programmes NGI ont démontré leur force et leur importance dans le soutien à
l’infrastructure logicielle européenne, formant un instrument générique de
financement des communs numériques qui doivent être rendus accessibles dans la
durée. Nous sommes dans l’incompréhension face à cette transformation, d’autant
plus que le fonctionnement de NGI est efficace et économique puisqu’il soutient
l’ensemble des projets de logiciel libre des plus petites initiatives aux mieux
assises. La diversité de cet écosystème fait la grande force de l’innovation
technologique européenne et le maintien de l’initiative NGI pour former un
soutien structurel à ces projets logiciels, qui sont au cœur de l’innovation
mondiale, permet de garantir la souveraineté d’une infrastructure européenne.
Contrairement à la perception courante, les innovations techniques sont issues
des communautés de programmeurs européens plutôt que nord-américains, et le
plus souvent issues de structures de taille réduite.

Le Cluster 4 allouait 27 millions d’euros au service de :

- «Human centric Internet aligned with values and principles commonly shared
    in Europe»;
- «A flourishing Internet, based on common building blocks
    created within NGI, that enables better control of our digital life»;
- «A structured eco-system of talented contributors driving the creation of new
    Internet commons and the evolution of existing Internet common«.

Au nom de ces enjeux, ce sont plus de 500 projets qui ont reçu un financement
NGI0 dans les 5 premières années d’exercice, ainsi que plus de 18 organisations
collaborant à faire vivre ces consortia européens.

NGI contribue à un vaste écosystème puisque la plupart du budget est dévolue au
financement de tierces parties par le biais des appels ouverts (open calls).
Ils structurent des communs qui recouvrent l’ensemble de l’Internet, du
matériel aux applications d’intégration verticale en passant par la
virtualisation, les protocoles, les systèmes d’exploitation, les identités
électroniques ou la supervision du trafic de données. Ce financement des
tierces parties n’est pas renouvelé dans le programme actuel, ce qui laissera
de nombreux projets sans ressources adéquates pour la recherche et l’innovation
en Europe.

Par ailleurs, NGI permet des échanges et des collaborations à travers tous les
pays de la zone euro et aussi avec ceux des widening countries[^2], ce qui est
actuellement une réussite tout autant qu’un progrès en cours, comme le fut le
programme Erasmus avant nous. NGI0 est aussi une initiative qui participe à
l’ouverture et à l’entretien de relation sur un temps plus long que les
financements de projets. NGI encourage également à l’implémentation des projets
financés par le biais de pilotes, et soutient la collaboration au sein des
initiatives, ainsi que l’identification et la réutilisation d’éléments communs
au travers des projets, l’interopérabilité notamment des systèmes
d’identification, et la mise en place de modèles de développement intégrant les
autres sources de financements aux différentes échelles en Europe.

Alors que les États-Unis d’Amérique, la Chine ou la Russie déploient des moyens
publics et privés colossaux pour développer des logiciels et infrastructures
captant massivement les données des consommateurs, l’Union Européenne ne peut
pas se permettre ce renoncement. Les logiciels libres et open source tels que
soutenus par les projets NGI depuis 2020 sont, par construction, à l’opposée
des potentiels vecteurs d’ingérence étrangère. Ils permettent de conserver
localement les données et de favoriser une économie et des savoirs-faire à
l’échelle communautaire, tout en permettant à la fois une collaboration
internationale. Ceci est d’autant plus indispensable dans le contexte
géopolitique que nous connaissons actuellement. L’enjeu de la souveraineté
technologique y est prépondérant et le logiciel libre permet d’y répondre sans
renier la nécessité d’œuvrer pour la paix et la citoyenneté dans l’ensemble du
monde numérique.

Dans ces perspectives, nous vous demandons urgemment de réclamer la
préservation du programme NGI dans le programme de financement 2025.

[^2]: Tels que définis par Horizon Europe, les États Membres élargis sont la
Bulgarie, la Croatie, Chypre, la République Tchèque, l’Estonie, la Grèce, la
Hongrie, la Lettonie, la Lituanie, Malte, la Pologne, le Portugal, la Roumanie,
la Slovaquie et la Slovénie. Les pays associés élargies (sous conditions d’un
accord d’association) l’Albanie, l’Arménie, la Bosnie Herzégovine, les Iles
Féroé, la Géorgie, le Kosovo, la Moldavie, le Monténégro, le Maroc, la
Macédoine du Nord, la Serbie, la Tunisie, la Turquie et l’Ukraine. Les régions
élargies d’outre-mer sont : la Guadeloupe, la Guyane Française, la Martinique,
La Réunion, Mayotte, Saint-Martin, Les Açores, Madère, les Iles Canaries.  In
this perpective, we urge you to claim for preserving the NGI programme as part
of the 2025 funding programme.

[^1]: As defined by Horizon Europe, widening Member States are Bulgaria, Croatia,
Cyprus, the Czech Republic, Estonia, Greece, Hungary, Latvia, Lituania, Malta,
Poland, Portugal, Romania, Slovakia and Slovenia. Widening associated countries
(under condition of an association agreement) include Albania, Armenia, Bosnia,
Feroe Islands, Georgia, Kosovo, Moldavia, Montenegro, Morocco, North Macedonia,
Serbia, Tunisia, Turkey and Ukraine. Widening overseas regions are :
Guadeloupe, French Guyana, Martinique, Reunion Island, Mayotte, Saint-Martin,
The Azores, Madeira, the Canary Islands.

## Addendum 2024-07-20: The CrowdStrike Disaster

> This is not part of the open letter, but a timely addendum made solely by
> the Interpeer Project.

Yesterday, an unchecked null pointer in a security software by company CrowdStrike
broke 8.5 million PCs in a single day.

Never mind that pundits blame programmers, or the choice of programming language
for this outage. That deserves a post of its own, as it is always a management
failure when these things get through to production. In this case, management
not only fired QA staff, but this is also CEO George Kurtz's second time being
responsible for a global outage. In this context, none of that matters very much.

What does matter is that the huge impact of this series of bad management
decisions can be traced back to one thing: *monoculture*.

We can now discuss whether this is the Microsoft monoculture, or the monoculture
of security products in use. Again, to do so would be to distract from the current
point.

The point is, that the only way to make the world resilient against such outages
is diversity in software. Diversity in software is not something you get by
funding the top players. Instead, look towards the grassroots.

If the goal is to keep the world resilient, both against software company
mismanagement or actual cyberattacks, funding software diversity is key. And
there is no more direct line to funding software diversity than to keep funding
FLOSS.

It's a very timely incident, so short after the publication of the above open
letter, that thoroughly underscores how *absolutely vital* FLOSS funding is for
critical digital infrastructure, diversity in software, and resilience in the
digital realm.

