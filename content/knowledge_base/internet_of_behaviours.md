---
title: "Internet of Behaviours"
aliases:
- iob
tags: ['internet of behaviours', 'iob']
faq:
  - question: What is the Internet of Behaviours?
    answer: >-
      The aim of the Internet of Behaviours (IoB) is to address how data
      collected can be interpreted from a human psychological and
      sociological perspective and how to use this understanding to influence
      or change human behaviour for various purposes, ranging from commercial
      interests to public policies
---

In 2012, Professor Gote Nyman coined the term
{{< external text="Internet of Behaviours"
  url="https://gotepoem.wordpress.com/2012/03/16/internet-of-behaviors-ib/"
  archive="force" >}} (IoB) to
describe a network in which behavioural patterns would have an IoB address in the
same way that each device has an IP address in the Internet of Things (IoT).

However, the term IoB is most often used to describe an extension of the Internet
of Things (IoT). A network of interconnected physical and digital objects that
collect and exchange information over the Internet, linking this data to
specific human measured or inferred behaviours.

The aim of IoB is to address how data collected can be interpreted from a human
psychological and sociological perspective and how to use this understanding to
influence or change human behaviour for various purposes, ranging from
commercial interests to public policies

*Source:* {{< external
  text="TechSonar Report 2023-2024"
  url="https://edps.europa.eu/data-protection/our-work/publications/reports/2023-12-04-techsonar-report-2023-2024_en"
  archive="force" >}}
by the European Data Protection Supervisor.

*See also:* [surveillance capitalism]({{< relref "surveillance_capitalism" >}}).
