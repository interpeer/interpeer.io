---
title: "Delay-Tolerant Networking"
tags: ['delay-tolerant networking', 'dtn', 'disruption-tolerant networking', 'ietf', 'ccsds']
aliases:
- dtn
- disruption-tolerant networking
faq:
  - question: What is a "Delay-Tolerant Networking"?
    answer: >-
      Delay-tolerent networking refers to techniques for networking systems
      where delays and intermittency are orders of magnitutes larger than on
      the Internet, such as in e.g. deep space communications.
      In the Internet Engineering Task Force, there exists a DTN working group
      that standardizes the Bundle Protocol, which is an implementation of the
      DTN architecture.

---

Delay-tolerent networking refers to techniques for networking systems
where delays and intermittency are orders of magnitutes larger than on
the Internet, such as in e.g. deep space communications.

In the Internet Engineering Task Force, there exists a [DTN working group](https://datatracker.ietf.org/wg/dtn/about/)
that standardizes the Bundle Protocol, which is an implementation of the
DTN architecture.

In doing so, it co-operates with the [Consultative Committee for Space Data Systems](https://ccsds.org)
(CCSDS), which is a multi-national forum for the development of communications & data systems standards
for spaceflight.

But disruption can also occur in earth-bound systems. The Internet Society
Foundation estimates that only about half the world population is connected
to the Internet. And when they are connected, that does not imply that this
connection is permanent or with low latency connections.

In remote communities, power or networking equipment may not be available
24/7, which means their needs share similarities to those of deep space
communications.

However, it is our view that a [human-centric networking]({{< relref "human_centric_internet" >}})
approach should be better suited as the more general networking architecture.
