---
title: "Data Sovereignty"
tags: ['data sovereignty', 'data independence']
faq:
  - question: What is Data Sovereignty?
    answer: >-
      Data Sovereignty means having full and unrestricted control over the data
      one owns, and implies control over personally identifiable information (PII).
---

Data Sovereignty is a part of [Digital Sovereignty]({{< relref "digital_sovereignty" >}}),
and means having full and unrestricted control over the data
one owns, and implies control over [personally identifiable information (PII)]({{< relref "personally_identifiable_information" >}}).

## Definition

Definitions are often restricted to merely being able to access and move data at
will.

However, there are distinct concerns that also affect this ability, which bear
keeping in mind:

1. **Data Transparency** refers to the transparency that is provided in storing
   personal data and for fine-grained access control when sharing it.
2. **Data Compatibility & Interoperability** considers that data may need to be
   provided in standardized formats when switching processing from one service
   provider to another, and may need to be transformed in order to make this
   switch.
3. **Data Security & Privacy** concerns both data stored at rest, as well as when
   transmitted. In either case, it must be ensured that only the parties with
   access are privy to the information.
4. **Service Portability** refers to the generalized concept of being able to
   switch service providers for data processing.

## Interpeer Project

It can be argued that if any of the above is not ensured, there is no full Data
Sovereignty in place, either. If access is only granted in principle, but not in
practice, there is no access after all.

As a result, we treat Data Sovereignty as the umbrella term that encompasses all
of the above aspects.


