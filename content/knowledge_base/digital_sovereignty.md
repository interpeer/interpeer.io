---
title: "Digital Sovereignty"
tags: ['digital sovereignty', 'digital independence']
faq:
  - question: What is Digital Sovereignty?
    answer: >-
      Digital sovereignty can mean different things based on context,
      but what combines all is independence from a single point of
      failure in digital architectures.
---

A comprehensive definition inspired by the {{< external
  text="Digitale Souveränität als Strategische Autonomie"
  url="https://www.oeffentliche-it.de/documents/10181/14412/Digitale+Souver%C3%A4nit%C3%A4t+als+strategische+Autonomie+-+Umgang+mit+Abh%C3%A4ngigkeiten+im+digitalen+Staat"
  archive="force" >}}
(German; "Digital Sovereignty as Strategic Autonomy") breaks this down as:

**Digital Sovereignty = [Data Sovereignty]({{< relref "data_sovereignty" >}}) + [Technical Sovereignty]({{< relref "technical_sovereignty" >}})**

This usage takes a holistic approach considering both the digital data we deal
with as well as the systems by which we access them, and how those systems can
be maintained and further developed.

## Usage in Politics

The term is used in the political context to mean that governments should
not depend on a single vendor for any of the services they use or offer to
their citizens. In particular, dependence on overseas vendors can be
problematic, as different legislation could mean that foreign governments
can mandate a vendor located in their legislation to grant access to
another government's data.

But beyond those concerns, there is a simple need for avoiding a single point
of failure. Governments would prefer to be able to procure from multiple
vendors in their own legislation to better meet the demands of digitalization.

## Interpeer Project

At first glance, here at the Interpeer Project, we are more concerned with
[Data Sovereignty]({{< relref "data_sovereignty" >}}) over its sibling. After
all the [Human-Centric Internet Architecture]({{< relref "human_centric_internet" >}})
we're developing is strongly focused on taking Data Sovereignty seriously.

That said, the various aspects of [Technical Sovereignty]({{< relref "technical_sovereignty" >}}) do matter
very much to us. The only difference is that we view them through the lense of
individual citizens' rather than government institutions' needs.

It is about humans, after all.
