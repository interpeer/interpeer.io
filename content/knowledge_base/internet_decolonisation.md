---
title: "Internet Decolonisation"
aliases:
- decolonisation-of-the-internet
- decolonisation-of-internet
tags: ['internet', 'decolonisation', 'decolonisation of the internet']
faq:
  - question: What does "decolonisation of the Internet" mean?
    answer: >-
      Decolonisation is a strategy that connects movements that question
      and challenge the power structure inherent in the fact that the majority
      of physical Internet infrastructure, as well as the content available on
      the Internet, is owned and produced by former colonial powers.
---

A large part of the Internet is effectively owned by former colonial powers.
This applies both to the physical infrastructure of the Internet, as well as
the content that is available for perusal.

The Robert Bosch Academy defines decolonisation as a strategy that connects
a lot of different movements that question these power structures and attempt
to challenge them.

It is our view that a [human-centric networking]({{< relref human_centric_internet >}})
architecture contributes to decolonisation by moving control over content away
from centralized machines owned by digital colonialists, towards the individuals
and communities that share such content.
