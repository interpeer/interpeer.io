---
title: "Deceptive Pattern"
aliases:
- dark-pattern
tags: ['deceptive pattern', 'dark pattern', 'pattern']
faq:
  - question: What is a "deceptive pattern"?
    answer: >-
      A deceptive pattern is a way of presenting choices to people that are
      manipulative, tricking them into making choices that are not in their
      best interest.
  - question: What is "deceptive design"?
    answer: >-
      Deception design uses deceptive design patterns such as trick wording,
      sneaking or obstruction to deceive people into making choices that are
      bad for them.
  - question: What is a "dark pattern"?
    answer: >-
      "Dark pattern" is an older term for a deceptive pattern.
---

A deceptive pattern is a way of presenting choices to people that are
manipulative, tricking them into making choices that are not in their
best interest.

The term is often applied in the field of user experience design, and cookie
banners are a perfect example of deceptive patterns.

Not only are cookie banners themselves often superfluous -- purely functional
cookies usually do not require them, only advertising/tracking cookies do.

But more often than not, the "accept all cookies" choice is presented larger,
or in green, giving it all the trappings of the obvious choice to make, when
users should likely prefer the choice that minimized the data being collected
about them.

Deceptive patterns and [informed consent]({{< relref "knowledge_base/informed_consent"
  >}}) have an interesting relationship, as deceptive patterns often do provide the
required information for requesting consent, while simultaneously manipulating
the user's choice.

Note that deceptive patterns used to be called "dark patterns", and you may
still find this term in use. It's better to speak about deceptive patterns
or {{<external text="deceptive design"
  url="https://www.deceptive.design/" archive=false >}}, though, because the association
between "dark" and "bad" reinforces racist stereotypes.
