---
title: "Personally Identifiable Information"
aliases:
- pii
tags: ['Personally Identifiable Information', 'pii']
faq:
  - question: What is "personally identifiable information (PII)"?
    answer: >-
      Personally identifiable information or PII for short refers to any
      information that can be used, either alone or together with other
      information, to identify a person.
---

Personally identifiable information or PII for short refers to any information
that can be used, either alone or together with other information, to identify
a person.

Some countries or states place particular protections on such data, such as
via the European Union's {{% extref gdpr %}} or the {{% extref ccpa %}}.

Unfortunately, not all legislations have such specific laws -- however, as the
right to privacy is one right codified in the {{% extref udhr %}}, which many nations
ratified (in the EU, also {{% extref echr %}} applies), in theory all those nations
should offer some protection for PII. Unfortunately, that theory is not necessarily
well reflected in reality.
