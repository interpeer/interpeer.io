---
title: "PrivacyWashing"
tags: ['privacywashing', 'privacy washing', 'washing']
faq:
  - question: What is "PrivacyWashing"?
    answer: >-
      PrivacyWashing refers to practices making something appear conscious of
      the data privacy rights of individuals, when in actual fact data privacy
      is not a big concern.
---

PrivacyWashing refers to practices making something appear conscious of
the data privacy rights of individuals, when in actual fact data privacy
is not a big concern.

For example, the law may permit collecting [personally identifiable information]({{<
    relref "knowledge_base/personally_identifiable_information" >}}) without asking for [informed consent]({{<
    relref "knowledge_base/informed_consent" >}}). When an entity then
advertises an easy method for reviewing and deleting gathered information,
without disclosing when such data is collected, that could count as
PrivacyWashing.

The first concern of privacy conscious operations should be to *minimize* data
collection -- not merely to offer ways of repairing damage already done.

## Related

The term is related to [#FOSSWashing]({{< relref "knowledge_base/foss_washing" >}})
and [#OpenWashing]({{< relref "knowledge_base/open_washing" >}}).
