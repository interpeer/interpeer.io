---
title: "Human-Centric Internet"
tags: ['human rights', 'human-centric internet', 'human-centric networking']
aliases:
- hci
- hcn
- human-centric-networking
faq:
  - question: What is a "Human-Centric Internet"?
    answer: >-
      The term "Human-Centric Internet" describes an Internet for humans.
      It highlights individual human concerns, in contrast to e.g. the
      Internet of Things (IoT), which is focused on connectivity for smart
      devices.
      The European Commission's Next Generation Internet (NGI) initiative
      aims for a human-centric Internet.
  - question: What is "Human-Centric Networking"?
    answer: >-
      Human-Centric Networking is an architectural approach to building a
      Human-Centric Internet.
---

The term "Human-Centric Internet" describes an Internet for humans.
It highlights individual human concerns, in contrast to e.g. the
Internet of Things (IoT), which is focused on connectivity for smart
devices.

The European Commission's {{% extref ngi %}} aims for a human-centric Internet.

## Context

Human concerns are plentiful, and since the current Internet is build by
humans, what precisely is it that makes a human-centric Internet different
from what we see today?

In a sense, the term is a reaction of recent developments in the web sphere,
a call back to this technology's roots. The web was built to disseminate
information, to connect people by sharing knowledge.

While it's clear that today's Internet is used for much more than sharing
knowledge, some of those use-cases are no longer concerned with connecting
people.

Taking the Internet of Things (IoT) as an example, we live in a world where
the sum of every person's concerns is represented by thousands of machines,
all connected to the same Internet. Whether it the home automation system that
closes our shades, or the smart sensor triggering a field to be irrigated,
these IoT devices do serve human needs.

However, the rise of [surveillance capitalism]({{< relref "surveillance_capitalism" >}})
shows like few other examples that not everything that happens on the Internet
has humanity's best interests at heart. For a truly human-centric Internet,
we need to refocus our efforts.

## Human-Centric

Human-centric concerns include those related to how data -- in particular,
[personally identifiable information]({{< relref "personally_identifiable_information" >}})
(PII) -- is handled, and therefore focus on:

- Data Transparency for a more transparent personal data storage and a more
  fine-grained data transfer when exercising personal data access rights;
- Data Compatibility & Interoperability to facilitate switches between service
  providers;
- Security & Privacy of consumers when their personal data are transferred from
  one provider to another.
- Service Portability to empower users to share their data with any service
  provider and host that they trust;
- [Data Sovereignty]({{< relref "data_sovereignty" >}}) to empower users to transfer a complete data set or parts of
  it to any new provider without giving reasons.

Additionally, human-centric concerns cannot zero in on these without concerns
for the wider ecosystem, so must also include considerations for a green
Internet, which in practice means keeping computer usage to the what
is necessary to support other human needs, by reducing:

- data storage;
- data transmission;
- duplicate processing;
- wasteful algorithms;
- etc.

Finally, a human-centric Internet is also trustworthy. Research shows that
transparency is a key factor in building trustworthy technology. This applies
not only to data storage and transmission, but additional concerns such as
energy use, etc.

## Human-Centric Networking Architecture

The architectural principles of Human-Centric Networking (HCN) share some
similarities to both
[information-centric networking]({{< relref "information_centric_networking" >}})
(ICN), and [delay-tolerant networking]({{< relref "delay_tolerant_networking" >}})
(DTN).

The notion that content should be addressable, which underlies ICN, is expanded
upon to no longer mean individual, static content blocks, but rather mutable,
shared resources. DTN influences the design by providing for explicit
custodianship of resources (though the meaning differs somewhat in DTN).

Shared resources are self-contained and know who they are shared with -- which
implies that humans sharing in the resource have some (ephemeral or static)
identifiers to represent them. Unlike ICN, DTN, or even the good, old-fashioned
Internet, this means that human identifiers are a fundamental component of this
architecture -- and as a result of this, also various authentication and
authorization concerns.

This is how human interactions can be secured; not by layering security as an
optional addition upon an architecture, but embedding it from the beginning.

The full {{% intref ip_arch "architecture specification" %}}
contains more detail.

## Related

The {{% extref techautonomy %}} expresses similar values to the
"human-centric" term. But where that declaration stands as a manifesto for
technology users, human-centric technology focuses more on the design
principles underpinning its creation.  They're two sides of the same coin.

Note also that some definitions of "human-centric" are just rebrandings of the
IoT definition we discuss above: rather than networking *for* humans, it describes
networking *about* humans, and focuses more on all the sensors we might carry
around these days that provide access to [PII]({{< relref "personally_identifiable_information" >}}).
This approach should not rightfully be called "human-centric".

Finally, the [peer-for-peer]({{< relref "peer_for_peer" >}}) (P4P) term
has been coined as a similar concept. Where P4P focuses on small
community networks, HCN acknowledges the Internet's global scope.
Much like the Internet is sometimes described as a "network of networks",
one can imagine the HCN term as analogous to the Internet in encompassing
many P4P networks.
