---
title: "Informed Consent"
tags: ['informed consent', 'consent']
faq:
  - question: What is "informed consent"?
    answer: >-
      Informed consent is given when someone consents to something after
      having been adequately informed of the consequences. Consent without
      sufficient information is not informed consent.
---

Informed consent is given when someone consents to something after
having been adequately informed of the consequences. Consent without
sufficient information is not informed consent.

The distinction matters when people enter into agreements they may
later regret. Sufficient up-front information both helps them avoid
such agreements in the first place -- but it also helps avoid liability
when sufficient information was provided.
