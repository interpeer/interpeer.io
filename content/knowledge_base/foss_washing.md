---
title: "FOSSWashing"
tags: ['FOSSwashing', 'FOSS washing', 'washing']
faq:
  - question: What is "FOSSWashing"?
    answer: >-
      FOSSWashing refers to practices that follow the letter of free
      and open source software (FOSS), but not the spirit.
aliases:
- foss-washing
---

FOSSWashing refers to practices that follow the letter of free and
open source software (FOSS) development, but not the spirit.

## Related

The term [#OpenWashing]({{< relref "knowledge_base/open_washing" >}}) is related,
but unnecessarily restricted to just "open source", ignoring free and libre
software.

Both share their origins in the {{< external text="greenwashing"
  url="https://en.wikipedia.org/wiki/Greenwashing" archive=false >}} term.
