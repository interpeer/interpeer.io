---
title: "Technical Sovereignty"
tags: ['technical sovereignty', 'technical independence']
faq:
  - question: What is Technical Sovereignty?
    answer: >-
      Technical Sovereignty refers to having access to knowledge and operational
      artifacts of digital technology, such that it can be freely used and adapted.

---

Technical Sovereignty refers to having access to knowledge and operational
artifacts of digital technology, such that it can be freely used and adapted.

In a sense, it is a rewording of the Four Freedoms of the
{{< external text="Free Software Definition" url="https://en.wikipedia.org/wiki/The_Free_Software_Definition" archive=false >}}.
Though the rewording does not imply using OSI-approved licenses in published
software, this is a reasonable interpretation.

Technical Sovereignty is one part of [Digital Sovereignty]({{< relref "digital_sovereignty" >}}).

## Definition

A possible definition of Technical Sovereignty could be:

1. **Knowledge Sovereignty** refers to access to information and knowledge about
   digital systems: their application, use, operation and components. It may also
   include access to experts for disseminating this knowledge.

   We support Knowledge Sovereignty directly through the dissemination of our
   research results as articles, talks, papers as well as appropriately licensed
   software.

2. **Research Sovereignty** refers to the freedom in choosing research topics, as
   well as the required access to e.g. algorithms and compute resources.

   Research Sovereignty also encompasses the capability to transfer well-founded
   research into international standards.

   We promote Research Sovereignty by proposing such standards early, in addition
   to the more general Knowledge Sovereignty topics.

3. **Development Sovereignty** means that people have freedom in decision making
   during the design and development of products. This may include the ability to
   adapt software to specific needs.

   We try to support this not only by publishing research results and software
   with appropriate licenses, but also supplementing this with documentation.

4. **Production Sovereignty** means having access to skills and resources for
   manufacturing and quality assurance.

   In the sense that we use standard tooling to build software which is freely
   usable by others, we do support Production Sovereignty.

5. **Operational Sovereignty** then refers to the ability to operate software
   as needed.

   When we produce software to be run by end-users or operators, we strive to
   provide it in a way that allows them to operate it safely and without
   artificial constraints.

6. **Utilization Sovereignty** is similar to Operational Sovereignty, but more
   focused on end-user needs.

7. **Transparency Sovereignty** means that users should be able to justify the
   results of software, and to invervene where desired.

## Interpeer Project

The above aspects of a definition of Technical Sovereignty might be ranked
as progressing from the least to the most end-user focused. As a research
organization, our focus lies more strongly on the earlier phases of the
definition.

But this is the current state. As more of the work reaches maturity, we fully
intend to shift our focus more towards operators and end-users.
