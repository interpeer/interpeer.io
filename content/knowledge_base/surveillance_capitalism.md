---
title: "Surveillance Capitalism"
tags: ['surveillance capitalism', 'surveillance', 'capitalism']
faq:
  - question: What is surveillance capitalism?
    answer: >-
      Surveillance capitalism refers to the practice of monetizing data collected
      about the users of a service.
---

Surveillance capitalism refers to the practice of monetizing data collected
about the users of a service. Doing so increases the incentives for collecting
more user data, which in turn violates user privacy further.

The practice may be legal, if the user consents to the collection of data. Some
businesses offer free services in exchange for the use of user data. Unfortunately,
it is not always made sufficiently clear how the data is used, and whether it
involves the collection of [personally identifiable information]({{< relref
    "knowledge_base/personally_identifiable_information" >}}).
