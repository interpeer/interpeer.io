---
title: "OpenWashing"
tags: ['openwashing', 'open washing', 'washing']
faq:
  - question: What is "OpenWashing"?
    answer: >-
      OpenWashing refers to practices that follow the letter of open source,
      but not the spirit, with the intent of falsely making something appear
      as open when it follows a closed ideology.
---

OpenWashing refers to practices that follow the letter of open source,
but not the spirit, with the intent of falsely making something appear
as open when it follows a closed ideology.

## Open Source

"Open" has many different meanings, but colloquially it is understood to have
permissionless access to either participation in or results of a process. In
practice, it may not even mean that.

In the context of {{% extref osd "Open Source" %}} it is used to describe a permissionless
model of collaboration in software development, with results made available to
the general public and with few constraints.

## Related

The term [#FOSSWashing]({{< relref "knowledge_base/foss_washing" >}}) is related.
Both share their origins in the {{< external text="greenwashing"
  url="https://en.wikipedia.org/wiki/Greenwashing" archive=false >}} term.
