---
title: "Peer-for-Peer"
tags: ['peer-for-peer', 'peer-to-peer', 'p4p', 'p2p']
aliases:
- p4p
faq:
  - question: What is a "Peer-for-Peer" (networking)?
    answer: >-
      The term "peer-for-peer" stems from Zenna Elfen's master thesis, and
      describes networking protocols that are characterized by small, modular
      and community-driven infrastructures.
---

The term "peer-for-peer" stems from {{% extref "p4p" "Zenna Elfen's master thesis" %}}
and describes networking protocols that are characterized by small, modular
and community-driven infrastructures.

There is a strong overlap beween this term and the [human-centric networking]({{< relref "human_centric_internet" >}})
(HCN) concept. The main difference is P4P's focus on smaller networks, while HCN's
scope is global.
