---
title: "Information-Centric Networking"
aliases:
- icn
tags: ['icn', 'information-centric networking', 'peer-to-peer', 'p2p']
faq:
  - question: What is "information-centric networking"?
    answer: >-
      Information-centric networking or ICN changes how we find things
      on the Internet. Instead of asking a specific website, we simply
      ask for the page by its identifier -- similar to how a search engine
      works.
image: information_centric_networking.svg
---

Information-centric networking is an evolution of [peer-to-peer (p2p)]({{< relref "peer_to_peer" >}})
concepts. To illustrate this, let's quickly review how the world wide web
works.

## URLs

You all know URLs, the addresses you may enter into or see in a browser bar. The
term URL stands for "Universal Resource Locator" -- the key part being the last
word. A URL tells the browser *where* something is located.

There is a related concept of URIs (Universal Resource Identifiers). To make
matters more complicated, all URLs are also URIs, but not all URIs are URLs.
The difference is that a URI *identifies* a resource, while a URL *locates*
it -- and therefore also works as an identifier of sorts.

If a URI is used that is not also a URL, then there needs to be a system that
*resolves* the URI to a URL before a connection can be established.

## IP Addresses

The host name in URLs gets translated into an IP address via the Domain
Name System (DNS). IP addresses are what allows computers to connect to
each other.

A less known fact is that there are IP address ranges set aside for use
as *identifiers*. That is, while most IP addresses act as *locators*, not
all do.

When using IP addresses that are *identifiers*, these, too, need to be
*resolved* into different IP addresses that are *locators* before computers can
talk to each other.

## Information-Centric Networking

In information-centric networking, the basic idea is that URIs -- or similar
content identifiers -- should be used to connect to computers, not URLs.

This is because we don't typically care *where* a resource is, just that we
find the right one. That is also why hardly anyone enters URLs into the
browser bar any longer -- most of us use it to ask a search engine to *resolve*
a search query to a list of candidate URLs.

In {{% doi "10.1016/j.comnet.2014.04.002"
  "Information-centric networking: The evolution from circuits to packets to content"
%}}, the author argue
that ICN presents an evolution from packet switching networks.

{{< fig src="packet_switching.svg"
  width="100%"
  caption="A) Packet Switching"
  >}}

Whereas in packet switching networks, routers switch opaque data packets based
on source and destination address, in ICN they take on the role of custodians
and caches for data packets.

{{< fig src="information_centric_networking.svg"
  width="100%"
  caption="B) Information-Centric Networking"
  >}}

Custodians are responsible for permanently storing data (or until deleted).
Caches on the other hand keep data packets that traverse them. Nodes can be
either custodian or cache, both, or in principle, neither.

### Redundancy & Failover

A major benefit of removing *locators* from finding resources is that it
reflects better how we use the Internet. If we do not care much about *where*
a resource exists, then it is also possible for the resource to exist in many
places at once -- so long as it is identical at each location. This provides
redundancy & failover in case a single source vanishes.

### Performance

Already on the existing web, we use redundancy techniques to also deliver web
pages faster. A content-delivery network (CDN) is doing just that. But because
the web is not built for this from the ground up, a lot of additions to web
technology were developed to ensure that each location has identical content
for a resource.

Unfortunately, however, this is still not guaranteed. It is possible, for example,
for a CDN to include advertising or tracking into a page.

ICN approaches do not permit such modifications, but leverage the same ability
to place resources into many different locations for optimizing performance.

### Security & Privacy

The web was not built around *identifiers* from the ground up. The difference
between *identifiers* and *locators* was a later addition.

The result is that it remains difficult to ensure a resource is identical in
multiple locations, as mentioned above. But the focus on *locators* also means
that the web is primarily concerned with securing the connection between
computers.

While ICN does not automatically provide better security, many ICN approaches
recognize that when the computer location is less relevant, securing the
resource itself becomes more important.

In practice, this means that when you request a specific resource by *identifier*,
there will be cryptographic means available to verify that the received
resource is unmodified. Additionally, the resource may be encrypted such that
the computers at which it's currently located may not be able to decipher it.

## Interpeer Project

The Interpeer Project approaches ICN from the ground up as a challenge to achieve
the highest levels of performance, as well as security & privacy, in order to
build a new Internet fit for future generations.
