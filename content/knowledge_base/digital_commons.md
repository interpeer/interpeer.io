---
title: "Digital Commons"
tags: ['digital commonds', 'commons']
faq:
  - question: What is the Digital Commons?
    answer: >-
      The Digital Commons encompasses the entirety of digital artifacts,
      from code to training data for large language models, that is
      available to the public. Much like land-based commons, it's value
      is intrinsically linked to being an equitably accessible resource.
---

The Digital Commons encompasses the entirety of digital artifacts,
from code to training data for large language models, that is
available to the public. Much like land-based {{< external text="commons"
  url="https://en.wikipedia.org/wiki/Commons" archive=false >}}, it's value
is intrinsically linked to being an equitably accessible resource.

> The commons is the cultural and natural resources accessible to all members
> of a society, including natural materials such as air, water, and a habitable
> Earth. These resources are held in common even when owned privately or publicly.
> Commons can also be understood as natural resources that groups of people
> (communities, user groups) manage for individual and collective benefit.
> Characteristically, this involves a variety of informal norms and values (social
> practice) employed for a governance mechanism. Commons can also be defined as a
> social practice of governing a resource not by state or market but by a community
> of users that self-governs the resource through institutions that it creates.
>
> -- Wikipedia, 2025-01-19

One example of a commons is {{% extref "osd" "Free/Libre and Open Source Software" %}},
but there are also overlaps with [digital sovereignty]({{< relref "digital_sovereignty" >}})
concerns: a healthy digital commons enables sovereignty by removing dependencies
on specific industry actors.

Similarly, open standards are necessary for enabling interoperability between
software packages, so that such dependence is further reduced.

Much like its historic land-based predecessor, the digital commons is vulnerable
to {{% doi "10.1016/j.worlddev.2016.11.005" "land-grabbing" %}}, and enclosure,
and a healthy commons must be protected against such practices.

The digital commons can also be understood to encompass digital communications
infrastructure. At a time where social networks are a major channel for political
discourse, it is necessary that remain a useful resource, and do not get {{< external
  text="flooded by mis- and disinformation"
  url="https://www.cbc.ca/news/canada/new-brunswick/misinformation-canada-meta-fact-checking-1.7434214"
  archive=false >}}. The digital commons may thus be understood to include any
social media platform that is independent of such influence, and provided, moderated
and fact-checkde by the citizenry itself.
