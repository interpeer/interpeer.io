---
title: "Peer-to-Peer"
aliases:
- p2p
tags: ['peer-to-peer', 'p2p']
faq:
  - question: What is "peer-to-peer (p2p)"?
    answer: >-
      Peer-to-peer refers to a mode of connecting computers so that every
      participating node is equal in rights and responsiblities, at least
      potentially. Unlike the client-server model, peers in p2p can choose
      whether to initiate an action (acting as a client) or respond
      (acting as server) at any time.
---

Peer-to-peer refers to a mode of connecting computers so that every
participating node is equal in rights and responsibilities, at least
potentially. Unlike the client-server model, peers in p2p can choose
whether to initiate an action (acting as a client) or respond
(acting as server) at any time.

## Client-Server

In the client-server model of networking, the protocol design determines
which machine initiates a request or action, and thus acts as the client.
By contrast, a server machine idles until it needs to fulfil a client request.

The implication is that servers need to be reachable. They have an IP address
that can be connected to. Firewall rules can prevent incoming requests; such
rules are not suitable for servers.

At the same time, firewall rules often permit clients to make outgoing
requests.

## P2P

In peer-to-peer models, the roles of client and server are more fluid. Or, to
put it differently, each node is both client *and* server, able to issue and
respond to requests.

Because the service architecture is thus not as strictly predefined, it usually
follows that nodes employ their own algorithms for judging how to act at any
given time. Additionally, administrative policies may influence whether a node
acts more like a client or more like a server.

An implication of this fluidity in roles is that p2p nodes need to be reachable.
This is either accomplished by running nodes on reachable IP addresses -- or by
using other means to establish connections, such as NAT piercing.

### NAT Piercing

Network Address Translation or NAT for short is a method by which gateways
provide a single address to one network, but connect to a wholly different
network on the other side. To permit traffic between them, addresses need to
be translated.

There are various kinds of NAT, but they tend to employ mapping each IP address
of the private network to a new port on the single address the public network
knows. Because the mapping method is unknown, NAT makes it difficult for outside
computers to connect to machines inside the private network, preventing p2p
operations.

NAT piercing refers to a variety of techniques used to establish a connection
in this direction anyway. Protocols such as {{< rfc text="STUN"
  num="5389" >}} help facilitate this.

## Further Reading

The peer-to-peer term has become relatively ambiguous over time. Because
the term is useful at different levels of abstraction, and also in a human
rather than computing context, it is not guaranteed that two people using
it mean the same thing. In ["peering through the fog"]({{< relref "blog/20220228_peering_through_the_fog" >}}),
you can read more about the different meanings of the term.
