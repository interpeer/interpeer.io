---
title: channeler
repo: https://codeberg.org/interpeer/channeler
spdx: GPL-3.0-only
tags: ['multi-channel', 'protocol', 'tunnelling', 'tunneling', 'encryption', 'transport', 'tcp', 'udp', 'quic', 'channeler']
impl:
  lang: C++
  deps:
  - liberate
  - spin_bit
  - s3kr1t
  - packeteer
image: channeler.png
funding:
  ngi: NGI Zero Discovery
  cordis: 825322
description: >-
  Channeler is a multi-channel protocol with per-channel capabilities; a parallel
  but superficially similar development to QUIC. Unlike QUIC, channeler can provide
  stream, datagram and novel modes of operation, selectable per channel.
---

## Introduction

Channeler is a multi-channel protocol with per-channel capabilities. It's
something of a parallel development to {{% extref quic "QUIC" %}}, but with distinct
features.

The design for channeler was originally inspired by experience with video
streaming over peer-to-peer networks in 2006-2009. In order to safely traverse
NATs, a {{% extref udp "UDP" %}}-based
implementation is required. Using that, however, means losing reliability
criteria of {{% extref tcp "TCP" %}}.

From an application perspective, however, it's not always clear which is
preferred. More precisely, it's usually the case that for some messages,
reliability is a good idea -- while for others, it adds unnecessary overhead.

Unlike *QUIC*, which replicates TCP's reliability over UDP, in *Channeler*
one can choose the reliability characteristics of every channel individually.
Furthermore, there are a lot more choices available than TCP's ordered mode
with resend or UDP's fire-and-forget mechanisms.

Because NAT traversal is only a concern once for the UDP source and destination
tuple, any other channels can still traverse the NAT, if reliability is enabled
or not.

Furthermore, transport encryption is part of Channeler's design. To this end,
channeler employs a simple scheme[^encryption] based on the {{% extref noise %}},
which explicitly circumvents complexities present in {{% extref dtls "DTLS" %}}.

[^encryption]: The encryption sub-protocol is a work in progress.

Work on Channeler was partially funded with a grant from {{% extref
  "ngi0_discovery" %}}.

## Dependencies

All of channeler makes use of [liberate]({{< relref liberate >}}). Measuring
round-trip time in order to make application-level decisions on congestion
uses the [spin_bit]({{< relref spin_bit >}}) library. Transport encryption
makes use of primitives from the [s3kr1t]({{< relref s3kr1t >}}) library.

The implementation does not make use of sockets or similar mechanisms directly,
and thus permits use of a user-defined send and receive mechanism. However, an
implementation based on [packeteer]({{< relref packeteer >}}) is self-evident.

