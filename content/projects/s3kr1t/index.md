---
title: s3kr1t
repo: https://codeberg.org/interpeer/s3kr1t
spdx: GPL-3.0-only
impl:
  lang: C++
tags: ['cryptography', 'wrapper', 'library', 'encryption', 'hash', 'signature', 'public key', 'key pair']
funding:
  isoc: research
description: >-
  Similar to liberate, s3kr1t is primarily an abstraction
  library. However, rather than abstracting out platform differences, the goal
  is to provide abstraction from cryptographic libraries of the underlying
  operating system.
---

## Introduction

Similar to [liberate]({{< relref liberate >}}), s3kr1t is primarily an abstraction
library. However, rather than abstracting out platform differences, the goal
is to provide abstraction from cryptographic libraries of the underlying
operating system.

The library is best understood as a very narrow slice of an API for the specific
cryptographic functions the other Interpeer sub-projects need.

Other than providing a stable API independent of cryptographic library used,
this primarily simplifies the maintenance of builds. Cross-platform building is
hard enough, so encapsulating some choices in a sub-project eases the burden on
other libraries.

Currently, s3kr1t provides an API over {{% extref openssl %}} in the 1.x or 3.x
branches. Naturally, it should similarly be useable with {{% extref libressl %}},
though that is untested. For some cryptographic primitives, {{% extref libsodium %}}
may be used.

Note that e.g. on Android, the OpenSSL version that is part of the {{< external
  text="Android NDK" url="https://developer.android.com/ndk/" archive=false >}} is used,
which leverages any hardware acceleration provided by the device and its
drivers. In a similar fashion, *OpenSSL* engines can be transparently used to
accelerate operations.
