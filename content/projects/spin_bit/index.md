---
title: spin_bit
repo: https://codeberg.org/interpeer/spin_bit
spdx: GPL-3.0-only
impl:
  lang: C++
tags: ['spin_bit', 'quic', 'rtt', 'round-trip time', 'measurement']
description: >-
  This library is a tiny C++ header implementation of spin bit based RTT
  measurements in network traffic.
---

## Introduction

This library is a tiny C++ header implementation of spin bit based RTT
measurements in network traffic.

The design is inspired by {{% rfc text="QUIC"
  num="9000" %}}, and based on
{{< external
  text="Piet De Vaere's master thesis"
  archive="force"
  url="https://pub.tik.ee.ethz.ch/students/2017-HS/MA-2017-16.pdf" >}}.

## Overview

The spin bit is a single bit that the client in a network connection sends, and
the server reflects back. When the client receives a bit with the same value it
has sent, it flips the bit for the next packet it sends. In this way, an
observer in the middle can infer a round-trip and time it accordingly.

However, the spin bit mechanism is not robust in the face of packet re-ordering
or outright loss. To mitigate this, the above master thesis introduces an
additional two bits of Vector Edge Counter (VEC), the value of which permits
determines whether an edge of a spin bit signal is valid.

This library implements the spin bit + VEC algorithm on the sender and receiver
side, and provides a "generator" that additionally produces matching packet
numbers. It also provides a basic observer, and a VEC-based observer for
analysing round-trip times.

The *spin_bit* library is built for use in [channeler]({{< relref channeler >}}).
