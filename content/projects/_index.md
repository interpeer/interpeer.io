---
title: Projects Overview
tags: ['human-centric networking', 'icn', 'information-centric networking', 'hcn', 'peer-to-peer', 'p2p']
index: 0
image: architecture.png
# FIXME necessary for correct menu rendering
slug: /
---

## Introduction

The Interpeer Project aims to provide a full human-centric networking
that supports all web use cases, and considers human rights preservation a first
class objective.

Because that is a big undertaking, a major design decision up front was to break
as much of this stack up into individual packages with minimal dependencies on
each other. This keeps every package easier to maintain and test, development
milestones clearer, etc. -- but it also permits for more experimentation and
re-use.

The exception to this are foundational libraries such as [liberate]({{< relref liberate >}})
and [s3kr1t]({{< relref s3kr1t >}}), which are relatively thinly scoped
platform abstraction libraries. Using those should reduce platform-specific
code in most other packages to a minimum.

## Human-Centric Networking

[Human-Centric Networking]({{< relref "knowledge_base/human_centric_internet" >}}),
or HCN for short, refers to a shared concept of which exist different
implementations. The concept is an evolution of [information-centric networking]({{< relref "knowledge_base/information_centric_networking" >}})
and 
[peer-to-peer (p2p)]({{< relref "knowledge_base/peer_to_peer" >}}) networks,
and a shift away from traditional networks.

In HCN, one does not address target machines to connect to, but instead connects
to shared resource directly. In this way, applications become unaware of where a
resource is located. The HCN stack can take care of finding the best match of a
machine to connect to.

It's possible to realize an HCN approach on top of existing Internet protocols,
both for ease of implementation and adoption. However, HCN is likely to realize
its potential particularly well when pushed lower down the ISO/OSI stack.

## HCN and Human Rights

The HCN approach is a particular good fit for treating human rights as a first
class concern, because of the way HCN removes the notion of a specific machine
to connect to.

The implication of this is that any machine can act as a cache for resources.
Since those machines cannot always be trusted, a modern HCN approach must
necessarily take care that information leakage is not possible and end-to-end
encrypt any resource that is not public. Only then is it feasible to treat
arbitrary machines as safe for caching.

A side-effect of this is that content introspection as the HTTP stack provides
is no longer feasible, which immediately solves a subset of human
rights issues on the Internet, such as protection from surveillance capitalism
or for whistleblowers, etc.

## Architecture

The full [architecture specification](https://specs.interpeer.org/draft-jfinkhaeuser-interpeer/)
contains details on the architecture. However, the following diagram
helps locate individual projects of the Interpeer HCN stack in the ISO/OSI
layers.

{{< fig src="architecture.svg"
    alt="Interpeer Architecture ISO/OSI layers"
    rel="external"
    target="\_blank"
    width="100%"
    nolink="true"
    caption="System Architecture with ISO/OSI layers"
>}}


* **Network Layer:** it's feasible/planned to create [packeteer]({{< relref packeteer >}})
  connectors that operate directly on data link layer protocols.
* **Transport Layer:** [channeler]({{< relref channeler >}}) has its own addressing scheme;
  given a routing protocol, it could operate on the data link layer directly. In practice,
  however, building on IP or other network layer protocols is best.
* **Session Layer:** mostly, *channeler* uses packeteer's socket API abstraction,
  in particular `AF_LOCAL` and UDP sockets.
  *Moldoveanu* is intended to be an enhanced (and incompatible)
  version of Kademlia. *Hubur* is intended to be a streaming protocol
  operating at the *vessel extent* level of granularity for resource
  access.
* **Presentation Layer:** [CAProck]({{< relref caprock >}}) integrates with *vessel*, but also
  exposes an API upwards for managing distributed authentication. [vessel]({{< relref vessel >}})
  is a streaming-optimized container file format with multi-authoring
  considerations built in. Finally, [wyrd]({{< relref wyrd >}}) is an API and plugin framework
  for representing files as a series of incremental modifications via a CRDT.
* **Application Layer:** The [GObject]({{< relref "projects/sdks/gobject" >}}) and *Android*
  SDKs provide application developer APIs for the stack, with other SDKs feasible.
* **Layer Spanning:** [liberate]({{< relref liberate >}}) is a platform abstraction library
  used in multiple projects. Similarly, [s3kr1t]({{< relref s3kr1t >}}) encapsulates crypto
  libraries such that their respective APIs do not need to be known in the other
  code bases.

## Technology Tree

Another way of looking at these projects is a technology tree, as you would
perhaps see in some games. This mixes projects and project features as
milestones with dependencies on each other.

It's possible to work on a milestone without having the dependencies in place,
but this can then only be a placeholder; something that demonstrates a principle.

{{< fig src="techtree.svg"
    alt="Interpeer Technology Tree"
    rel="external"
    target="\_blank"
    width="100%"
    nolink="true"
    caption="Technology Tree"
>}}

In this view, the colours represent various stages of completion:

- *Salmon:* indicates perpetually ongoing work, which grows alongside other
  milestones. This applies to e.g. documentation work, or base libraries.
- *Green:* means feature complete, with only minor tweaks or bugfixes expected.
- *Teal:* shows that an initial implementation was completed, but it will need
  more work for feature completeness. This is distinct from having a new
  feature milestone, and may e.g. mean work to clean up to the code base or
  add a smaller feature.
- *Purple:* indicates that a project is currently in progress. If you do not
  see any purple here, however, this does not mean there is no progress
  being made. It just means this graphic is outdated.
- *Light gray:* shows planned milestones, perhaps with a design outline already
  in existence.

## Future

Human rights on the Internet cannot be fully protected in a networking stack.
That much is clear.

For example, dark patterns in user interface design can trick people into
making choices that are bad for them, apparently consenting to violations of
their rights that they may not agree with.

Should such topics be part of the Interpeer Project? Probably.

But for now, the technical work required to build a full HCN stack is enough
for the resources we have.

If you would like to help reach such a future faster, you can always participate
in one of the projects -- or [make a small donation]({{< relref "donations" >}}).
