---
title: packeteer
repo: https://codeberg.org/interpeer/packeteer
spdx: GPL-3.0-only
tags: ['datagram', 'packet', 'platform abstraction', 'socket', 'networking', 'input/output', 'async', 'asynchronous']
impl:
  lang: C++
  deps:
  - liberate
image: packeteer.png
funding:
  ngi: NGI Zero Discovery
  cordis: 825322
description: >-
  Packeteer is a cross-platform networking abstraction with a socket-like
  interface, providing datagram capabilities as first class citizens.
---

## Introduction

Packeteer is a cross-platform networking abstraction with a socket-like
interface, providing datagram capabilities as first class citizens.

The design of this library goes back a long time, before networking code was
anywhere near standardization in C++. While this is changing, even now the
majority of abstractions over sockets in C++ assume a connection-oriented
design, which on the Internet implies {{% extref tcp "TCP/IP" %}}.

However, in order to implement protocols that do not follow the connection
paradigm, a datagram oriented API is also required. This typically also
requires a more reactive API that responds to network I/O events rather than
simply waiting.

Packeteer abstracts out platform differences between sockets and socket-like
APIs such as named or anonymous pipes. It furthermore provides an event-driven
I/O interface, and fully supports multi-threaded I/O. In fact, adding multiple
I/O threads is as easy as providing a configuration flag in the initialization
code.

Unlike other I/O event libraries, packeteer does not leak any platform-specific
abstraction into application code. In doing so, it is possible to add extensions
to packeteer that follow the same general API -- stream or datagram oriented
I/O -- without building on operating system handles or file descriptors.
