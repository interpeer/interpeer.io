---
title: CAProck
repo: https://codeberg.org/interpeer/caprock
spdx: GPL-3.0-only
tags: ['distributed authorization', 'authorization', 'power of attorney', 'caprock', 'capabilities', 'ocap', 'icap']
impl:
  lang: C++
  api: C
  deps:
  - liberate
  - s3kr1t
specs:
  - url: https://specs.interpeer.org/draft-jfinkhaeuser-caps-for-distributed-auth/
    title: Capabilities for Distributed Authorization
  - url: https://specs.interpeer.org/draft-jfinkhaeuser-caprock-auth-scheme/
    title: CAProck Distributed Authorization Scheme
  - url: https://specs.interpeer.org/draft-jfinkhaeuser-caprock-enc-compact/
    title: CAProck Compact Wire Encoding
docs: caprock
funding:
  isoc: research
description: >-
  CAPRock provides an implementation of a distributed authorization
  primitive with cryptographic guarantees.
---

## Introduction

CAProck provides an implementation of a distributed authorization scheme
with cryptographic guarantees. It is essentially a distributed {{% extref
  "ocap" "Object-capability model" %}} using a
cryptographic variant of a {{< external text="Power of Attorney"
  url="https://en.wikipedia.org/wiki/Power_of_attorney" archive=false >}} model for ensuring
it can be used in a fully distributed manner.

## Operation

Centralized authorization schemes query a centralized service whether an
authenticated entity (a *subject*) currently holds a privilege or permission
(a *predicate*) for a given *object*[^rdf].

[^rdf]: The data queried therefore conceptually represents an {{% extref
    "rdf_triple" "RDF triple" %}}, even though
    IRIs may not be used. However, it is theoretically possible to use IRIs for
    these fields.

Distributing this information means serializing it into a distributable form.
However, to verify that it has not been tampered with, one needs to additionally
add a cryptographic signature. In order to verify the signature, an issuer
identifier is also helpful.

Distributing such serialized *tokens* or *capabilities* is now possible, and the
recipient can verify it. This implies that the signer and recipient do not need
to communicate directly. Instead, the signer may pass the token to e.g. the
subject of the capability, which then hands it to the recipient at some later
date. The recipient can validate the token, and grant access to the object
without the signer getting involved.

## Use-Cases

The use cases for this kind of authorization scheme always involve scenarios
in which the issuer of a token cannot be reached at the time the authorization
needs to be enforced.

* In IoT device provisioning, it may be necessary to configure a device via
  some physical medium before it being able to register on a network. For
  example, the device may need to send a token to a network server to join
  a system of IoT nodes.
* When collaborating on a shared resource, it is necessary for a service at
  which the resource lives to grant access to all collaborators, whether the
  owner is currently reachable on the network.

  This can also be addressed in a centralized system, and is often the cited
  reason for centralization - but a distributed authorization scheme makes such
  centralization unnecessary.
* etc.

## Additional Concerns

Other than the basic principle of signing and verifying authorization triples,
care must be taken to not grant authorization in perpetuity.

* Typically, tokens include a valid time range to limit damage that may be done.
* Explicit revocations may still be necessary for emergencies.
* Wildcards may be useful to limit the number of tokens issued, e.g. by issuing
  one for all objects belonging to an issuer, etc.

CAProck addresses these concerns; see the linked specifications for details. The
quick version is that CAProck can produce and consume an ordered stream of
tokens, and act upon the final authorization state that results from processing
the stream.

## Caveats

In a distributed systems, a stream of tokens may not be fully synchronized.
CAPRock's approach is to embrace eventual consistency, and produce results based
on the synchronized sub-set of the stream -- on the assumption that in many
cases, that will be entirely sufficient.

In principle that makes attacks possible where monitoring a stream and
suppressing the delivery of selected tokens will manipulate the authorization
results. In practice, the solution to this is to encrypt the stream, either
in transport or by encapsulating it in a [vessel]({{< relref vessel >}})
resource.
