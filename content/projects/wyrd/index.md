---
title: wyrd
repo: https://codeberg.org/interpeer/wyrd
spdx: GPL-3.0-only
tags: ['collaborative-editing', 'collaborative-framework', 'crdt', 'conflict-free replicated data type', 'conflict-free', 'replicated', 'data type', 'wyrd', 'internet']
impl:
  lang: C++
  api: C
  deps:
  - vessel
  - liberate
image: wyrd.png
funding:
  isoc: research
  ngi: NGI Assure
  cordis: 957073
docs: wyrd
description: >-
  Wyrd is an implementation of a Conflict-free Replicated Data Type (CRDT)
  built to integrate with vessel, and thus provides
  a framework for collaborative editing. It represents a resource as a sequence
  of edits, which can be merged into a finalized state after synchronization.
---

## Introduction

Wyrd is an implementation of a {{< external
  text="Conflict-free Replicated Data Type" url="https://crdt.tech/" archive=false >}} (CRDT)
built to integrate with [vessel]({{< relref vessel >}}), and thus provides
a framework for collaborative editing. It represents a resource as a sequence
of edits, which can be merged into a finalized state after synchronization.

## Status

Wyrd is completed in principle, but integration with vessel is ongoing. As such,
it is not yet easy to synchronize files produced by wyrd incrementally. This is
expected to change in early 2023.
