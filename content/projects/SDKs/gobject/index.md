---
title: GObject SDK
slug: sdks/gobject-sdk
tags: ['collaborative-editing', 'collaborative-framework', 'SDK', 'internet', 'interpeer', 'gobject', 'glib']
repo: https://codeberg.org/interpeer/gobject-sdk
spdx: GPL-3.0-only
impl:
  lang: C++
  api: C (GIR)
  deps:
  - projects/wyrd
  - projects/vessel
  - projects/liberate
  - projects/s3kr1t
funding:
  ngi: NGI Assure
  cordis: 957073
docs: gobject-sdk
description: >-
  The GObject SDK provides a convenient mapping from wyrd properties to GObject
  properties and vice versa. The SDKs are under heavy development.
---

## Introduction

The GObject SDK provides a convenient mapping from [wyrd]({{< relref "projects/wyrd" >}})
properties to [GObject](https://docs.gtk.org/gobject/index.html) properties and
vice versa. GObject is an object abstraction for use with
[GTK](https://gtk.org/) applications.

In the GObject model, it is possible to listen for and emit signals. Properties
are named object members that emit a signal when modified. When a user interface
element modifies an internal object variable, it is possible to listen for this,
and immediately trigger an application state change.

We use this to convert the GObject property to a Wyrd property, and write its
value to a resource. Conversely, we use Wyrd's callbacks of a similar purpose
to update GObject properties.

In this way, we create a bridge between the user interface and application
state, and the shared [vessel]({{< relref "projects/vessel" >}}) resource
backing the Wyrd properties. When the resource gets synchronized, so does the
application state.

## Status

The SDK is simple, but demonstrates the principle well. Future iterations must
necessarily include more of the Interpeer stack.
