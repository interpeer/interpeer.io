---
title: SDKs
tags: ['collaborative-editing', 'collaborative-framework', 'SDK', 'internet', 'interpeer']
description: >-
  The purpose of the Interpeer SDKs is to provide an integrated experience for
  creating collaborative editing applications. The SDKs are under heavy development.
---

## Introduction

The purpose of the Interpeer SDKs is to provide an integrated experience for
creating collaborative editing applications. This can mean either to modify
and share [vessel]({{< relref "projects/vessel" >}}) resoures, or to manipulate
[wyrd]({{< relref "projects/wyrd" >}}) properties stored within such resources.

As long as the APIs these libraries provide are still somewhat in flux, you can
expect the SDKs to also change. That being said, they provide convenience for
integrating the Interpeer stack into their target platform.

It's best to dive simply into the examples and documentation each SDK below
provides.

## SDKs

- [GObject SDK]({{< relref "projects/sdks/gobject" >}})
