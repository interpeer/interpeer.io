---
title: liberate
repo: https://codeberg.org/interpeer/liberate
spdx: GPL-3.0-only
impl:
  lang: C++
tags: ['utility', 'library', 'liberate']
image: liberate.png
funding:
  ngi: NGI Zero Discovery
  cordis: 825322
  isoc: research
docs: liberate
description: >-
  Liberate is a platform abstraction library, written in C++. It serves all other
  Interpeer projects.
---

## Introduction

Liberate is a platform abstraction library, written in C++. It serves all other
projects.

Providing a good platform abstraction in C++ is difficult. The standard library
covers only so much ground. Larger library collections such as {{< external
  text="boost" url="https://www.boost.org/" archive=false >}} provide great resources, but at
the expense of large binary sizes and, in some cases, custom build tools. These
make it difficult to maintain cross-platform builds.

Given that the needs of Interpeer's sub-projects are relatively narrow, it was
an easier choice to provide an own platform abstraction library. However, users
should be aware that the goal is not to replace or compete with other projects
here. This library is predominantly for internal use.

That being said, there may be just enough here for your use case.
