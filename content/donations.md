---
title: Donate Today!
layout: bare
tags:
- donate
- threat
- surveillance capitalism
- disinformation
- mission
description: |
  Donate today to the Interpeer Project, and join the mission to create an
  Internet as it was meant to be -- bringing together people around the world
  safely and securely!
---

{{<multicol 2>}}
{{<col "home row-start-2 md:row-start-1">}}

{{<slogan "h1" "mb-8 md:mb-16">}}Donate Today!{{</slogan>}}

<script type="text/javascript" src="https://secure.fundraisingbox.com/app/paymentJS?hash=uhhw8hscn5qoxjio"></script>
<noscript>Please enable JavaScript</noscript>
<a target="_blank" href="https://www.fundraisingbox.com"><img class="no-lazy" border="0" style="border: 0 !important" src="https://secure.fundraisingbox.com/images/FundraisingBox-Logo-Widget.png" width="140" height="40" alt="FundraisingBox Logo" /></a>
{{</col>}}

{{<col "home row-start-1 md:pr-10">}}
{{<markdown>}}
{{<heading>}}The Internet under Threat{{</heading>}}

The Internet promised to be this amazing network that could bring people around
the world together! But whether it is surveillance capitalism that collects data
about you, or political troll farms that spew disinformation, that liberating and
unifying Internet is increasingly under threat.

{{<slogan "h2" "my-9 md:mt-18">}}We need you,<br/> because this vision can still be achieved!{{</slogan>}}

Interpeer gUG tries to finance the Interpeer Project development via research
grants. Such grants work well for working out the basics, but everything else
-- bugfixing, working out edge cases, making things work well in short -- is
difficult to fund.

{{<heading "my-6">}}How to help?{{</heading>}}

If you think you can help directly, join one of our [projects]({{< relref "projects" >}}),
or [get in touch]({{< list_subscribe >}}).
Otherwise, a small contribution can fund someone who can.

All donations go 100% towards our [mission]({{< relref "about/mission_statement" >}}). The
process is simple and quick -- just fill in the form on this page!

Thank you!
{{</markdown>}}
{{</col>}}
{{</multicol>}}
