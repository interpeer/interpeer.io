---
type: page
title: References
layout: "bare"
description: A list of references with particular relevance to the Interpeer Project
---

The following is *not* a complete list of references (external and internal),
but comprises those that are referenced on a number of pages, or are
particularly relevant to the Interpeer Project. That makes them worth listing collectively.

\[references\]
