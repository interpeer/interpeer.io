---
title: Interpeer Project
layout: bare
orginfo: true
tags:
- digital sovereignty
- human centric
- information-centric networking
- icn
- privacy
- open source
description: |
  The Interpeer Project's objective is to empower a human centric Internet. In
  practice, this currently takes the form of a technological initiative with the
  goal of providing a secure and efficient information-centric networking stack.

---

{{<multicol 2>}}
{{<col "home row-start-2 md:row-start-1">}}
{{<slogan "h1" "mb-8 md:mb-16">}}Towards a human-centric,<br/> next generation Internet.{{</slogan>}}
{{<blog_latest>}}
{{</col>}}

{{<col "home row-start-1">}}
{{<heading>}}What is the Interpeer Project?{{</heading>}}

{{<markdown>}}
The Interpeer Project's objective is to empower a human centric Internet.
This currently takes the form of a technological initiative
with the goal of providing a secure and efficient
[human-centric networking]({{< relref "knowledge_base/human_centric_internet" >}}) stack
that protects human rights by design.
{{</markdown>}}

{{<slogan "h2" "my-9 md:mt-18">}}The future is fully distributed.{{</slogan>}}

{{<markdown>}}
This breaks down into a growing number of component
[projects]({{< relref "projects" >}}), that you can help with -- join the
discussion list to reach out! Or if you prefer to donate, every cent will
go 100% to our [mission]({{< relref "about/mission_statement" >}}).
{{</markdown>}}

{{<callout>}}
[DONATE NOW]({{<relref "donations" >}}) to help create a better Internet!
{{</callout>}}

{{</col>}}


{{<col "md:col-span-2 flex justify-center">}}

{{<card title="Join the mailing list" var="forest" extra="max-w-md" extra_content="card-snug">}}
[Join today]({{< list_subscribe >}}) to stay informed, and
talk to us. We don't send ads. This is a general purpose discussion list for the
project.
{{</card>}}

{{</col>}}

{{</multicol>}}
