---
title: Imprint
---

## Contact

Interpeer gUG (haftungsbeschränkt)<br/>
Feldgereuth 8<br/>
86926 Greifenberg<br/>
DE - GERMANY<br/>

info@interpeer.org<br/>

## Legal/Tax Information

| Registration             | Number      |
|--------------------------|-------------|
| **Amtsgericht Augsburg** | HRB 37686   |
| **VAT/USt-IdNr.**        | DE355996098 |

## Legal Disclaimer

The contents of our pages have been created with the utmost care. However, we
cannot guarantee the contents’ accuracy, completeness or topicality.
According to statutory provisions, we are furthermore responsible for our own
content on these web pages. In this context, please note that we are
accordingly not obliged to monitor merely the transmitted or saved information
of third parties, or investigate circumstances pointing to illegal activity.
Our obligations to remove or block the use of information under generally
applicable laws remain unaffected by this as per §§ 8 to 10 of the Telemedia
Act (TMG).

Responsibility for the content of external links (to web pages of third
parties) lies solely with the operators of the linked pages. No violations were
evident to us at the time of linking. Should any legal infringement become
known to us, we will remove the respective link immediately.
