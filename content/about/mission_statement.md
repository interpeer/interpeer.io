---
title: "Mission Statement of the the Interpeer Project"
type: page
layout: bare
tags:
- mission statement
- non-profit
- data transparency
- data sovereignty
- compatibility
- inteoperability
- security
- privacy
- data transfer
aliases:
- mission-statement
- mission
---

{{<multicol 2>}}

{{<col "md:col-span-2 flex flex-col text-center justify-center">}}
{{<markdown>}}
## Mission Statement

The law in Germany regarding non-profit entities like Interpeer gUG mandate
that a specific set of laws is referred to in a mission statement that is
part of the legal entity's by-laws. This has to be included in German. Below is
the German original, as well as a version translated into English.
{{</markdown>}}
{{</col>}}


{{<col>}}
{{<markdown>}}
### German

1.  Zweck der Gesellschaft ist die Förderung der Wissenschaft und Forschung
    (§ 52 Abs. 2 Satz 1 Nr. 1 AO) sowie Förderung der Erziehung, Volks- und
    Berufsbildung einschließlich der Studentenhilfe (§ 52 Abs. 2 Satz 1 Nr.
    7 AO).
1. Die Gesellschaft erfüllt ihre Zwecke in Abs. (1) insbesondere durch folgende
   Maßnahmen:
   1. Forschung und Entwicklung von neuen Technologien in offenen und
      dezentralisierten, computergestützten Software- und Systemarchitekturen,
      insbesondere zu den folgenden Themenbereichen:
      - Datentransparenz und Datenhoheit bspw. durch Technologien, die die
        Übermittelung von persönlichen oder personenbezogenen Daten für die
        Betroffenen einsehbar machen und es ermöglichen, dieser feingranular
        zuzustimmen oder entgegenzuwirken;
      - Datenkompatibilität und -interoperabilität bspw. durch offene Standards,
        welche die Übertragung von Daten von einem an einen anderen
        Dienstleister vereinfachen, ohne Verlust an Metainformationen, der die
        spätere Bearbeitung beeinträchtigen könnte;
      - Sicherheit und Privatsphäre bspw. durch kryptographische Systeme, die
        sicherstellen, dass Daten so verschlüsselt werden, dass nur eine
        ausgewählte Zielstelle sie einsehen kann (Ende-zu-Ende-Verschlüsslung);
      - Datentransfer bspw. durch Netzwerkprotokolle, die Daten nur unter
        Berücksichtigung der o.g. Bereiche übermitteln.
   1. Durchführung wissenschaftlicher Veranstaltungen (z.B. Konferenzen und
      Symposien).
   1.  Veröffentlichungen sowie Veranstaltung von Fortbildungen, Vorträgen und
      Seminaren.
{{</markdown>}}
{{</col>}}


{{<col>}}
{{<markdown>}}
### English

1. The purpose of the company is the advancement of science and research (§ 52
   Abs.  2 Satz 1 Nr. 1 AO) as well as the furtherance of public education and
   occupational training, including student support (§ 52 Abs. 2 Satz 1 Nr. 7
   AO).
1. The company fulfils its purpose in paragraph (1) especially by the following
   measures:
   1. Research and development of new technologies in open and decentralized,
      computer supported soft- and system architectures, in particular as
      relating to the following topics:
      - data transparency and [data sovereignty]({{< relref "knowledge_base/data_sovereignty" >}}), e.g. through technology that
        makes visible the transfer of personal or personally identifiable
        information, and that allows agreeing to or preventing it;
      - data compatibility and interoperability, e.g. through open standards that
        ease data mobility from one service provider to another, without loss of
        meta information that could hinder later processing;
      - security and privacy, e.g. through cryptographic systems that ensure data
        is encrypted in such a way that only the recipient can access it (end-to-end
        encryption);
      - data transfer, e.g. through networking protocols that transfer data only
        under consideration of the topics above.
    1. Realisation of scientific events (e.g. conferences and symposia).
    1. Publications and events relating to education, lectures and seminars.
{{</markdown>}}
{{</col>}}


{{</multicol>}}


