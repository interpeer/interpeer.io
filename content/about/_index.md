---
title: "About the Interpeer Project"
type: page
layout: bare
faq:
  - question: What is the Interpeer Project?
    answer: >-
      The Interpeer Project's objective is to empower a human centric Internet.
  - question: What is "human-centric"?
    answer: >-
      We use the term "human-centric" to oppose that which is not focused on serving
      people.
      This is, for example, market-driven data-harvesting. It is tech that violates
      human rights, perhaps simply by not addressing such concerns.
  - question: What is meant by "public money, public code"?
    answer: >-
      As we're mainly funded by public research grants and donations, we pledge to
      make all research results and code public -- free of charge, of course!
tags:
- digital sovereignty
- human centric
- human-centric internet
- human-centric networking
- information-centric networking
- ICN
- public money
- public code
- privacy
- open source
- decolonisation of the internet
---

{{<multicol 2>}}
{{<col>}}

{{<card title="Why Interpeer?" extra="md:h-full">}}
The Interpeer Project was born out of a critical perspective of the contemporary
state of human-computer interaction and the Internet.

Computers have become ubiquitous. However, the most popular protocol connecting
users to remote machines, HTTP, follows an obsolete philosophy: our actions do not
need to be arbitrated by third party machines, and often our data does not need
to traverse them.

We need an Internet that fits contemporary usage patterns.

- An Internet where people own their data (often called [data sovereignty]({{<
      relref "knowledge_base/data_sovereignty" >}})) and
  can share it between devices and contacts without the need for intermediaries.
- Where digital technology that powers it is as freely accessible as the knowledge
  to adapt it to specific needs (called [technical sovereignty]({{<
      relref "knowledge_base/technical_sovereignty" >}})) and
- Where cryptography allows users to securely delegate trust to any number of
  devices, even those they don’t know directly.
- A [information-centric networking]({{< relref
    "knowledge_base/information_centric_networking" >}})-like technology that
  enables trust delegated machines to share data directly and efficiently.
- An Internet which thus aims for [decolonisation]({{< relref
      "knowledge_base/internet_decolonisation" >}}).

The Interpeer Project was started to address these topics, with the goal of
helping build a [Human-Centric Internet]({{< relref "knowledge_base/human_centric_internet" >}}).
{{</card>}}

{{</col>}}

{{<col>}}

{{<card title="What is \"human-centric\"?" var="forest" extra="mb-10 md:mb-14">}}
We use the term "human-centric" to oppose that which is not focused on
serving people.

This is, for example, market-driven data-harvesting. It is tech that
violates human rights, perhaps simply by not addressing such concerns.

But it also acknowledges the human experience as a small part of the
environment in which it unfolds. "Human-centric" also means opposing
wasteful computing practices that destroy our environment.
{{</card>}}

{{<card title="Public money, public code" var="forest">}}
Research and development with public funds means we produce {{<
  external text="open source" url="https://opensource.org/osd" archive=false >}}
code as well as [openly accessible specifications](https://specs.interpeer.org).

If there are relevant studies or other results we produce, we pledge
to also publish them in a freely accessible manner.
{{</card>}}

{{</col>}}
{{</multicol>}}


